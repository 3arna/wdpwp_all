var help = require(process.cwd() + '/lib/help.js');

module.exports = function(app){
  
  var controllers = app.controllers;
  
  if(app.env == 'development'){
    
    this.get('/api/wdpwp/profile/?(:id)?', controllers.api.wdpwpProfile);
    this.get('/api/bet/users', controllers.api.betUsers);
    this.get('/api/wdpwp/logs', controllers.api.uniqueUsers);
  }
  return this;
}