var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.ips = Model.col({col:'Ips', db:'profiles'});
  
  Model.updateOne = function(data, cb){
    var set = {
      $inc : { amount : 1 },
      $addToSet : { steamIds : data.steamId },
      $set : {ip : data.ip, updated : help.date.return('ms')},
    }
    Model.cols.ips.update({ip : data.ip}, set, {upsert : true}, function(err,data){
      cb ? cb(err,data) : false;
    })
    /*Model.cols.ips.update({steamId : data.steamId}, {$set : profile}, {upsert : true}, function(err,data){
      cb ? cb(err,data) : false;
    })*/
  }
  
  /*
  Model.updateOne = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }*/
  
  
  return Model;
}