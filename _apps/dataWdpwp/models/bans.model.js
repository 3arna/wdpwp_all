var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.bans = Model.col({col:'Bans', db:'profiles'});
  
  Model.findOne = function(data, cb){
    Model.cols.bans.findOne({$or : [{ip : data.ip}, {steamId : data.steamId}]}, cb).lean();
  }
  
  Model.ip = function(data, cb){
    Model.cols.bans.update({id : help.date.return('ms')}, data, {upsert : true}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  /*
  Model.updateOne = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }*/
  
  
  return Model;
}