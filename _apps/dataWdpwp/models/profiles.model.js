var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.profiles = Model.col({col:'Profiles', db:'profiles'});
  
  /*Model.updateStats = function(stats){
    if(stats){
      for(var user in stats){
        Model.updateOne(user, stats[user])
      }
    }
  }*/
  
  Model.return = function(cb){
    if(Model.off){return cb(false,false)}
    Model.cols.profiles.find().lean().exec(cb);
  }
  
  Model.returnOne = function(cb){
    if(Model.off){return cb(false,false)}
    var steamId = this.id || false;
    Model.cols.profiles.findOne({steamId : steamId}, {_id : 0}).lean().exec(cb);
  }
  
  Model.updateOne = function(profile, cb){
    profile.updated = help.date.return('ms');
    Model.cols.profiles.update({steamId : profile.steamId}, {$set : profile}, {upsert : true}, function(err,data){
      cb ? cb(err,data) : false;
    })
  }
  
  /*
  Model.updateOne = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }*/
  
  
  return Model;
}