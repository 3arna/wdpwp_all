var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.main = Model.col({col:'Users', db:'def'});
  
  Model.return = function(cb){
    if(Model.off){return cb(false,false)}
    Model.cols.main.find().lean().exec(cb);
  }
  
  Model.addOne = function(user, cb){
    if(Model.off){return cb(false,false)}
    
    user = {
      steamId : user.steamId,
      user : user.nick,
      name : user.nick,
      disabled : true,
      psw : help.str.random(7)
    }
    cb(false, user);
    /*Model.cols.main.update({steamId : user.steamId}, {$set : user}, {upsert : true}, function(err, done){
      
    });*/
  }
  
  
  return Model;
}