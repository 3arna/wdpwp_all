var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.steamProfile = function(cb){
    var id = this.id || false;
    
    async.parallel(
      {
       profile : parsers.steamProfileData.return.bind({id : id}),
       sr : parsers.steamRep.return.bind({id : id}),
       games : parsers.steamProfileGames.return.bind({id : id}),
       comments : parsers.steamProfileComments.return.bind({id : id}),
       friends : parsers.steamProfileFriends.return.bind({id : id}),
       inventory : collectInventoryAmount.bind({id : id}),
       rep : parsers.tf2Rep.return.bind({id:id})
      },
      function(err, result){
        if(err){return cb(err)}
        result = help.lodash.merge(
          result.profile,
          result.sr,
          result.games,
          result.comments,
          result.friends,
          result.inventory,
          result.rep
        );
        cb(err, result);
      }
    );
  }
  
  var collectInventoryAmount = function(cb){
    var id = this.id;
    async.parallel(
      {
       giftsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '753'}),
       dota2ItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '570'}),
       csgoItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '730'}),
       tf2ItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '440'})
      },function(err, result){
        result.items = true;
        err ? cb(false, {items : false}) : cb(err, result);
      }
    );
  }
  

  return controller;
}




