var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.wdpwpProfile = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
       profile : models.profiles.returnOne.bind({id : id})
      },
      function(err, result){
        if(err){return res.send(err)}
        res.send(result);
      }
    );
  }
  
  controller.steamProfileData = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
       data : parsers.steamProfileData.return.bind({id : id})
      },
      function(err, result){
        res.send(result);
      }
    );
  }
  
  controller.uniqueUsers = function(req, res){
    models.logs.returnUnique(function(err, data){
      res.send(app.render({length : data.length, ips : data}));
    })
  }
  
  controller.betUsers = function(req, res){
    models.users.return(function(err, data){
      res.send(app.render({data : data, keys : help.arrObjs.collectKeys(data)}));
    })
  }
  
  return controller;
}




