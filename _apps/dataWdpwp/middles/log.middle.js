var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    auth        = require(paths.lib + 'auth/steam'),
    async       = require('async');
      
module.exports = function(app){
  var middle = {};
  var models = app.models;
  
  middle.route = function(req, res, next){
    if(app.env === 'development'){return next();}
    var log = {
      ip : help.getIp(req),
      route : req.url
    };
    
    app.sys.apps.dataBets.models.users.ipExists(log.ip, function(err, ipCount){
      if(!ipCount){
        app.sys.cache.unique++;
      }
    })
    
    app.sys.cache.requests++;
    
    req.params && Object.keys(req.query).length ? log.params = req.params : false;
    req.query && Object.keys(req.query).length ? log.query = req.query : false;
    req.protocol ? log.protocol = req.protocol : false;
    req.cookies ? log.cookies = JSON.stringify(req.cookies) : false;
    req.body && Object.keys(req.body).length? log.body = JSON.stringify(req.body) : false;
    models.logs.addOne(log);
    next();
  }
  
  return middle;
}




