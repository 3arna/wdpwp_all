var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    auth        = require(paths.lib + 'auth/steam'),
    async       = require('async');
      
module.exports = function(app){
  var middle = {};
  var models = app.models;
  var steamAuth = new auth();
  
  middle.user = function(req, res, next){
    steamAuth.auth(req,res,function(err, id){
      checkIpAndId({
        id : id,
        ip : help.getIp(req),
        url : req.url
      }, function(err){
        if(err) return res.send('Your account is banned please try to login again with other account <a href="/">home page</a>');
        req.steamId = id;
        next();
      })
    });
  };
  
  middle.setModer = function(req, res, next){
    if(req.session && req.session.profile && req.session.profile.steamId === '76561198065626987'){
      req.moder = true;
    }
    next();
  }
  
  middle.moder = function(req, res, next){
    if(req.session && req.session.profile && req.session.profile.steamId === '76561198065626987'){
      return next();
    }
    
    return res.redirect('/error');
  }
  
  
  var checkIpAndId = function(obj, cb){
    var ban = {
      steamId : obj.id,
      ip : obj.ip,
      route : obj.url,
    }
    if(!obj.ip){
      ban.desc = 'no ip specified';
      ban.reason = 'hacking atempt';
      models.bans.ip(ban);
      return cb(ban.reason);
    } else {
      models.bans.findOne({ip : obj.ip, steamId : obj.id}, function(err, data){
        if(data){
          ban.desc = 'banned person is trying to connect to the system again';
          ban.reason = 'banned already';
          models.bans.ip(ban);
          return cb(ban.reason);
        } else {
          models.ips.updateOne({
            ip : obj.ip,
            steamId : obj.id
          });
          return cb(false);
        }
      })
    }
  }
  
  return middle;
}




