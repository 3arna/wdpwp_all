var help = require(process.cwd() + '/lib/help.js');

module.exports = function(app){
  
  var controllers = app.controllers;
  
  if(app.env == 'development'){
    
    this.get('/api/steam/profile/?(:id)?', controllers.api.steamProfile);
    this.get('/api/steam/profile/data/?(:id)?', controllers.api.steamProfileData);
    this.get('/api/steam/profile/games/?(:id)?', controllers.api.steamProfileGames);
    
    this.get('/api/steam/rep/?(:id)?', controllers.api.steamRep);
    this.get('/api/steam/app/?(:appId)?', controllers.api.steamApp);
    
  }
  return this;
  
}