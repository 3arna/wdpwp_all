var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.modify(basic));
    })
  }
  
  Parser.returnUrl = function(steamId){
    steamId = steamId || Parser.steamId;
    return 'http://rep.tf/api/bans?str='+steamId;
  };
  
  Parser.modify = function(data){
    var modded = false;
    if(data){
      modded = {
        opBan : data.opBans.banned == 'bad' ? true : false,
        bzBan : data.bzBans.banned == 'bad' ? true : false,
        //mctBan : data.mctBans.banned == 'bad' ? true : false,
        bpBans : data.bptfBans.banned == 'bad' ? true : false,
        tf2t : data.tf2tBans.banned == 'bad' ? true : false
      }
    }
    return modded;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'json', function(err, html){
      if(err) return cb(err);
      cb(err, html);
    }, 'post');
  }
  
  return Parser;
}