var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamApp = '760';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.modify(basic));
    })
  }
  
  Parser.returnUrl = function(steamId){
    steamId = steamId || Parser.steamId;
    return 'http://steamcommunity.com/comment/Profile/render/'+steamId+'/-1/';
  };
  
  Parser.modify = function(data){
    if(!data){return false}
    var modded = {
      steamComments : data.total_count || 0
    }
    return modded;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'json', function(err, html){
      if(err) return cb(err);
      cb(err, html);
    });
  }
  
  return Parser;
}