var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamApp = '760';
  
  Parser.return = function(cb){
    Parser.steamApp = this.id || Parser.steamApp;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.modify(basic));
      //cb(err, Parser.modify(basic));
    })
      
  }
  
  Parser.returnUrl = function(steamApp){
    steamApp = steamApp || Parser.steamApp;
    /*http://steamcommunity.com/id/timvisee/games?tab=all&xml=1&l=english*/
    var filters = ''/*'&filters=price_overview'*/;
    return 'http://store.steampowered.com/api/appdetails?appids='+steamApp+'&cc=us'+filters;
  };
  
  Parser.modify = function(data){
    if(!data){return false}
    data = data[Parser.steamApp] ? data[Parser.steamApp].data : false;
    var modded = false;
    if(data){
      modded = {
        name      : data.name,
        appId     : data.steam_appid,
        released  : help.date.return('ms', data.release_date.date),
        img       : data.header_image,
        free      : data.is_free
      }
      data.metacritic ? modded.meta = data.metacritic.score : false;
      if(data.price_overview){
        var cur = data.price_overview.currency;
        modded['price'+cur] = data.price_overview.initial/100;
        data.price_overview.discount_percent 
          ? modded.discount = data.price_overview.discount_percent/100 
          : false;
      }
    }
    return modded;
  }
  
  Parser.price = function(data){
    if(!data){return false}
    data = data[Parser.steamApp];
    if(data.success){
      if(data.data.price_overview){
        data = data.data.price_overview.final / 100;
      } else {
        data = 0;
      }
    }
    return data;
  }
  
  Parser.basic = function(cb){
    console.log(Parser.url);
    Parser.ask(Parser.url, 'json', function(err, html){
      if(err) return cb(err);
      cb(err, html);
    });
  }
  
  return Parser;
}