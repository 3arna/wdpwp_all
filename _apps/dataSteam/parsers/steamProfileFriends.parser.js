var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamApp = '760';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.modify(basic));
    })
  }
  
  Parser.returnUrl = function(steamId){
    steamId = steamId || Parser.steamId;
    return 'http://steamcommunity.com/profiles/'+steamId+'/friends?xml=1';
  };
  
  Parser.modify = function(data){
    if(!data){return false}
    var modded = false;
    if(data.friendsList){
      data = data.friendsList.friends.friend;
      modded = {
        steamFriends : data.length || 0
      }
    }
    return modded;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'xmlToObj', function(err, html){
      if(err) return cb(err);
      cb(err, html);
    });
  }
  
  return Parser;
}