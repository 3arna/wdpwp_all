var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.appId = '753';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.appId = this.appId || Parser.appId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.amount(basic));
    })
  }
  
  Parser.returnUrl = function(steamId, appId){
    steamId = steamId || Parser.steamId;
    appId = appId || Parser.appId;
    var num = appId != '753' ? '2' : '1';
    return 'http://steamcommunity.com/profiles/'+steamId+'/inventory/json/'+appId+'/'+num;
  };
  
  Parser.modify = function(data){
    if(!data){return false}
    var modded = false;
    return modded;
  }
  
  Parser.amount = function(data){
    if(!data){return false}
    return data.success ? Object.keys(data.rgInventory).length : 0
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'json', function(err, html){
      if(!err && html && html.success){
        cb(err, html);
      } else {
        cb(err || 'cant load the inventory');
      }
      
    });
  }
  
  return Parser;
}