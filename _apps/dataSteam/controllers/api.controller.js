var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.steamProfile = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
       profile : parsers.steamProfileData.return.bind({id : id}),
       sr : parsers.steamRep.return.bind({id : id}),
       games : parsers.steamProfileGames.return.bind({id : id}),
       comments : parsers.steamProfileComments.return.bind({id : id}),
       friends : parsers.steamProfileFriends.return.bind({id : id}),
       inventory : collectInventoryAmount.bind({id : id}),
       rep : parsers.tf2Rep.return.bind({id:id})
      },
      function(err, result){
        if(err){return res.send(err)}
        result = help.lodash.merge(
          result.profile,
          result.sr,
          result.games,
          result.comments,
          result.friends,
          result.inventory,
          result.rep
        );
        res.send(result);
      }
    );
  }
  
  var collectInventoryAmount = function(cb){
    var id = this.id;
    async.parallel(
      {
       giftsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '753'}),
       dota2ItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '570'}),
       csgoItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '730'}),
       tf2ItemsAmount : parsers.steamProfileInventory.return.bind({id : id, appId : '440'})
      },function(err, result){
        result.items = true;
        err ? cb(false, {items : false}) : cb(err, result);
      }
    );
  }
  
  controller.steamProfileData = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
       data : parsers.steamProfileData.return.bind({id : id})
      },
      function(err, result){
        res.send(result);
      }
    );
  }
  
  controller.steamApp = function(req, res){
    var id = req.params.appId || false;
    
    async.parallel(
      {
        app : parsers.steamApp.return.bind({id : id})
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  controller.steamRep = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
        profile : parsers.steamRep.return.bind({id : id})
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  controller.steamProfileGames = function(req, res){
    var id = req.params.id || false;
    
    async.parallel(
      {
       profile : parsers.steamProfileGames.return.bind({id : id})
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  //some app collector
  var collectAppsByList = function(list, cb){
    models.apps.returnByList(list, function(err, apps){
      var ids = help.lodash.xor(list, help.arrObjs.collectVars(apps, 'appId'));
      console.log('game amount needed', ids.length);
      ids.length
        ? models.apps.collectAndSaveByList(ids, function(err, newApps){
          cb(err, help.lodash.merge(apps, newApps));
        })
        : cb(err, apps)
    });
  }
  
  return controller;
}




