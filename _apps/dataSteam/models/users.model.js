var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.users = Model.col({col:'Users'});
  
  Model.updateStats = function(stats){
    if(stats){
      for(var user in stats){
        Model.updateOne(user, stats[user])
      }
    }
  }
  
  Model.return = function(cb){
    Model.cols.users.find().lean().exec(cb);
  }
  
  Model.returnOne = function(user, cb){
    Model.cols.users.findOne({user : user}).lean().exec(cb);
  }
  
  Model.updateOne = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  
  return Model;
}