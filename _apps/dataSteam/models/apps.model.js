var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var parsers = app.parsers;
  Model.cols.apps = Model.col({col:'Apps', db:'apps'});
  
  Model.returnByList = function(list, cb){
    if(Model.off){return cb(false,false)}
    Model.cols.apps.find({appId: {$in: list}, unable : {$exists : false}}).lean().exec(cb);
  }
  
  Model.collectAndSaveByList = function(list,cb){
    if(Model.off){return cb(false,false)}
    var apps = [];
    async.eachSeries(list, function(appId, cb) {
      
      parsers.steamApp.id = appId;
      parsers.steamApp.return(function(err, app){
        if(app){
          if(appId != app.appId){
            app = {appId : appId, mainId : app.appId, extension : true, free:true};
          }
        } else {
          app = {appId : appId, unable : true};
        }
        apps.push(app);
        Model.addOne(app, cb);
        /*help.delay(cb, 100);*/
      });
    }, function(err){
      
      cb(err, apps);
    });
  }
  
  Model.addOne = function(app, cb){
    if(Model.off){return cb ? cb(false,false) : false;}
    app.updated = help.date.return('ms');
    Model.cols.apps.update({appId : app.appId}, app, {upsert : true}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  /*Model.updateStats = function(stats){
    if(stats){
      for(var user in stats){
        Model.updateOne(user, stats[user])
      }
    }
  }
  
  Model.return = function(cb){
    Model.cols.users.find().lean().exec(cb);
  }
  
  Model.returnOne = function(user, cb){
    Model.cols.users.findOne({user : user}).lean().exec(cb);
  }
  
  Model.updateOne = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }*/
  
  
  return Model;
}