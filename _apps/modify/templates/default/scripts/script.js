/*function AppViewModel() {
    this.firstName = "Bert";
    this.lastName = "Bertington";
}

// Activates knockout.js
ko.applyBindings(new AppViewModel());*/

var ViewModel = function(first, last) {
  var self = this;
  
  self.users = ko.observableArray();
  self.keys = ko.observableArray();
  
  self.key = ko.observable(first);
  self.value = ko.observable(last);

  self.fullName = ko.pureComputed(function() {
    return self.key() + " " + self.value();
  }, self);
  
  self.addPerson = function(){
    self.keys.push(self.key());
  }
  
  $.getJSON( "/api/bet/users", function(res) {
    self.users(res.data);
    self.keys(res.keys);
  })
};

ko.applyBindings(new ViewModel("Planet", "Earth"));