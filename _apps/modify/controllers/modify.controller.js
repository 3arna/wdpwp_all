var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  
  controller.main = function(req,res) {
    res.end(app.render({}, 'modify/main'));
  }
  

  return controller;
}




