module.exports = function(app){
  
  var controllers = app.controllers;

  if(app.env == 'development'){
    this.get('/mod', controllers.modify.main);
  }
  return this;
  
}