var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Col = Model.col({col:'Users'});
  
  Model.return = function(cb){
    if(Model.off){return cb(false, false)}
    
    Col.find({}).lean().sort({"stats.balance" : -1}).exec(function(err, data){
      cb(err, data);
    });
  }
  
  Model.returnActive = function(cb){
    if(Model.off){return cb(false, false)}
    var ms = help.date.return('ms')-604800000;
    Col.find({"stats.updated" : {$gt : ms}}).lean().sort({"stats.balance" : -1}).exec(function(err, data){
      cb(err, data);
    });
  }
  
  Model.returnInactive = function(cb){
    if(Model.off){return cb(false, false)}
    var ms = help.date.return('ms')-604800000;
    Col.find({"stats.updated" : {$lt : ms}}).lean().sort({"stats.balance" : -1}).exec(function(err, data){
      cb(err, data);
    });
  }
  
  Model.returnOne = function(cb){
    var user = this.user || false;
    if(Model.off && !user){return cb(false, false)}
    
    Col.find({user : user}).lean().exec(function(err, data){
      cb(err, data);
    });
  }
  
  return Model;
}