var help = require(process.cwd() + '/lib/help.js');

module.exports = function(app){
  
  var controllers = app.controllers;
  var middles = app.sys.apps.dataWdpwp.middles;
  
  var modeDev = function(req,res,next){
    req.mode = 'dev';
    next();
  };
  
  var modeImage = function(req,res,next){
    req.mode = 'image';
    next();
  }
  
  //prod mode
  this.post('/app/trendis', modeImage, controllers.api.postImages);
  
  this.get('/app/trendis', modeImage, controllers.api.getImages);
  this.get('/app/trendis/up/:dateMs', modeImage, controllers.api.getUpImages);
  this.get('/app/trendis/down/:dateMs', modeImage, controllers.api.getDownImages);
  
  //dev mode
  this.post('/dev/trendis', modeDev, controllers.api.postImages);
  
  this.get('/dev/trendis', modeDev, controllers.api.getImages);
  this.get('/dev/trendis/up/:dateMs', modeDev, controllers.api.getUpImages);
  this.get('/dev/trendis/down/:dateMs', modeDev, controllers.api.getDownImages);
  
  return this;
}