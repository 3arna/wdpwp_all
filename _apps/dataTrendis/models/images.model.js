var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var recordLimit = 10;
  var Model = new M(app);
  var col = {
    image : Model.col({col:'Images', db:'trendis'}),
    dev : Model.col({col:'Dev', db:'trendis'})
  };
  
  Model.return = function(cb){
    col[app.mode].find().sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.returnUp = function(dateMs, cb){
    col[app.mode].find({'dateMs' : { $gt: dateMs } }).sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.returnDown = function(dateMs, cb){
    col[app.mode].find({'dateMs' : { $lt: dateMs } }).sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.add = function(data, cb){
    data.dateMs = help.date.return('ms');
    col[app.mode].update({imageId : data.imageId}, data, { upsert:true }, cb);
  }
  
  return Model;
}