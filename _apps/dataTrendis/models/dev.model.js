var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var recordLimit = 10;
  var Model = new M(app);
  var ImagesCol = Model.col({col:'Dev', db:'trendis'})
  
  Model.return = function(cb){
    ImagesCol.find().sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.returnUp = function(dateMs, cb){
    ImagesCol.find({'dateMs' : { $gt: dateMs } }).sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.returnDown = function(dateMs, cb){
    ImagesCol.find({'dateMs' : { $lt: dateMs } }).sort({dateMs : -1}).limit(recordLimit).lean().exec(cb);
  }
  
  Model.add = function(data, cb){
    data.dateMs = help.date.return('ms');
    ImagesCol.update({imageId : data.imageId}, data, { upsert:true }, cb);
  }
  
  return Model;
}