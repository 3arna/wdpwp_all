var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  
  var defaultResponse = {ok : false, msg : 'operation error'};
  
  controller.getImages = function(req,res){
    app.mode = req.mode;
    models.images.return(function(err, data){
      if(err){
        return res.send(defaultResponse);
      }
      return res.send({
        data : data,
        ok : true,
        msg : 'operation done'
      });
    });
  }
  
  controller.getUpImages = function(req,res){
    app.mode = req.mode;
    models.images.returnUp(parseInt(req.params.dateMs), function(err, data){
      if(err){
        return res.send(defaultResponse);
      }
      return res.send({
        data : data,
        ok : true,
        msg : 'operation done'
      });
    });
  }
  
  controller.getDownImages = function(req,res){
    app.mode = req.mode;
    models.images.returnDown(parseInt(req.params.dateMs), function(err, data){
      if(err){
        return res.send(defaultResponse);
      }
      return res.send({
        data : data,
        ok : true,
        msg : 'operation done'
      });
    });
  }
  
  controller.postImages = function(req,res){
    if(!req.body){
      return res.send(defaultResponse);
    }
    
    app.mode = req.mode;
    models.images.add(req.body, function(err, done){
      if(err){
        return res.send(defaultResponse);
      }
      return res.send({
        ok : true,
        msg : 'operation done'
      });
    });
  }
  
  return controller;
}




