var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.stats = function(req,res){
    res.header("Access-Control-Allow-Origin", "*");
    var games = req.body.games ? JSON.parse(req.body.games) : false;
    var user = req.body.user || false;
    var psw = req.body.password || false;
    var ip = help.getIp(req);
    var a = {msg : 'server can not save data', ok : false};
    console.log(user +' '+ ip+ ' sending ');
    
    if(user && psw){
      models.users.returnOne({user : user, psw : psw}, function(err, userData){
        if(userData && ( userData.ips == false || (userData.ips && userData.ips.indexOf(ip)!=-1) )){
          games.forEach(function(game) {
            game.user = user;
            if(game.win && game.matchId && game.items.length>0 && game.value){
              var win = game.win;
              var value = game.value;
              
              models.bets.returnByGame(game, function(err, bet){
                if(bet && (bet.win != win || bet.value != value)){
                  models.bets.updateOne(game);
                } else if(!bet) {
                  models.bets.addOne(game);
                }
              });
            }
          });
          res.send({msg : 'bet saved', ok : true});
        } else res.send(a);
      })
      
    } else res.send(a);
    
  }
  
  controller.markAsUnfinished = function(req,res){
    var id = req.params.id || false;
    if(id){
      models.matches.markAsUnfinished(id, function(err, data){
        res.redirect('/');
      })
    } else {
      res.redirect('/');
    }
    
  }
  
  controller.markAsFinished = function(req,res){
    var id = req.params.id || false;
    if(id){
      parsers.loungeMatch.return.bind({id:id})(function(err, lounge){
        models.matches.markAsFinished(lounge, function(err, data){
          res.redirect('/');
        })
      });
    } else {
      res.redirect('/');
    }
    
  }

  return controller;
}




