var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.updateMatches = function() {
    console.log('update matches started');
    async.parallel(
      {
        unfinished    : models.matches.returnUnfinished,
        matches       : parsers.loungeMatches.return,
        //redditMatches : parsers.redditMatches.return
      },
      function(err, result){
        result.matches = combineMatchData(result.matches, result.redditMatches);
        
        combineHltvData(result.matches, function(err, matches){
          //console.log(matches);
          result.matches = matches;
          var refresh = false;
          var match;
          var unfinished = help.arr.toObj(result.unfinished, 'matchId', 'obj');
          
          for(var i in result.matches){
            match = result.matches[i];
            //console.log(match);
            if(match.t1.rate != '0%'){
              if(unfinished[match.matchId]){                                        //unfinished match exists
                //console.log(match);
                models.matches.updateOne(match);
                if(match.finished){                                                 //unfinished match exists and match is finished
                  models.bets.updateFinished(match);
                  refresh = true;
                  console.log('need to finish the match and update the bets and statistic ' + match.matchId);
                } else if(match.live && !unfinished[match.matchId].stream) {
                  models.matches.updateStream(match.matchId);
                }
              } else {                                                              //unfinished match does not exists
                if(!match.finished){                                                //need to add match on database
                  console.log('there is new match. We have to add it ' + match.matchId);
                  models.matches.addOne(match);
                }
              }
            }
            
          }
          if(refresh){
            console.log('that should have to update');
            //help.delay(controller.updateBets, 5000);
            //help.delay(controller.updateStats, 10000);
            //refresh=false;
          }
        })
      }
    );
  }
  
  controller.combineMatches = function(cb){
    async.parallel(
      {
        matches       : parsers.loungeMatches.return,
        redditMatches : parsers.redditMatches.return
      },
      function(err, result){
        result.matches = combineMatchData(result.matches, result.redditMatches);
        
        combineHltvData(result.matches, cb);
      }
    );
  }
  
  var combineMatchData = function(matchesLounge, matchesReddit){
    var objs = help.arr.toObj(matchesReddit, 'matchId', 'obj');
    var match, teams, obj;
    for(var i in matchesLounge){
      if(objs[matchesLounge[i]['matchId']]){
        match = matchesLounge[i];
        obj = objs[match['matchId']];
        teams = obj.teams; delete obj.teams;
        /*parsers.hltvMatch.id = match.matchId;
        */
        
        for(var t in teams){
          if(help.str.isIn(t, match.t2.name, true)){
            match.t2.players = teams[t];
          } 
          if(help.str.isIn(t, match.t1.name, true)){
            match.t1.players = teams[t];
          }
          //console.log(t, match.t1.name, match.t2.name);
        }
        matchesLounge[i] = help.lodash.merge(match, obj);
        //console.log(matchesLounge[i]);
      }
    }
    return matchesLounge;
    //result.matches = help.arr.mergeByVal(result.matches, result.redditMatches, 'matchId');
  }
  
  var combineHltvData = function(matches, cb){
    matches = matches;
    async.eachSeries(matches, function(m, cb) {
      if(m.hltvId && m.hltvId.length>5){
        parsers.hltvMatch.matchId = m.hltvId;
        parsers.hltvMatch.return(function(err, data){
          
          /*console.log(data.t1.name, m.t1.name, help.str.isIn(data.t1.name, m.t1.name, true))
          console.log(data.t2.name, m.t2.name, help.str.isIn(data.t2.name, m.t2.name, true))*/
          if(err || !data || !data.t1){
            return cb(err || 'some problem with hltv');
          }
          if(help.str.isIn(data.t1.name, m.t1.name, true) || help.str.isIn(data.t2.name, m.t2.name, true)){
            m.maps = data.maps;
            m.t1.players = data.t1.players;
            m.t1.id = data.t1.id;
            m.t2.players = data.t2.players;
            m.t2.id = data.t2.id;
          } else if(help.str.isIn(data.t1.name, m.t2.name, true) || help.str.isIn(data.t2.name, m.t1.name, true)){
            var maps = [];
            for(var i in data.maps){
              var map = data.maps[i];
              var temp;
              if(map && map.score && map.score.t1){
                temp = map.score.t1;
                map.score.t1 = map.score.t2;
                map.score.t2 = temp;
              }
              maps.push(map);
            }
            m.maps = maps;
            m.t1.id = data.t2.id;
            m.t1.players = data.t2.players;
            m.t2.id = data.t1.id;
            m.t2.players = data.t1.players;
          }
          cb(err, data);
        });
      } else {
        cb(false);
      }
    }, function(err){
      cb(err, matches)
    });
  }
  
  controller.updateStats = function(){
    models.bets.collectStats(function(err, stats){
      console.log('stats updated');
      models.users.updateStats(stats);
    });
  }
  
  controller.updateBets = function(){
    models.bets.collectUnfinished(function(err, bets){
      bets = help.arr.toObj(bets, 'matchId');
      var matchIds = Object.keys(bets);
      
      models.matches.returnByMatchIds(matchIds, function(err, matches){
        console.log('bets updated');
        for(i in matches){
          if(matches[i].finished){
            models.bets.updateFinished(matches[i]);
          }
        }
      })
      
    })
  }
  
  controller.cacheData = function(){
    async.parallel(
      {
        unique : models.users.uniqueIps,
        requests : models.users.requests
      },
      function(err, result){
        app.sys.cache.requests = result.requests;
        app.sys.cache.unique = result.unique;
      }
    );
  }
  

  return controller;
}




