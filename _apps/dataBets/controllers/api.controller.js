var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  var parsers = app.parsers;
  
  controller.combinedMatches = function(req,res){
    app.controllers.data.combineMatches(function(err, data){
      res.send(app.render(data));
    })
  }
  
  controller.loungeMatch = function(req,res) {
    
    var id = req.params.id || false;
    
    async.parallel(
      {
        match : parsers.loungeMatch.return.bind({id:id})
      },
      function(err, result){
        models.matches.updateStream(id);
        res.send(app.render(result));
      }
    );
  }
  
  controller.hltvMatch = function(req,res) {
    
    var id = req.params.id || false;
    
    async.parallel(
      {
        match : parsers.hltvMatch.return.bind({id:id})
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  controller.loungeMatches = function(req,res) {
    
    async.parallel(
      {
       matches : parsers.loungeMatches.return
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  controller.redditMatches = function(req,res) {
    
    async.parallel(
      {
       matches : parsers.redditMatches.return
      },
      function(err, result){
        res.send(app.render(result));
      }
    );
  }
  
  controller.unique = function(req,res){
    res.send(app.render({
      requests : app.sys.cache.requests,
      unique : app.sys.cache.unique
    }));
  }
  
  controller.unfinished = function(req,res){
    var response = {ok:false};
    
    async.parallel(
      {
       unfinished : models.matches.returnUnfinished
      },
      function(err, result){
        if(!err){
          response = {
            ok : true,
            matches : result.unfinished,
            length : result.unfinished.length
          }
        }
        res.send(app.render(response));
      }
    );
  }
  
  controller.finished = function(req,res) {
    
    var user = req.params.user || false;
    
    async.parallel(
      {
        finished : models.bets.returnFinishedByUser.bind({user : user})
      },
      function(err, result){
        result = result.finished;
        result.bets = help.arr.toObj(result.bets, 'matchId', 'obj');
        var bet, matchId, match;
        
        for(var i in result.matches){
          match = result.matches[i];
          matchId = match.matchId;
          bet = result.bets[matchId];
          match.value = help.num.toFixed(bet.value);
          match.items = bet.items;
          
          if(match.winner && match.winner == bet.win){
            match.result = 1;
            match.balance = help.num.toFixed(bet.pot);
          } else if(bet.winner == 'none') {
            match.result = 0;
            match.balance = 0;
          } else {
            match.result = -1;
            match.balance = -help.num.toFixed(bet.value);
          }
          result.matches[i] = match;
        }
        
        res.header("Access-Control-Allow-Origin", "*");
        res.send(app.render(result.matches));
      }
    );
  }
  
  return controller;
}




