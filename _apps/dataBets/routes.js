var help = require(process.cwd() + '/lib/help.js');

module.exports = function(app){
  
  var controllers = app.controllers;
  var middles = app.sys.apps.dataWdpwp.middles;
  
  
  //controllers.data.cacheData();
  
  //data autoUpdate
  

  if(app.env == 'development'){
    this.get('/api/lounge/match/(:id)?', controllers.api.loungeMatch);
    this.get('/api/lounge/matches', controllers.api.loungeMatches);
    this.get('/api/reddit/matches', controllers.api.redditMatches);
    
    this.get('/api/combined/matches', controllers.api.combinedMatches);
    
    this.get('/api/hltv/match/(:id)?', controllers.api.hltvMatch);
    
    controllers.data.updateMatches();
    //controllers.data.updateBets();
    //controllers.data.updateStats();
  }
  
  //moder routes
  this.get('/m/(:id)/unfinished', middles.check.moder, controllers.page.markAsUnfinished);
  this.get('/m/(:id)/finished', middles.check.moder, controllers.page.markAsFinished);

  
  if(app.env == 'production'){
    console.log('MATCH TRACKING STARTED');
    help.repeat(600000, controllers.data.updateMatches);
    controllers.data.updateMatches();
  }
  
  //pub api
  this.get('/api/unique', controllers.api.unique);
  this.get('/api/unfinished', controllers.api.unfinished);
  this.get('/api/finished/:user', controllers.api.finished);
  
  this.post('/stats', middles.log.route, controllers.page.stats);
  
  return this;
}