var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.url = 'http://csgolounge.com/match?m=';
  Parser.matchId = '1685';
  
  Parser.return = function(cb){
    
    Parser.matchId = this.id || Parser.matchId;
    Parser.basic(function(err, basic){
      //cb(err, {basic : basic, moded : Parser.modify(basic)});
      cb(err, Parser.modify(basic));
    })
      
  }
  
  Parser.modify = function(match){
    //console.log(match);
    var moded = help.obj.clone(match);
    var winner = moded.t1.name.indexOf('win')!=-1 
      ? 't1' 
      : moded.t2.name.indexOf('win')!=-1
        ? 't2'
        : false
    
    if(winner){
      moded.finished  = true;
      moded.winner    = winner;
      moded.t1.name   = moded.t1.name.replace(' (win)', '');
      moded.t2.name   = moded.t2.name.replace(' (win)', '');
    }
   
    moded.date = help.date.fromNow(moded.left, 'standart');
    
    moded.startMs = help.date.return('ms', moded.date+' '+moded.start.split(' ')[0]);
    moded.startDate = help.date.return('full', moded.date);
    
    return moded;
  }
  
  Parser.basic = function(cb){
    
    //console.log(Parser.url + Parser.matchId);
    Parser.ask(Parser.url + Parser.matchId, 'dom', function(err, $){
      if(err) return cb(err);
      
      var cont = $('.match-box').eq(0);
      var halfs = cont.find('.col.s4');
      var teama = cont.find('.team-a').eq(0);
      var teamb = cont.find('.team-b').eq(0);
      var stream = $('iframe');
      var object = $('object');
      var match = {};
      
      var match = {
        matchId : Parser.matchId,
        left : halfs.eq(0).text(),
        games : halfs.eq(1).text(),
        start : halfs.eq(2).text().replace(/[^\d,:]+/g, ''),
        t1 : {
          name : teama.find('.match-team-name').text(),
          rate : teama.find('.match-team-percent').text(),
          img : teama.find('.team').eq(0).css('background').replace("url('",'').replace("')",'').replace("\\",'')
        },
        t2 : {
          name : teamb.find('.match-team-name').text(),
          rate : teamb.find('.match-team-percent').text(),
          img : teama.find('.team').eq(0).css('background').replace("url('",'').replace("')",'').replace("\\",'')
        },
        stream : stream.length ? stream.eq(0).attr('src') : false
      }
      
      if(!match.stream){
        match.stream = object.length ? object.eq(0).attr('data') : false
      }
      
      cb(err, match);
    });
  }
  
  return Parser;
}