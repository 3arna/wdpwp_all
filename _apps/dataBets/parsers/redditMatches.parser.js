var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.url = 'http://www.reddit.com/r/csgobetting/search.json?q=flair:match%20OR%20flair:finished&sort=new&restrict_sr=on';
  Parser.hltvId = false;
  
  Parser.return = function(cb){
    Parser.basic(function(err, basic){
      //cb(err, {moded : Parser.modify(basic), basic : basic});
      cb(err, Parser.modify(basic));
    })
  }
  
  Parser.modify = function(matches){
    if(!matches){return matches};
    matches = matches.data.children;
    var match = {}, m={}, html, links=[], a, $, temp = [];
    for(var i in matches){
      
      m = matches[i].data;
      $ = Parser.toDom(help.str.replaceSpecials(m.selftext_html));
      
      var info = Parser.extractInfo($);
      var title = m.title.replace('Title: ', '');
      var teams = Parser.extractTeamsFromInfo(info, title);
      var maps = Parser.extractMaps(info);
      var matchId = $("a:contains('CSGL')").length ? $("a:contains('CSGL')").eq(0).attr('href').split('?m=')[1] : false;
      var hltv = Parser.extractHltv($);
      
      match = {
        matchId :  matchId,
        links : Parser.extractLinks($),
        type : Parser.extractType($),
        /*info : info,*/
        maps : maps.length<1 ? 'TBA' : maps,
        title : title,
        teams : teams
      }
      match.links.push({
        name : 'reddit',
        url : m.url
      });
      
      if(hltv){
        match.links.push({name:'hltv', url: hltv.url});
        match.hltvId = hltv.id;
      }
      
      temp.push(match);
    }
    return temp;
  }
  
  Parser.extractLinks = function($){
    var links = [], txt, hltvId=false, name, url;
    var a = $('p').eq(0).find('a');
    for(var n=0;n<a.length;n++){
      txt = $('a').eq(n).text();
      if(!help.str.isIn(txt, ['Straw', 'Trade', 'CSGL', 'HLTV', 'Stream'])){
        name =  $('a').eq(n).text().toLowerCase();
        url = $('a').eq(n).attr('href');
        links.push({
          name : name,
          url : url
        })
      }
    }
    return links;
  }
  
  Parser.extractHltv = function($){
    var a = $('a:contains("HLTV")').eq(0);
    if(a.length && a.text() == 'HLTV'){
      var url = a.attr('href');
      return {url : url, id : help.str.onlyNumbers(url.split('-')[0])}
    }
    return false;
  }
  
  Parser.extractType = function($){
    var txt = $("p:contains('LAN')").text().split(':')[1];
    return help.str.isIn(txt, 'LAN') ? 'LAN' : 'Online';
  }
  
  Parser.extractTeamsFromInfo = function(info, title){
    title=title.split('|')[0];
    var l = info.length;
    var teams = {};
    for(var i in info){
      if(help.str.isIn(title, i, true)){
          teams[help.str.clear(i, ['sports', 'Sports'])] = help.str.isIn(info[i], ',')
            ? help.arr.removeEmpty(info[i].split(','))
            : help.arr.removeEmpty(info[i].split(' '))
      }
    }
    
    /*help.arr.removeEmpty(info[l-1].value.split(','))*/
    
    return teams;
  }
  
  Parser.extractInfo = function($){
    var info = {};
    var elem,txt;
    $('del').remove();
    p = $('p');
    for(var n=0;n<p.length;n++){
      elem = $('p').eq(n);
      if(!elem.find('del').length && !elem.find('em').length){
        txt = help.str.clear($('p').eq(n).text(), ['expected', 'and', 'Expected']).split(': ');
        if(txt[1]){
          info[help.str.onlyLettersAndNumbers(txt[0])] = txt[1];
        }
      }
    }
    
    delete info['Time'];
    delete info['LANOnline'];
    delete info['TournamentLeague'];
    delete info['Links'];
    delete info['Date'];
    //delete info['Maps'];
    return info;
  }
  
  Parser.extractMaps = function(info){
    var maps = false;
    if(info['Maps']){
      maps = info['Maps'];
    } else if(info['Map']){
      maps = info['Map'];
    }
    return maps ? help.str.onlyLetters(help.str.clear(maps, ['BO', '(', ')', 'Best of', 'All'])) : 'TBA';
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'json', function(err, json){
      if(err) return cb(err);
      cb(err, json);
    });
  }
  
  return Parser;
}