var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.url = 'http://csgolounge.com';
  
  Parser.return = function(cb){
    Parser.basic(function(err, basic){
      //cb(err, {basic : basic, moded : Parser.modify(basic)});
      cb(err, Parser.modify(basic));
    })
  }
  
  Parser.modify = function(matches){
    var match = {}, temp = [];
    for(var i in matches){
      match = matches[i];
      if(match){
        
        match.t1.cof = help.num.toFixed(parseInt(match.t2.rate) / parseInt(match.t1.rate));
        match.t2.cof = help.num.toFixed(parseInt(match.t1.rate) / parseInt(match.t2.rate));
        match.live=false;
        
        var time = match.left;
        if(time.indexOf('LIVE')!=-1){
          match.live = true;
          match.startMs = help.date.return('ms') - help.date.strToMs(match.left);
          match.left = parseInt(match.left)
          + ' ' 
          + (time.indexOf('minute')!=-1 
              ? 'minute' 
              : time.indexOf('hour')!=-1 
                ? 'hour' 
                : time.indexOf('day')!=-1 
                  ? 'day'
                  : 'second') + ' left';      } else if(!match.finished){
          match.left = parseInt(match.left)
          + ' ' 
          + (time.indexOf('minute')!=-1 
              ? 'minute' 
              : time.indexOf('hour')!=-1 
                ? 'hour' 
                : time.indexOf('day')!=-1 
                  ? 'day'
                  : 'second') + ' left';
          match.startMs = help.date.return('ms') + help.date.strToMs(match.left);
        } else {
          delete match.time;
          delete match.left;
        }
        temp.push(match);
      }
    }
    return temp;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'dom', function(err, $){
      if(err) return cb(err);
      var match = {}, matches = [];
      var games = $('.matchmain');
      games.each(function(){
        match = false;
        var game = $(this);
        if(game.find('.matchleft').length){
          var href = game.find('a').attr('href');
          var type = href.indexOf('match') != -1 ? 'match' : false;
          
          
          if(type){
            match = {
              event : game.find('.eventm').eq(0).text(),
              left : game.find('.whenm').eq(0).text(),
              matchId : game.find('a').attr('href').split('?m=')[1],
              finished : game.find('.notavailable').length ? true : false,
              t1 : {
                name : game.find('.teamtext').eq(0).find('b').text(),
                rate : game.find('.teamtext').eq(0).find('i').text(),
                img : game.find('.team').eq(0).css('background') 
                  && game.find('.team').eq(0).css('background').replace("url('",'').replace("')",'').replace("\\",'')
              },
              t2 : {
                name : game.find('.teamtext').eq(1).find('b').text(),
                rate : game.find('.teamtext').eq(1).find('i').text(),
                img : game.find('.team').eq(1).css('background') 
                  && game.find('.team').eq(1).css('background').replace("url('",'').replace("')",'').replace("\\",'')
              }
            }
            if(!match.aviable && game.find('img').length){
              match.winner = game.find('.team').eq(0).find('img').length ? 't1' : 't2';
            }
          }
        
          matches.push(match);
        }
      });
      cb(err, matches);
    });
  }
  
  return Parser;
}