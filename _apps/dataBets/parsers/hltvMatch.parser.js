var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.url = 'http://www.hltv.org/match/';
  
  Parser.return = function(cb){
    
    Parser.matchId = this.id || Parser.matchId;
    Parser.basic(function(err, basic){
      //cb(err, {basic : basic, moded : Parser.modify(basic)});
      cb(err, basic);
    })
      
  }
  
  Parser.modify = function(match){
    var moded = help.obj.clone(match);
    
    var winner = moded.t1.name.indexOf('win')!=-1 
      ? 't1' 
      : moded.t2.name.indexOf('win')!=-1
        ? 't2'
        : false
    
    if(winner){
      moded.finished  = true;
      moded.winner    = winner;
      moded.t1.name   = moded.t1.name.replace(' (win)', '');
      moded.t2.name   = moded.t2.name.replace(' (win)', '');
    }
   
    moded.date = help.date.fromNow(moded.left, 'standart');
    
    //moded.startMs = help.date.return('ms', moded.date+' '+moded.start.split(' ')[0]);
    //moded.startDate = help.date.return('full', moded.date);
    
    return moded;
  }
  
  Parser.basic = function(cb){
    var url = Parser.url + Parser.matchId + '-';
      if(Parser.matchId.length>4){
      /*var opts = {
        headers : {
          'User-Agent':       'Android',
          'Content-Type':     'application/x-www-form-urlencoded'
        }
      }*/
      Parser.ask(url, 'dom', function(err, $){
        //console.log(url);
        if(err || !$) return cb(err);
        
        var match = false;
        var main = $('.centerFade div').eq(0);
        err = 'problem with hltv parsing';
        if(main){
          err = false;
          var headers = $('.hotmatchboxheader');
          var boxes = main.children('div');
          
          var temp = [], text, header;
          boxes.each(function(i, elem) {
            header = $(this).find('.headertext');
            
            if(header){
              text = header.text();
              if(text.indexOf('Maps')!=-1){
                temp.maps = i;
              } else if(text.indexOf('lineup')!=-1){
                temp.t2 = i;
              }
            }
          });
          
          var teams = extractTeams(temp.t2, boxes);
          var finished = main.find('div').eq(2).text() == 'Match over' ? true : false;
          var match = {
            hltvId : Parser.matchId,
            finished : finished,
            t1 : teams.t1,
            t2 : teams.t2,
            maps : extractMaps(temp.maps, boxes)
          }
        }
        
        cb(err, match);
      }, 'get');
    } else {
      cb('problem with hltvId');
    }
  }
  
  var extractMaps = function(num, boxes){
    var box = boxes.eq(num).find('.hotmatchbox').eq(0);
    var divs = box.children('div');
    var maps = [];
    var map;
    var scores;
    var s;
    divs.each(function(i, elem) {
      elem = divs.eq(i);
      
      if(elem.html().length && elem.text().indexOf('Best of')==-1){
        if(elem.find('img').length){
          map = {
            name : elem.find('img').eq(0).attr('src').split('/')[6].replace('.png', '')
          }
          map.score = extractScores(divs.eq(i+1));
          
          map.score && map.score.t1 >= 16 ? map.winner = 't1' : false;
          map.score && map.score.t2 >= 16 ? map.winner = 't2' : false;
          
          maps.push(map);
        }
      }
    })
    return maps;
  }
  
  var extractScores = function(scores){
    var data = false;
    if(scores.text().length){
      data = {
        t1 : {
          score : parseInt(scores.find('span').eq(0).text())
        },
        t2 : {
          score : parseInt(scores.find('span').eq(1).text())
        }
      };
      var s = scores.find('span').eq(2);
      if(help.str.isIn(s.attr('style'), 'red')) data.t1.t = parseInt(s.text());
      if(help.str.isIn(s.attr('style'), 'blue')) data.t1.ct = parseInt(s.text());
      
      s = scores.find('span').eq(3);
      if(help.str.isIn(s.attr('style'), 'red')) data.t2.t = parseInt(s.text());
      if(help.str.isIn(s.attr('style'), 'blue')) data.t2.ct = parseInt(s.text());
      
      s = scores.find('span').eq(4);
      if(help.str.isIn(s.attr('style'), 'red')) data.t1.t = parseInt(s.text());
      if(help.str.isIn(s.attr('style'), 'blue')) data.t1.ct = parseInt(s.text());
      
      s = scores.find('span').eq(5);
      if(help.str.isIn(s.attr('style'), 'red')) data.t2.t = parseInt(s.text());
      if(help.str.isIn(s.attr('style'), 'blue')) data.t2.ct = parseInt(s.text());
    }
    return data;
  }
  
  var extractTeams = function(num, boxes){
    var t1div = boxes.eq(num-3);
    var t2div = boxes.eq(num);
    // help.str.clear(t1div.find('.headertext').eq(0).text(), ['lineup', '(expected)'])
    var data = false;
    if(boxes.eq(0).find('a').eq(0).attr('href')){
      data = {
        t1 : {
          name : boxes.eq(0).find('a').eq(0).text(),
          id : boxes.eq(0).find('a').eq(0).attr('href').split('&')[1].split('=')[1],
          players : extractPlayers(boxes.eq(num-2))
        },
        t2 : {
          name : boxes.eq(0).find('a').eq(1).text(),
          id : boxes.eq(0).find('a').eq(1).attr('href').split('&')[1].split('=')[1],
          players : extractPlayers(boxes.eq(num+1))
        }
      }
    }
    return data;
  }
  
  var extractPlayers = function(box){
    var divs = box.children('div');
    var players = [];
    var ids = [];
    divs.each(function(i, elem) {
      elem = divs.eq(i);
      var player = {};
      if(elem.text().length>1){
        player.name = elem.find('span').eq(0).text();
        if(elem.find('a').length){
          player.name = elem.find('span').eq(0).text()
          player.id = parseInt(elem.find('a').eq(0).attr('href').split('&')[1] && elem.find('a').eq(0).attr('href').split('&')[1].split('=')[1])
        } else {
          player.name = help.str.onlyLettersAndNumbers(elem.text());
        }
        players.push(player);
      }
    })
    return players;
  }
  
  return Parser;
}