var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.matches = Model.col({col:'Matches'});
  Model.cols.bets = Model.col({col:'Bets'});
  
  Model.updateOne = function(data, cb){
    data.updated = help.date.return('ms');
    Model.cols.matches.update({matchId : data.matchId}, {$set : data}, {}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  Model.returnByMatchIds = function(matchIds, cb){
    Model.cols.matches.find({ matchId : {$in : matchIds}}).lean().exec(function(err, data) {
      cb(err, data);
    });
  }
  
  Model.updateStream = function(matchId, cb){
    Parsers.loungeMatch.matchId = matchId;
    Parsers.loungeMatch.return(function(err,loungeMatch){
      Model.cols.matches.update({matchId : matchId}, {stream : loungeMatch.stream}, {}, function(err, data) {
        cb ? cb(err,data) : false;
      })
    })
  }
  
  Model.addOne = function(data, cb){
    Parsers.loungeMatch.matchId = data.matchId;
    Parsers.loungeMatch.return(function(err,loungeMatch){
      data = help.obj.merge(loungeMatch, data);
      data.updated = help.date.return('ms');
      Model.cols.matches
        ? Model.cols.matches.update({matchId : data.matchId}, data, {upsert : true}, function(err, data) {
            cb ? cb(err,data) : false;
          })
        : cb ? cb(err,data) : false;
    });
  }
  
  Model.markAsUnfinished = function(matchId, cb){
    Model.cols.matches.update({matchId : matchId}, {finished : false}, {upsert : false}, function(err, data) {
      Model.cols.bets.update({matchId : matchId}, {$unset: { winner: ""}}, {upsert : false}, function(err, done){
        cb ? cb(err,done) : false;
      })
    })
  }
  
  Model.markAsFinished = function(data, cb){
    var winner = data.winner || false;
    Model.cols.matches.update({matchId : data.matchId}, {$set : {finished : true, winner : winner}}, {upsert : false}, function(err, d) {
      Model.cols.bets.update({matchId : data.matchId}, {$set: { winner: winner || 'none'}}, {upsert : false, multi : true}, function(err, done){
        cb ? cb(err,done) : false;
      })
    })
  }
  
  Model.returnUnfinished = function(cb){
    if(Model.off){return cb(false, false)}
    Model.cols.matches.find({$or : [{"finished" : {$exists : false}}, {"finished" : false}]}).sort({startMs : 1}).lean().exec(function(err, data) {
      cb(err, data);
    });
  }
  
  
  return Model;
}