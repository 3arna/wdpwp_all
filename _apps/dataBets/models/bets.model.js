var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.bets = Model.col({col:'Bets'});
  Model.cols.matches = Model.col({col:'Matches'});
  
  Model.updateFinished = function(match, cb){
    
    var data = { 
      matchId : match.matchId,
      winner : match.winner || 'none',
      updated : help.date.return('ms')
    }
    
    Model.returnByMatchId(match.matchId, function(err, bets){
      bets.forEach(function(bet){
        data.pot = (bet.value * match[bet.win].cof).toFixed(2);
        data.user = bet.user;
        Model.updateOne(data);
      });
    })
  }
  
  Model.collectUnfinished = function(cb){
    Model.cols.bets.find({winner : {$exists : false}}).lean().exec(cb);
  }
  
  Model.collectStats = function(cb){
    Model.return(function(err, bets){
      var stats = {}, bet;
      for(var i in bets){
        bet = bets[i];
        
        stats[bet.user] 
          ? false 
          : stats[bet.user] = {value : 0, win : 0, loss : 0, count : 0, countW : 0, countL : 0, balance : 0, last : 0, updated : 0};
        
        if(bet.value){
          
          bet.winner ? stats[bet.user].count ++ : false;
          stats[bet.user].updated = bet.updated;
          stats[bet.user].value += help.num.toFixed(bet.value);
          
          if(bet.winner && bet.winner!='none'){
            if(bet.winner == bet.win){
              var pot = help.num.toFixed(bet.pot) || 0;
              stats[bet.user].countW += 1;
              stats[bet.user].win += pot;
              stats[bet.user].balance += pot;
              stats[bet.user].last = pot;
            } else {
              var value = help.num.toFixed(bet.value);
              stats[bet.user].countL += 1;
              stats[bet.user].loss += value;
              stats[bet.user].balance -= value;
              stats[bet.user].last = -value;
            }
          }
        }
      }
      console.log(stats['Etas']);
      cb(err, stats);
    })
  }
  
  Model.return = function(cb){
    Model.cols.bets.find().sort({updated : 1}).lean().exec(cb);
  }
  
  Model.returnByMatchId = function(matchId, cb){
    Model.cols.bets.find({matchId : matchId}).lean().exec(function(err, bets){
      cb(err, bets);
    });
  }
  
  Model.returnByGame = function(bet, callback){
    Model.cols.bets.findOne({matchId : bet.matchId, user : bet.user}, function(err, b) {
      callback(err, b);
    }).lean();
  }
  
  Model.addOne = function(bet, cb){
    bet.date = help.date.return('ms');
    bet.updated = bet.date;
    Model.cols.bets.update({matchId : bet.matchId, user : bet.user}, {$set : bet}, {upsert : true}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  Model.updateOne = function(bet, cb){
    bet.updated = help.date.return('ms');
    Model.cols.bets.update({matchId : bet.matchId, user : bet.user}, {$set : bet}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  Model.returnFinishedByUser = function(cb){
    if(Model.off && !user){return cb(false, false)}
    
    var user = this.user;
    var find = {user : user, winner : {$exists : true}};
    var sort = {date : -1, updated : -1};
    var pick1 = {
      _id : 0,
      "t1.name": 1, 
      "t1.rate": 1,
      "t2.name": 1,
      "t2.rate": 1,
      date : 1,
      winner : 1,
      matchId : 1,
      updated : 1
    }
    
    var pick2 = {
      _id : 0,
      matchId : 1,
      win : 1,
      pot : 1,
      value : 1,
      winner : 1,
      items : 1
    }
    
    Model.cols.bets.find(find, pick2).sort({updated : -1}).lean().exec(function(err, bets){
      var matchIds = help.arrObjs.collectVars(bets, 'matchId');
      Model.cols.matches.find({matchId : {$in : matchIds}}, pick1).sort(sort).lean().exec(function(err, matches){
        cb(err, {matches : matches, bets : bets});
        //cb(err, help.arrObjs.addArrToObj(matches, bets, 'matchId', 'bets'));
      })
    })
  }
  
  
  return Model;
}