var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Parsers = app.parsers;
  Model.cols.users = Model.col({col:'Users'});
  var LogsCol = Model.col({col:'Routes', db:'logs'})
  
  Model.updateStats = function(stats){
    if(stats){
      for(var user in stats){
        Model.updateOneStats(user, stats[user])
      }
    }
  }
  
  Model.ipExists = function(ip, cb){
    if(Model.off){return cb(false, false)}
    LogsCol && LogsCol.count({"ip" : ip}, cb);
  }
  
  Model.uniqueIps = function(cb){
    if(Model.off){return cb(false, false)}
    LogsCol && LogsCol.distinct("ip", function(err, ips){
      cb(err, ips.length);
    });
  }
  
  Model.requests = function(cb){
    if(Model.off){return cb(false, false)}
    LogsCol && LogsCol.count({}, function(err, data){
      cb(err, data);
    });
  }
  
  Model.return = function(cb){
    Model.cols.users.find().lean().exec(cb);
  }
  
  Model.returnOne = function(user, cb){
    Model.cols.users.findOne(user).lean().exec(cb);
  }
  
  Model.returnBySteamId = function(cb){
    var steamId = this.id || false;
    Model.cols.users.findOne({steamId : steamId}).lean().exec(cb);
  }
  
  Model.updateOne = function(data, cb){
    Model.cols.users.update({steamId : data.steamId}, {$set : data}, {upsert : false}, function(err, d) {
      cb ? cb(err,d) : false;
    });
  }
  
  Model.updateOneStats = function(user, stats, cb){
    Model.cols.users.update({user : user}, {$set : {stats : stats}}, {upsert : false}, function(err, data) {
      cb ? cb(err,data) : false;
    });
  }
  
  Model.addOne = function(user, cb){
    if(Model.off){return cb(false,false)}
    var nick = help.str.onlyLettersAndNumbers(user.nick);
    user = {
      steamId : user.steamId,
      user : nick,
      name : user.nick,
      ips : false,
      registered : help.date.return('ms'),
      psw : help.str.random(7)
    }
    Model.cols.users.update({steamId : user.steamId}, {$set : user}, {upsert : true}, function(err, done){
      cb(err, user);
    });
  }
  
  
  return Model;
}