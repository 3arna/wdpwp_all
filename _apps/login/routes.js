var help = require(process.cwd() + '/lib/help.js');

module.exports = function(app){
  
  var controllers = app.controllers;
  var middles = app.sys.apps.dataWdpwp.middles;
  
  this.get('/login', middles.log.route, controllers.page.steamRed);
  this.get('/steam/auth', middles.log.route, middles.check.user, controllers.page.profileCashe);
  this.get('/logout', middles.log.route, controllers.page.logout);
  
  return this;
}