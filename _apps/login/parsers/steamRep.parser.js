var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    async     = require('async'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamId = '76561198040934972';
  Parser.tm = false;
  Parser.url = 'http://steamrep.com/search?q=';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    
    async.parallel(
      {
        profile : Parser.profile,
        reports : function(callback){
          Parser.basic(function(err, basic){
            Parser.tm = Parser.extractTm(basic);
            Parser.reports(callback);
          });
        }
      },
      function(err, result){
        result.profile.srReports = parseInt(result.reports);
        cb(err, result.profile);
      }
    );
  }
  
  Parser.returnUrl = function(obj, type){
    type = type || false;
    var steamId = obj ? obj.steamId || Parser.steamId : Parser.steamId;
    var tm = obj ? obj.tm || Parser.tm : Parser.tm;
    var url = 'http://steamrep.com/search?q='+steamId;
    
    switch(type){
      case "profile":
        url = 'https://steamrep.com/api/beta3/reputation/'+steamId+'?json=1&extended=1';
        break;
      case "reports":
        url = 'http://steamrep.com/api/beta3/reports/'+steamId+'?tm='+tm;
        break;
    }
    
    return url;
  }
  
  Parser.modify = function(json){
    json = json.steamrep;
    if(json.flags.status == 'exists'){
      json = {
        steamId32 : json.steamID32,
        steamId : json.steamID64,
        srBan : json.reputation.summary == 'SCAMMER' ? true : false,
        srWarn : json.reputation.summary == 'CAUTION' ? true : false,
        customUrl : json.customurl,
        tradeBan : json.tradeban == '2' ? true : false,
        vacBan : json.vacban != '0' ? true : false
      }
    } else {
      json = false;
    }
    return json;
  }
  
  Parser.extractTm = function($){
    var data = $('#profiledatabox script').eq(0).html().replace(' ', '').split(';');
    for(var i in data){
      if(help.str.isIn(data[i], 'g_TimeStamp2')){
        data = data[i].replace("'", "").split(' = ')[1].replace("'", '');
        break;
      }
    }
    return data;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.returnUrl(), 'dom', function(err, json){
      if(err) return cb(err);
      cb(err, json);
    });
  }
  
  Parser.reports = function(cb){
    Parser.ask(Parser.returnUrl(false, 'reports'), 'json', function(err, json){
      if(err){ 
        console.log('ERROR with SR reports parsing');
        return cb(false, false);
      }
      return cb(err, json.reports);
    });
  }
  
  Parser.profile = function(cb){
    Parser.ask(Parser.returnUrl(false, 'profile'), 'json', function(err, json){
      if(err) return cb(err);
      cb(err, Parser.modify(json));
    });
  }
  
  return Parser;
}