var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamId = '0';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      var moded = Parser.modify(basic);
      Parser.steamLvl(function(err, steamLvl){
        moded.steamLvl = steamLvl;
        cb(err, moded);
      });
      //cb(err, {moded : Parser.modify(basic)});
    })
      
  }
  
  Parser.returnUrl = function(steamId){
    steamId = steamId || Parser.steamId;
    return 'http://steamcommunity.com/profiles/'+steamId+'/?xml=1';
    /*'http://steamcommunity.com/id/Majasqzi/friends?xml=1'*/
    /*http://api.steampowered.com/IPlayerService/GetSteamLevel/v1?steamid=76561198064835471&key=F26D77C26645814EA726350E83E1A243*/
    /*http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=F26D77C26645814EA726350E83E1A243&steamids=76561198065634960*/
    
  };
  
  Parser.modify = function(match){
    if(!match || !match.profile){return false};
    var moded = match.profile;
    var groups = Parser.extractGroupesData(moded.groups);
    var profile = {
      groups : groups.count,
      primaryGroup : groups.primary,
      steamId : Parser.steamId,
      vacBan : moded.vacBanned ? true : false,
      tradeBan : moded.tradeBanState == 'None' ? false : true,
      avatar : moded.avatarIcon,
      private : moded.privacyState == 'public' ? false : true,
      regDate : help.date.return('ms', moded.memberSince),
      nick : moded.steamID,
      memberSince : moded.memberSince
    }
    
    typeof moded.customURL != 'string' ? delete moded.customURL : profile.customUrl = moded.customURL;
    typeof moded.location != 'string' ? delete moded.location : profile.location = moded.location;
    
    
    delete moded.summary;
    //delete moded.onlineState;
    //delete moded.stateMessage;
    //delete moded.hoursPlayed2Wk;
    delete moded.avatarMedium;
    delete moded.avatarFull;
    //delete moded.steamRating;
    delete moded.headline;
    //delete moded.realname;
    //delete moded.groups;
    delete moded.mostPlayedGames;
    delete moded.inGameInfo;
    delete moded.inGameServerIP;
    //delete moded.visibilityState;
    
    return profile;
  }
  
  Parser.extractGroupesData = function(groupsData){
    groupsData = groupsData ? groupsData.group : false;
    if(!groupsData){return false;}
    var groups = {};
    groups.count = groupsData.length;
    for(var i in groupsData){
      if(groupsData[i].isPrimary){groups.primary = groupsData[i].groupID64; break;}
    }
    return groups;
  }
  
  Parser.basic = function(cb){
    
    Parser.ask(Parser.url, 'xmlToObj', function(err, html){
      if(err && Parser.timeOut){
        Parser.timeOut--;
        console.log('loading try times', Parser.timeOut);
        Parser.basic(cb)
      }
      else {
        Parser.timeOut = Parser.maxTimeOuts;
        cb(err, html);
      }
    });
  }
  
  Parser.steamLvl = function(cb){
    var url = 'http://api.steampowered.com/IPlayerService/GetSteamLevel/v1?'+
    'steamid='+Parser.steamId+'&key=F26D77C26645814EA726350E83E1A243';
    Parser.ask(url, 'json', function(err, json){
      if(err || !json){ return cb(err, 'problem with steam profile lvl')};
      cb(err, json.response.player_level);
    });
  }
  
  return Parser;
}