var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    P         = require(paths.lib + '/fbf/parser');
    

module.exports = function(app){
  
  var Parser = new P(app);
  Parser.steamId = '76561198040934972';
  
  Parser.return = function(cb){
    Parser.steamId = this.id || Parser.steamId;
    Parser.url = Parser.returnUrl();
    Parser.basic(function(err, basic){
      cb(err, Parser.modify(basic));
      //cb(err, Parser.modify(basic));
    })
      
  }
  
  Parser.returnUrl = function(steamId){
    steamId = steamId || Parser.steamId;
    http://steamcommunity.com/id/timvisee/games?tab=all&xml=1&l=english
    return 'http://steamcommunity.com/profiles/'+steamId+'/games?tab=all&xml=1&l=english'
  };
  
  Parser.modify = function(games){
    if(!games){return false}
    var moded = {}, list = games.gamesList.games.game;
    moded.count = list.length;
    moded.hoursPlayed = help.arrObjs.sum(list, 'hoursOnRecord');
    moded.hoursPlayed2W = help.arrObjs.sum(list, 'hoursLast2Weeks');
    moded.ids = help.arrObjs.collectVars(list, 'appID');
    
    return moded;
  }
  
  Parser.basic = function(cb){
    Parser.ask(Parser.url, 'xmlToObj', function(err, html){
      if(err) return cb(err);
      cb(err, html);
    });
  }
  
  return Parser;
}