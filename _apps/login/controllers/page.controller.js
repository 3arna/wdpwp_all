var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    auth        = require(paths.lib + 'auth/steam'),
    async       = require('async');
      
module.exports = function(app){
  var msg = 'Your account is banned please try to login again with other account <a href="/">home page</a>';
  //controllers
  var controller = {};
  controller.steamProfile = app.sys.apps.dataSteam.controllers.data.steamProfile;
  //models
  var models = app.models;
  models.profile = app.sys.apps.dataWdpwp.models.profiles;
  models.history = app.sys.apps.dataWdpwp.models.history;
  models.betUsers = app.sys.apps.dataBets.models.users;
  //parsers
  var parsers = app.parsers;
  var steamAuth = new auth();
  
  controller.steamRed = function(req,res) {
    if(req.session.profile) return res.send(app.sys.msgs.error);
    req.session.backUrl = req.header('Referer') || '/';
    steamAuth.redirect(req,res);
  }
  
  controller.logout = function(req,res){
    delete req.session.profile;
    res.redirect('/');
  }
  
  controller.profileCashe = function(req,res) {
    if(req.steamId){var id = req.steamId}
    else return res.send(app.sys.msgs.error);
    
    async.parallel(
      {
        betsProfile : models.betUsers.returnBySteamId.bind({id : id}),
        wdpwpProfile : models.profile.returnOne.bind({id : id}),
        steamProfile : controller.steamProfile.bind({id : id})
      },
      function(err, result){
        
        if(result.wdpwpProfile && result.steamProfile){
          addChanges(result.wdpwpProfile, result.steamProfile);
        } else if(result.steamProfile){
          models.profile.updateOne(result.steamProfile);
        }
        
        if(result.betsProfile){
          req.session.profile = result.betsProfile;
          res.redirect(req.session.backUrl || '/');
        } else {
          models.betUsers.addOne(result.steamProfile, function(err, user){
            req.session.profile = user;
            res.redirect(req.session.backUrl || '/');
          })
        }
      }
    )
  }
  
  var addChanges = function(dbProfile, parsedProfile){
    var wdpwp = dbProfile;
    var steam = parsedProfile;
    //console.log(wdpwp, steam);
    var diff = help.obj.diff(wdpwp, steam);
    if(Object.keys(diff).length>1){
      diff.steamId = dbProfile.steamId;
      //console.log(diff);
      models.history.addOne(diff);
    }
  }

  return controller;
}