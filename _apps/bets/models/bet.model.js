var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Col = Model.col({col:'Matches'});
  var ColBets = Model.col({col:'Bets'});
  
  Model.returnByUser = function(cb){
    if(Model.off && !user){return cb(false, false)}
    
    var user = this.user;
    var find = {user : user, winner : {$exists : false}};
    var sort = {startMs : 1};
    
    switch(this.type){
      case 'finished':
        find = {user : user, winner : {$exists : true}};
        sort = {date : -1, updated : -1};
      break;
    }
    
    ColBets.find(find).sort({updated : -1}).lean().exec(function(err, bets){
      var matchIds = help.arrObjs.collectVars(bets, 'matchId');
      Col.find({matchId : {$in : matchIds}}).sort(sort).lean().exec(function(err, matches){
        cb(err, help.arrObjs.addArrToObj(matches, bets, 'matchId', 'bets'));
      })
    })
  }
  
  
  return Model;
}