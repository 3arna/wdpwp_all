var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var Col = Model.col({col:'Matches'});
  var ColBets = Model.col({col:'Bets'});
  
  Model.returnUnfinished = function(cb){
    if(Model.off){return cb(false, false)}
    var find = {$or : [{"finished" : {$exists : false}}, {"finished" : false}], "t1.rate" : { $ne : '0%' }};
    
    Col.find(find).sort({startMs : 1}).lean().exec(function(err, data) {
      cb(err, data);
    });
  }
  
  Model.returnUnfinishedWithBets = function(cb){
    if(Model.off){return cb(false, false)}
    
    Model.returnUnfinished(function(err, matches){
      var m = matches;
      var matchIds = help.arrObjs.collectVars(matches, 'matchId');
      ColBets.find({matchId : {$in : matchIds}}).sort({value : -1}).lean().exec(function(err, bets){
        cb(err, help.arrObjs.addArrToObj(m, bets, 'matchId', 'bets'));
      })
    })
  }
  
  Model.returnOneWithBets = function(cb){
    if(Model.off){return cb(false, false)}
    
    var matchId = this.matchId;
    
    Model.returnOne(matchId, function(err, match){
      ColBets.find({matchId : matchId}).sort({value : -1}).lean().exec(function(err, bets){
        match.bets = bets;
        cb(err, match);
      })
    })
  }
  
  Model.returnOne = function(matchId, cb){
    if(Model.off){return cb(false, false)}
    Col.findOne({matchId : matchId}).lean().exec(cb);
  }
  
  Model.returnFinished = function(cb){
    if(Model.off){return cb(false, false)}
    
    var name = this.name || false;
    var t1 = this.t1 || false;
    var t2 = this.t2 || false;
    var limit = this.limit || 0;
    var find = {"finished" : true};
    if(this.t1){
      find = {"finished" : true, $or : [{'t1.name' : t1, 't2.name' : t2}, {'t1.name' : t2, 't2.name' : t1}]} 
    } else if(name) {
      find = {"finished" : true, $or : [{'t1.name' : name}, {'t2.name' : name}]} 
    }
    
    
    Col.find(find).sort({date : -1, updated : -1}).limit(limit).lean().exec(function(err, data) {
      cb(err, data);
    });
  }
  
  
  return Model;
}