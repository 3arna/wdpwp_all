module.exports = function(){
    var obj =  {
      main : {
        title : 'go big or go home',
        description : 'our small wdpwp betting community',
        copyrights : '© 2015 '+
          '<a target="_blank" class="txt-red" href="http://wdpwp.com">wdpwp</a>'+
          '. All rights reserved.'
      },
      routes : {
        main : '/',
        slot : '/slot',
        ladder : '/ladder',
        user : '/u/',
        match : '/m/',
        team : '/t/'
      },
      links : [
        {
          name : 'chrome extension',
          link : 'https://chrome.google.com/webstore/detail/wdpwp-bettrack/naimocokmoolndojocncllmpbgoipeem'
        },
        {
          name : 'cs go lounge',
          link : 'http://csgolounge.com/'
        },
        {
          name : 'reddit',
          link : 'http://www.reddit.com/r/csgobetting/'
        },
        {
          name : 'hltv',
          link : 'http://hltv.org/'
        }
      ],
      slots : {
        number : 100,
        text : 'ask for slot'
      },
      left : {
        title : 'Ladder',
        description : 'most active system users'
      },
      right : {
        title : 'Finished Games',
        description : 'Latest games list'
      },
      users : [
        {
          "name": "Zarna",
          "stats": {
              "value": 72.42999999999998,
              "win": 20.440000000000005,
              "loss": 19.989999999999995,
              "count": 117,
              "balance": 0.45000000000000107
          }
        },
        {
          "name": "Majasqzi",
          "stats": {
              "value": 2.6700000000000004,
              "win": 2.98,
              "loss": 1.27,
              "count": 9,
              "balance": 1.71
          }
        },
        {
          "name": "Evas",
          "stats": {
              "value": 27.86000000000001,
              "win": 6.4399999999999995,
              "loss": 14.76,
              "count": 33,
              "balance": -8.32
          }
        },
        {
          "name": "Etas",
          "stats": {
              "value": 45.23,
              "win": 13.76,
              "loss": 23.899999999999995,
              "count": 26,
              "balance": -10.140000000000004
          }
        }
      ],
      unfinished : [
        {
          "matchId": "1952",
          "updated": 1418832811221,
          "startDate": "2014-12-17 19:00",
          "startMs": 1418842800000,
          "date": "2014-12-17",
          "t2": {
              "name": "CW",
              "rate": "65%",
              "img": "http://cdn.csgolounge.com/img/teams/CW.jpg?15"
          },
          "t1": {
              "name": "Planetkey",
              "rate": "35%",
              "img": "http://cdn.csgolounge.com/img/teams/Planetkey.jpg?15"
          },
          "start": "19:00 CET",
          "games": "Best of 3",
          "left": "18 hours from now",
          "time": "2 h"
        },
        {
            "matchId": "2071",
            "updated": 1421245309025,
            "startMs": 1421193600000,
            "date": "2015-01-14",
            "t2": {
                "name": "NiP",
                "rate": "96%",
                "img": "http://cdn.csgolounge.com/img/teams/NiP.jpg?17",
                "cof": 0.041666666666666664
            },
            "t1": {
                "name": "GGWP",
                "rate": "4%",
                "img": "http://cdn.csgolounge.com/img/teams/GGWP.jpg?17",
                "cof": 24
            },
            "games": "Best of 3",
            "left": "16 hours from now",
            "time": "3 h",
            "bets": [
              {
                  "_id": "54b65d72509295c52833064a",
                  "matchId": "2071",
                  "user": "Slapukas",
                  "value": 8.18,
                  "pot": "0.34",
                  "updated": 1421237618941,
                  "date": 1421237618941,
                  "items": [
                      {
                          "value": "2.86",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/_mE7661BSYHuXkYGxaalrkYt8VrpMUfBOpCVHCFA7Qk3agwQ9F0Pk06HWvjjxvkgXiqqHeAuR8U8m5IOIFb6AzgoGBrlWQiaXvZe8uCM6jFaPOEFpHsJ3DCaoxA7UqgzJW4QGu1bGIR1yFTI9Iz6Ik8G4UuhdTfANpOUCSFR_w1iMkwdtAgbkkvLCKe62Kk0T2DnH-Z6XM1tzZ1EbAOsWmY-T0m7Fg2YTQ==/99fx66f",
                          "name": "M4A1-S | Dark Water (Minimal Wear)"
                      },
                      {
                          "value": "2.56",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/g8yoTnS_BX58r8wk7EBY4DuAYv8wzws-qGEfPgimEEdKx5-1LaNDbNx20NrKIARuI4c5uDnQCzquahgsCbAHTUWFm7U1qkImzz3SxchhOn0yh3PQYYVdfas1QGdEtABDT52L4jzwAm_dbYvUlDpWLTHAd-0_gRdrr2ESOUW2BRsF2oG9/99fx66f",
                          "name": "CS:GO Case Key"
                      },
                      {
                          "value": "2.01",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz5_MeKyPDJYeh39FqVcT8o-9RrnDDUN5M5kXMP4oLlfKl7uvdCXYLUuZYoZGJPUU6OFN1upvBk406VdL8DYoi_ti3i_MnBKBUSwW3YSBA/99fx66f",
                          "name": "Desert Eagle | Crimson Web (Minimal Wear)"
                      },
                      {
                          "value": "0.75",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz5uJ_OKIz5rdwrBBLJhVOwF_QboACA919JwROi6_rwOPRK7vYDHM-N5MdBOGcPXXvHXYwmo6Uk4gKdcepaKo3u62yztOz1ZUkbv5Ctaz7RSfvML/99fx66f",
                          "name": "USP-S | Royal Blue (Minimal Wear)"
                      }
                  ],
                  "win": "t2"
              },
              {
                  "_id": "54b5c042509295c52833062f",
                  "matchId": "2071",
                  "user": "Gwux",
                  "value": 3.5599999999999996,
                  "pot": "0.15",
                  "updated": 1421197378366,
                  "date": 1421197378366,
                  "items": [
                      {
                          "value": "2.38",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/1xdrajgU_ScfnyoXi8FM1G9bodt8ZPNny1H5DW8nBHMeHFyRYQi7Nb9GNumtoRBad1z6nHV782PNWv4fbjETeRFeSJtwDLw8rzcy467rA0tzSrGEMS69esFbzw0qYEdJFwhzn31Z_jy6BQrkrPsUdWtGssMyZeVsllDzWXBkQ3MZEBmcIQz5MuhRZL-hv0JIM0uwnH4u7WjKA6FeIGReZhEW/99fx66f",
                          "name": "AK-47 | Blue Laminate (Minimal Wear)"
                      },
                      {
                          "value": "0.40",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/FckaN7zjtXgPepk2I0upPK2F0Ib4k7s427RKLMet4Zvcwi3M5f_zaq-jhcgFK_WytYKLwfGMuzzdv00-xrv2kdOAOcb0-_Rjv9KBwgZh5qOxlMDZtdn1JdG-fD6auaXG4swo_OHp5j_z0o_KBWvmtqSd-5un2P0g0_4bKIy79MvYyjvH96i5N6-7h8QKYPL38MbFlKGF92bf5xd6i-6lxtnNc9P8_Q==/99fx66f",
                          "name": "StatTrak™ SSG 08 | Slashed (Field-Tested)"
                      },
                      {
                          "value": "0.44",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/WhSiEwKmSBjK2y59FQbVWeJYaKJG1kZYHhX9Z_Hgnf6TH5XoW7oOCmoCMoMzZonX-l8z5U_JRlwYHvp18PaK9JxdgeJKvgkDenM2iTAsmsb-SXj9C5wIRRQfy2WlpNz6rROI2EK2CB1hSzSCASSNw-NZcfxOm1FQShWiYuanjPjBQYDiGL5MX2obZYpoeNmRu04s4hrAXABMFaEx8eOH_A==/99fx66f",
                          "name": "StatTrak™ CZ75-Auto | Poison Dart (Battle-Scarred)"
                      },
                      {
                          "value": "0.34",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEgj2sTtIjdr1QvGDBO8Hld9ntcEBliJukAx_PeS7OSNmYUqSB_RcWaFi9VDpXnBlvJRhAoCy8esHcFrusoCSO7IkOYwaGpLWC6OGeEmjvZutjtp_/99fx66f",
                          "name": "Sticker | Team Dignitas | Cologne 2014"
                      }
                  ],
                  "win": "t2"
              },
              {
                  "_id": "54b61184509295c528330639",
                  "matchId": "2071",
                  "user": "Apomeister",
                  "value": 0.33999999999999997,
                  "pot": "8.16",
                  "updated": 1421218180486,
                  "date": 1421218180486,
                  "items": [
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/TL7qrrZXtQTmqIo8StAB3PTyIB_yJ7tEMmZZJq42SXuFtd1V70vzFkZxlsJssF1S7PV7WPs4u0A0bV40ryBecYr3yV_-T_QfVgCSyG_6TkPo4zBAv231WThsbyXpP1Jwu6vdZexP8RZ9LJ3Cc-tjUf3oMDCkYfNBIyxUJOZ8DC2F68wO_k3lRxBuxZwzqwQQ-b43XqtqoRwzMgUjuX0PJoLhg0r2SQ==/99fx66f",
                          "name": "PP-Bizon | Sand Dashed (Minimal Wear)"
                      },
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/4NpCxRbY9eqv-FWN97rA41iWiHRSqPuqezaGlxNciEQp0XU-T8Sz-A8hSXPR2pxtQJHTM1u3-659PYGFEkqfTiaTYTRewLTxH1BNedKQj3xEh5grH-K1t3E8sJNRRohEF89qDkvRoP4OUEd9zpyTeG-OlWMA8_r2LmOMwF8dxRJ5iWFiD8WjrV4_SyjfwsUsB4PIZ1m24fYpMI6XDBnLDzjSYg==/99fx66f",
                          "name": "Dual Berettas | Colony (Minimal Wear)"
                      },
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/XjImoSPRJE_Qi5ZZf8LbwuZ-7BBnoSoPBEVFQ5skk2WXORFaes1iXXBSiqdZoodM_nm3V26-KgsCTkJRmjKEb5h7BVBryWVUYCOOrVrolF36b_xPKutkEg5Pc0_FbrhzhgsVVH3NT0t8E5u8a_6HUupV9Ak65nFMWRgfRoU1hmOQZFICOJgkDCwfjK4A74MN6DysWWi3NABYFx4QjGfQZdgkD1I=/99fx66f",
                          "name": "MP9 | Sand Dashed (Minimal Wear)"
                      },
                      {
                          "value": "0.22",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz5uJ_OKIz5rdwrBBLJhVOwF5g3oHS417dVcWN6x_69IfFjos4qQNbAvMIwZG8nSU_DQZQ306kM5h6lce52N9Xzt3Ci4MzwDWw2rpDwDhQhh3A/99fx66f",
                          "name": "USP-S | Blood Tiger (Factory New)"
                      }
                  ],
                  "win": "t1"
              },
              {
                  "_id": "54b63686509295c52833063e",
                  "matchId": "2071",
                  "user": "MNZ",
                  "value": 0.16,
                  "pot": "3.84",
                  "updated": 1421227654567,
                  "date": 1421227654567,
                  "items": [
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/knMbwEHgGmzkeOLsQdMK7Co_0XEFkBQsMLYx9qU1QktbeCw7GPxcfkSh_hJns1ZiMjiKNgyPFCg2vTbkpCNVQVQ6ODEJ-Ft3VND6GGT5RXM2LsEuSNpaMTq8B_nkMFdxSXoDJw73SkRN6vkUf_EZdnFzkjUGh190Mbc89epzDxZeIGhsWahMfRjprktv_QAkci-XYw6KXm8lvD8=/99fx66f",
                          "name": "Nova | Sand Dune (Field-Tested)"
                      },
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/3I-IXs4kvyDyIRdq9l6ga2TDQu-KVLFgJu_EcBK46MwVhL-llzj5MlL4C5TQPvzlfMQZqINLsWQg5MNiE67_xhrGq6-GPP47QokPntN07_R40lKwxx7_fSzl8nNVsfPHK5q_lZQ8-zJppQCUz2XC5m3ZUsDdHvpkNuaDIwSq-ZAX3vr5g23vbgDgC5iOdfn0PtJT_IEYrTgi6pgkCa2qmRWIrOSQM-w=/99fx66f",
                          "name": "PP-Bizon | Sand Dashed (Battle-Scarred)"
                      },
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/VcgmzNtZwxdkQ9xSPnk9Y-2E7H2fKc1XsI0PSNqfdcScwxE3gkWFBcSawKwYGWHt9YO3OpY2zVO2hgha24lizpOBBT2TQYIM1OvEphtTcvzxlfwi0mODSrqHOUyYhXXEot0OB4ZQlgPF686iB19u-Nqd_WnMc48U7I9XH8LeNMSfl1I9kBGVV5iDxvtMAmKtvZGoOZVjgQzh218ekoowl9PeDz8=/99fx66f",
                          "name": "Dual Berettas | Colony (Well-Worn)"
                      },
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/An2Kt_39jq_MEIZRLjbraroxQAa5jYDvGN5VS8rQo83Ldr1MpOHIvWzJmq8IVrfkojYbQbCSgOse1VJZy8a0x8Q0qUa15c-0fLiepQscpPWmIFBZ9MfO8hLUY1KJkueZnkS-TIzp1at7uJSlARCj-fx1DEKxxpbjTd8PTIeTspubKawT6reMujiEzvhUSuGjtnJXRbfEm-QflExEgw==/99fx66f",
                          "name": "XM1014 | Blue Spruce (Field-Tested)"
                      }
                  ],
                  "win": "t1"
              },
              {
                  "_id": "54b6669b509295c52833064f",
                  "matchId": "2071",
                  "user": "zarna",
                  "value": 0.05,
                  "pot": "1.20",
                  "updated": 1421239963225,
                  "date": 1421239963225,
                  "items": [
                      {
                          "value": "0.05",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/vqx6da18nkG26HjmZvnGzwbgsMTpDJABYiar_IIfjmh3p02O9GDYUxYxZBhAmZpBHufrg-ATkAVkLazugwmZYnjlWYTlZN9aBkBgEkPTiVAa8aCbpEbeHGgsnfzZC6V-eZVNjOBax1MWdnIaA4DJU1uk8YzkQoYKNiCg_5UPwml1-l_Q5TCaBUUnP0ZI0J9XWaah0uANzwJg/99fx66f",
                          "name": "AUG | Contractor (Field-Tested)"
                      }
                  ],
                  "win": "t1"
              },
              {
                  "_id": "54b67dbb509295c528330654",
                  "matchId": "2071",
                  "user": "Dread",
                  "value": 0.04,
                  "pot": "0.96",
                  "updated": 1421245883632,
                  "date": 1421245883632,
                  "items": [
                      {
                          "value": "0.04",
                          "img": "https://steamcommunity-a.akamaihd.net/economy/image/Zn7YUJiCXmnmJ6qxkLhBUN4yEuHc8lApMul5q3ReCfevde-rwZ4Ye0b-tk-22B3exjVJptXtUC004n65dUge_aA3-6HQmh9yVo-yRbWSDs_CIwK-kbgeNDjjT7pjHSLhvkfstMSaE0FRsbtEhJoZyt8zC7-EuE51Mu4h-msbRKOvLq7105pefEHn7UPuxBrMgHVRptPlRn1jvyWrdF0T9Q==/99fx66f",
                          "name": "P90 | Sand Spray (Field-Tested)"
                      }
                  ],
                  "win": "t1"
              }
            ]
        },
        {
            "matchId": "2072",
            "updated": 1421245309027,
            "startMs": 1421193600000,
            "date": "2015-01-14",
            "t2": {
                "name": "Dignitas",
                "rate": "90%",
                "img": "http://cdn.csgolounge.com/img/teams/Dignitas.jpg?17",
                "cof": 0.1111111111111111
            },
            "t1": {
                "name": "myXMG",
                "rate": "10%",
                "img": "http://cdn.csgolounge.com/img/teams/myXMG.jpg?17",
                "cof": 9
            },
            "games": "Best of 3",
            "left": "19 hours from now",
            "time": "6 h",
            "bets": [
                {
                    "_id": "54b63675509295c52833063d",
                    "matchId": "2072",
                    "user": "MNZ",
                    "value": 27.59,
                    "pot": "3.07",
                    "updated": 1421227637576,
                    "date": 1421227637576,
                    "items": [
                        {
                            "value": "8.84",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/IOvrfxKNFAZCo8rDTVLlMpinIc5W_RpGlm0Z2am0rZXp4NyES5FSFOJ61j1rMrm8gKB6iV_iGkKQZh7LqKK6n-ai359Vl0sU9CeaN2tus62EvCLXD7cHG8I9X4vjqriIprybjwuVREK_YtNmNyW--8fgbNhfsFZIyzAS3bbx4ZXttZTbCsYUSLV6xTxh/99fx66f",
                            "name": "Sticker | 3DMAX | Katowice 2014"
                        },
                        {
                            "value": "7.43",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/TLLQ-JV5yoLXz6UOcmmJPfT-GknRCcTCAwF2FJaPwZqFuecDzGWMkHcWufBUCdWz7PlBDtgWxMYFCnEGl5nWkIr78wndYYuZZ2e9-ldDxqLo7woWnEOK3wkLQADLjOqMjbjyAthljKpwTYXqSlbrpvDvCViFUrTDDwJ3AZbFgcnc5fUK3WXNlydbvKdfFdfz_e9cCIpA2p1QBH0QiprXydDm8QrfLo6bdA==/99fx66f",
                            "name": "StatTrak™ USP-S | Guardian (Factory New)"
                        },
                        {
                            "value": "6.54",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz56I_OKMyJYdAXUBKxfY_Qt5DfhDCM7_cotA4Lhr7lSLQ_tt4GVYrl4MY1IGJOGX_fTYF-p6E1u0qJVL5GB8S-9jDOpZDknDIyvzQ/99fx66f",
                            "name": "AWP | Corticera (Field-Tested)"
                        },
                        {
                            "value": "4.78",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/jJRFGlg621k6gfaflL94izTYj6scStUZ7k8lhXBZMCxFn3LhASadS5pY6mGy3yQFLN_U7BVV1R3oRCKXcU8nJkrdZusQIppCiinua7GVNxQoyZ_0UQCbBORFE5EtWhs6TZ5n4BUmnXGdA9ZlvpkoFDLzl7JBDY5as04o1WgScH4clzG-QCGLHcZE6DfvkSMXbc7M6kcBwxK8HnnVPBlwewqCbOk=/99fx66f",
                            "name": "USP-S | Caiman (Factory New)"
                        }
                    ],
                    "win": "t2"
                },
                {
                    "_id": "54b5c07f509295c528330632",
                    "matchId": "2072",
                    "user": "Gwux",
                    "value": 3.34,
                    "pot": "0.37",
                    "updated": 1421238549118,
                    "date": 1421197439264,
                    "items": [
                        {
                            "value": "0.26",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/qXVNC7GdqbYTI6mmQATGGxE5h7r17af2x-16vKTijrxgfnrw6IHvpLP6tVhmZJqVCT7c_fzyp_LB5n2upfSZtm88bvr5heito4uxUmUuiYQNKJfluKfp683nTL_j65W3XntzwOiF6aSli7VFbjiPuhQol6O6r6avwe8i6Lvwy7gwcm77-dWpoOLt4Q9vLcOAHyzA_v7z7aPBuCe-v6ib93F9bQ==/99fx66f",
                            "name": "StatTrak™ PP-Bizon | Water Sigil (Battle-Scarred)"
                        },
                        {
                            "value": "0.22",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz5uJ_OKIz5rdwrBBLJhVOwF5g3oHS417dVcWN6x_69IfFjos4qQNbAvMIwZG8nSU_DQZQ306kM5h6lce52N9Xzt3Ci4MzwDWw2rpDwDhQhh3A/99fx66f",
                            "name": "USP-S | Blood Tiger (Factory New)"
                        },
                        {
                            "value": "1.30",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/6WOJdn4qRnPvuAQBday9O1EvQ8c6WkgzO3bXG5FK9ZwgaL6NJzYAYU9hGP9TzOG1SSgYgDNFSDc9fdAJkFzili8qqoc2MgdoXxAc9VCG8qRNPlOYdxAGLjF84R7aWOaVJFqvkw82HGNZLg31WrzkoFg8W9JfGQ45NmaQQo8N4MggNf-DYmoQYhN_TacNgbf2AD0E0mJHVW5sJYwbiQGywXA8_cwgPRU=/99fx66f",
                            "name": "Desert Eagle | Heirloom (Minimal Wear)"
                        },
                        {
                            "value": "1.56",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/mqAJzKUuI8dS5usdueFZzCLsw33hXi2Hhig4B10HEWtTqz43_DJl1fI_9-OfgQVCOuuYOuhBLYOAIz8VXBEGYVzpKj3tNmLc4k7z6ZzLFlM-_dMirBRjmowiDhMeBFE7baU7B_46Z-_1fub8ndwFXhX03mqzBSyI0nthVkZAVDYK9Hc9sjIlh6Ug8unGzQYHfKrWPuMVMtyAKWQFR0VTIEKoKQ==/99fx66f",
                            "name": "StatTrak™ UMP-45 | Corporal (Factory New)"
                        }
                    ],
                    "win": "t2"
                },
                {
                    "_id": "54b67dbb509295c528330656",
                    "matchId": "2072",
                    "user": "Dread",
                    "value": 0.66,
                    "pot": "5.94",
                    "updated": 1421245883636,
                    "date": 1421245883636,
                    "items": [
                        {
                            "value": "0.55",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz51O_W0DzRyTQrNF6FhV_ozywTlDi8mppU7V4Sw8utVLwvsstDCO-R5ZdlMSsGFUv7TYQ6suUs6gaJfe8bb8SzxnXO-rB-OZck/99fx66f",
                            "name": "Nova | Koi (Minimal Wear)"
                        },
                        {
                            "value": "0.11",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/fWFc82js0fmoRAP-qOIPu5THSWqfSmTELLqcUywGkijVjZYMUrsm1j-9xgEObwgfEh_nvjlWhNzZCveCDfIBj98xqodQ2CZknz51MeSwJghkZzvAE6FKXeE74DfiDCA3_vhuUdO_4rZIfFnotteSMLN_N4tFHMHUXaKFYw2ou004gfIPKsbYp3vsiS_qb2ZYCQ2rpDyjN8Rd0w/99fx66f",
                            "name": "Negev | Bratatat (Field-Tested)"
                        }
                    ],
                    "win": "t1"
                },
                {
                    "_id": "54b655df509295c528330646",
                    "matchId": "2072",
                    "user": "Majasqzi",
                    "value": 0.06,
                    "pot": "0.54",
                    "updated": 1421235679744,
                    "date": 1421235679744,
                    "items": [
                        {
                            "value": "0.06",
                            "img": "https://steamcommunity-a.akamaihd.net/economy/image/BvRC5ReJoYETMLR1-jBVgL64iFRT-a_Bx_5nbx7WHSfP_3UeTpXnk7PpqIvcUAkOpr_TE1rmr8XB9WB9H8AKLcC9YRRfkeCao5isgd8aGh-iqZgLHrPh3M30UX4JlTYx3s1hEEmA-ZOIr66FxwZGGu75nRZbtOPIxK4_awGSXiSc9GFHDMmkx-T_roWAG15L5qjEEFnk4oLS9Gk=/99fx66f",
                            "name": "P90 | Scorched (Battle-Scarred)"
                        }
                    ],
                    "win": "t1"
                }
            ]
        },
      ],
      finished : [
        {
          "matchId": "1947",
          "t2": {
              "name": "Denial",
              "rate": "91%",
              "img": "http://cdn.csgolounge.com/img/teams/Denial.jpg?15"
          },
          "t1": {
              "name": "Lunatik",
              "rate": "9%",
              "img": "http://cdn.csgolounge.com/img/teams/Lunatik.jpg?15"
          },
          "finished": true,
          "winner": "t1"
        },
        {
          "matchId": "1946",
          "t2": {
              "name": "eLevate",
              "rate": "5%",
              "img": "http://cdn.csgolounge.com/img/teams/eLevate.jpg?15"
          },
          "t1": {
              "name": "IBP",
              "rate": "95%",
              "img": "http://cdn.csgolounge.com/img/teams/IBP.jpg?15"
          },
          "finished": true,
          "winner": "t1"
        }
      ]
    }
    return obj;
}