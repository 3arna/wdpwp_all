module.exports = function(app){
  
  var controllers = app.controllers;
  var middles = app.sys.apps.dataWdpwp.middles;

  this.get('/', middles.check.setModer, middles.log.route, controllers.page.main);
  this.get('/nostream', middles.log.route, controllers.page.main);
  this.get('/ladder', middles.log.route, controllers.page.ladder);
  this.get('/stream', middles.log.route, controllers.page.main);
  this.get('/u/:user', middles.log.route, controllers.page.user);
  this.get('/m/(:id)/*', middles.check.setModer, middles.log.route, controllers.page.match);
  this.get('/t/:name', middles.log.route, controllers.page.matches);
  this.get('/:t1/vs/:t2', middles.log.route, controllers.page.matches);
  this.get('/matches', middles.log.route, controllers.page.matches);
  
  this.get('/info', middles.log.route, controllers.page.rules);
  this.get('/cash', middles.log.route, controllers.page.cash);
  
  //must be signed in
  //this.get('/settings', middles.log.route, controllers.profile.settings);
  this.get('/extension', middles.log.route, controllers.profile.extension);
  //this.get('/slot', middles.log.route, controllers.profile.slot);
  
  if(app.env == 'development'){
    this.get('/modify/users', controllers.modify.users);
    this.get('/test', controllers.page.test);
  }
  return this;
  
}