var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  app.templateName = 'onePage';
  var models = app.models;
  
  controller.users = function(req,res) {
    models.user.return(function(err, users){
      res.send(app.render({users:users, keys:help.arrObjs.collectKeys(users)}, 'modify/users'));
    })
  }
  

  return controller;
}




