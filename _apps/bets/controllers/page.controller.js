var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  app.templateName = 'onePage';
  var models = app.models;
  
  controller.main = function(req,res) {
    var stream = req.url == '/stream' ? true : false;
    async.parallel(
      {
       activeUsers : models.user.returnActive,
       inactiveUsers : models.user.returnInactive,
       freshUsers : models.user.returnFresh,
       unfinished : models.match.returnUnfinishedWithBets,
       finished : models.match.returnFinished.bind({limit : 20}),
      },
      function(err, result){
        result.unique = app.sys.cache.unique;
        result.requests = app.sys.cache.requests;
        result.profile = req.session.profile || false;
        result.moder = req.moder || false;
        result.stream = stream;
        stream ? result.subtitle = 'Autoplaying first match stream' : false;
        res.send(app.render(result, 'page/main'));
      }
    );
  }
  
  controller.ladder = function(req,res) {
    async.parallel(
      {
       activeUsers : models.user.returnActive,
       inactiveUsers : models.user.returnInactive,
       freshUsers : models.user.returnFresh,
      },
      function(err, result){
        result.profile = req.session.profile || false;
        result.subtitle = 'CS:GO Betting ladder';
        result.subdescription = 'Here you can check our system members statistics and how good they are in CS:GO betting.';
        res.send(app.render(result, 'page/ladder'));
      }
    );
  }
  
  controller.rules = function(req, res){
    async.parallel(
      {
       activeUsers : models.user.returnActive,
       inactiveUsers : models.user.returnInactive,
      },
      function(err, result){
        result.profile = req.session.profile || false;
        result.subdescription = 'Basic tutorial how to become a part of WDPWP.';
        result.subtitle = 'How to be on betting ladder?';
        res.send(app.render(result, 'page/rules'));
      }
    );
  }
  
  controller.cash = function(req, res){
    var result = {
      profile : req.session.profile || false,
      subdescription : 'Basic tutorial how to convert your virtual items to cash.',
      subtitle : 'How to conver your virtual items to cash'
    }
    res.send(app.render(result, 'page/cash'));
  }
  
  controller.match = function(req,res) {
    var id = req.params.id || '2142';
    
    async.parallel(
      {
        match : models.match.returnOneWithBets.bind({matchId : id})
      },
      function(err, result){
        result.profile = req.session.profile || false;
        result.moder = req.moder || false;
        result.subdescription = 'Match details in '
          +' '+result.match.event
          +' '+result.match.type
          +' tournament: ('+result.match.t1.rate+')'+ result.match.t1.name 
          +' vs ' + result.match.t2.name 
          +'('+result.match.t2.rate+') ' 
          + result.match.date 
          +' '+result.match.start + '. ';
        result.subtitle = result.match.t1.name + ' vs ' + result.match.t2.name + ' ' + result.match.date;
        res.send(app.render(result, 'page/match'));
      }
    );
  }
  
  controller.matches = function(req,res) {
    
    var obj = {};
    if(req.params.t1){
      obj.t1 = req.params.t1;
      obj.t2 = req.params.t2;
    } else if(req.params.name){
      obj.name = req.params.name;
    }
    
    async.parallel(
      {
        finished : models.match.returnFinished.bind(obj)
      },
      function(err, result){
        result.profile = req.session.profile || false;
        result.subdescription = 'CS:GO pro match winner and loser list since 2014-10-22.';
        result.subtitle = 'Finished CS:GO match list';
        res.send(app.render(result, 'page/matches'));
      }
    );
  }
  
  controller.user = function(req,res) {
    
    var user = req.params.user || false;
    
    async.parallel(
      {
       unfinished : models.bet.returnByUser.bind({user : user}),
       finished   : models.bet.returnByUser.bind({user : user, type : 'finished'}),
       users      : models.user.returnOne.bind({user : user})
      },
      function(err, result){
        result.profile = req.session.profile || false;
        if(result.finished.length){
          if(result.users[0]){
            result.subdescription = 'WDPWP system user ' + result.users[0].name + ' betting history.'
            + ' Won:' + result.users[0].stats.win.toFixed(2)
            + '$; Lost:' + result.users[0].stats.loss.toFixed(2)
            + '$; Balance: ' + result.users[0].stats.balance.toFixed(2)
            + '$; Bets: ' + result.users[0].stats.count + '. ';
            result.subtitle = result.users[0].name + ' betting history';
          }
        }
        res.send(app.render(result, 'page/user'));
      }
    );
  }
  
  controller.test = function(req,res) {
    
    res.send(app.render({}, 'page/test'));
  }

  return controller;
}




