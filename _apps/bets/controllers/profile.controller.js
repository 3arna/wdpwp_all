var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  app.templateName = 'onePage';
  var models = app.models;
  models.betUsers = app.sys.apps.dataBets.models.users;
  
  controller.settings = function(req,res) {
    if(req.session.profile && req.session.profile.user && !req.session.profile.disabled){
      
      var result = {profile : req.session.profile || false};
      res.send(app.render(result, 'profile/settings'));
      
    } else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Cannot ' + req.method + ' ' + req.url);
    }
  }
  
  controller.extension = function(req,res) {
    if(req.session.profile && req.session.profile.user && !req.session.profile.disabled){
      
      var result = {profile : req.session.profile || false};
      res.send(app.render(result, 'profile/extension'));
      
    } else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Cannot ' + req.method + ' ' + req.url);
    }
  }
  
  controller.slot = function(req, res){
    if(req.session.profile && req.session.profile.user && !req.session.profile.slot){
      
      var result = {profile : req.session.profile || false};
      
      var data = {
        steamId : req.session.profile.steamId,
        slot : help.date.return('ms')
      }
      models.betUsers.updateOne(data);
      req.session.profile.slot = data.slot;
      res.redirect(req.header('Referer') || '/');
      
    } else {
      res.statusCode = 404;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Cannot ' + req.method + ' ' + req.url);
    }
  }
  

  return controller;
}




