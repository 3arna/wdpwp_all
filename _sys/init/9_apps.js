var paths   = require(process.cwd() + '/_sys/cfg/paths'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys.js'),
    app     = require(paths.lib + '/fbf/app');

module.exports = function() {
  var sys = this;
  sys.apps={};
  sys.devs={};
  sys.msgs =  require(process.cwd() + '/_sys/cfg/msgs.js');
  if(cfg.apps.length || cfg.devs.length){
    
    cfg.apps.forEach(function(appName){
      sys.apps[appName] = new app(sys, appName);  
    });
    
    if(true || sys.settings.env === 'development'){
      
      console.log(cfg.devs);
      
      cfg.devs.forEach(function(devAppName){
        sys.devs[devAppName] = new app(sys, devAppName, {dev: true});
      });

      //var temp = { 'post' : [], 'get' : []}
      
      /*if(sys.routes.post){
        for(var i in sys.routes.post)
          temp.post.push(sys.routes.post[i].path)
      }
      
      if(sys.routes.get){
        for(var i in sys.routes.get)
          temp.get.push(sys.routes.get[i].path)
      }*/
        
      //console.log(temp);
      console.log('');
    }
  }
}