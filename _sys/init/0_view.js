var paths = require(process.cwd() + '/_sys/cfg/paths');
var fs      = require('fs');
var doT = require('express-dot');

module.exports = function() {
  
  var dir = paths.views;
  this.set('views', dir);
  this.set('view engine', 'dot');
  // Register Dot as a template engine.
  this.engine('dot', doT.__express);
  //file loader inside of views
  doT.setGlobals({
    loadfile:function(path){return require('fs').readFileSync(paths.views + path, 'utf8');}
  });
  
  doT.templateSettings = {
    evaluate:    /\{\{([\s\S]+?)\}\}/g,
    interpolate: /\{\{=([\s\S]+?)\}\}/g,
    encode:      /\{\{!([\s\S]+?)\}\}/g,
    use:         /\{\{#([\s\S]+?)\}\}/g,
    define:      /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
    conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
    iterate:     /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
    varname: 'it',
    strip: true,
    append: true,
    selfcontained: false
  };
  //console.log(doT);
  
  //Less engine loader
  
  
  /*if(this.env == 'development' && true){
    fs.readFile(dir + '_styles/styles.less', function(err,styles) {
        if(err) return console.error('Less MSG <> Could not open file: %s',err);
        less.render(styles.toString(), function(er,css) {
            if(er) return console.error(er);
            fs.writeFile(process.cwd() + '/public/css/default.css', css, function(e) {
                if(e) return console.error(e);
                console.log('Less MSG <> CSS Compiled');
            });
        });
    });
  }*/
}