var express         = require('express');
var path            = require('path');
var favicon         = require('serve-favicon');
var serveStatic     = require('serve-static');
var bodyParser      = require('body-parser');
var methodOverride  = require('method-override');
var cookieParser    = require('cookie-parser');
var cookieSession   = require('cookie-session');
var errorHandler    = require('errorhandler');
//var express = require('express');
//var router  = express.Router();
//var debug           = require('debug')('wdpwp');


module.exports = function() {
  
  /*this.vault = new (require(process.cwd() + '/lib/vault.js'))(this);
  var app = this;
  Categories.tree(function(err, data){
    !err ? app.vault.add(data) : false; 
  })*/
  
  this.use(favicon(process.cwd()+ '/public/ico/wdpwp.ico'));
  this.use(serveStatic(process.cwd() + '/public'));
  this.use(bodyParser.urlencoded({ extended: true }));
  this.use(bodyParser.json());
  //this.use(express.multipart()); NEED TO USE DIFFERENT THING for express 4
  this.use(methodOverride());
  this.use(cookieParser());
  this.use(cookieSession({ secret: 'asdfasdfgh' }));
  //console.log(this.Router());
  //this.use(this.router);
  this.use(errorHandler());
  
  this.cache = {};
   
}
