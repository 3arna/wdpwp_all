var mongoose = require("mongoose");
var cfg      = require(process.cwd() + '/_sys/cfg/sys.js');

module.exports = function() {
  var sys = this;
  
  sys.db = sys.db || {};
  if(cfg.mongo && !cfg.mongo.disabled){
    for(var i in cfg.mongo.dbs){
      var dbInfo = cfg.mongo.dbs[i];
      if(!dbInfo.env || dbInfo.env == sys.settings.env){
        sys.db[dbInfo.name] = mongoose.createConnection(dbInfo.connect);
        console.log('[DB][ '+ dbInfo.name +' ] => successfully connected');
        for(var y in dbInfo.collections){
          var collection = dbInfo.collections[y];
          collection.name && collection.schema
            ? sys.db[dbInfo.name].model(''+collection.name+'', collection.schema)
            : sys.db[dbInfo.name].model(''+collection+'', new mongoose.Schema({},{strict : false}))
        }
      }
    }
  } else {
    sys.db = false;
    console.log('no db specified');
  }
  
}