var Schema = require('mongoose').Schema;

// "type"       one of - app, section, field, entry, entity
// "parent"     for - section, field, entry, entity   => parentSectionId
// "value"      for - app, section, field, entity
// "container" for - entry, entity

var cfg = {
  authorisation: 'wdpwp_content_dev',
  apps : [
    'dataSteam', 'dataWdpwp', 'dataBets', 'dataTrendis',
    'bets', 'login'/*, 'modify'*/
  ],
  devs : [
    /*'contentModify'*/
  ],
  mongo : {
    disabled : false,
    dbs : [
      {
        name : 'content',
        connect : 'mongodb://testing:testingtesting@ds047514.mongolab.com:47514/wdpwp_content',
        collections : [
          {
            name: 'Sections',
            schema: new Schema({
              
              app: {type: Schema.Types.ObjectId, ref: 'Sections'},
              
              type: {type: String},
              children: { type: Schema.Types.Mixed },
              
              parent: { type: Schema.Types.ObjectId, ref: 'Sections'},
              note: { type: String },
              
              value: { type: String },
              
              entry: { type: Schema.Types.ObjectId, ref: 'Sections'},
            })
          },
          {
            name: 'Users',
            schema: new Schema({
              username: String,
              password: String,
              owner: [ Schema.Types.ObjectId ],
              editor: [ Schema.Types.ObjectId ]
            })
          }
        ]
      },
      {
        name : 'def',
        connect : 'mongodb://main:mainmainmain@ds039261.mongolab.com:39261/wdpwp_bets',
        collections : ['Users', 'Matches', 'Bets'], //env: production
      },
      {
        name : 'profiles',
        connect : 'mongodb://test:testtest@ds039321.mongolab.com:39321/wdpwp_profiles',
        collections : ['History', 'Permissions', 'Profiles', 'Ips', 'Bans'],
      },
      {
        name : 'logs',
        connect : 'mongodb://test:testtest@ds037581.mongolab.com:37581/wdpwp_logs',
        collections : ['Routes'],
      },
      {
        name : 'trendis',
        connect : 'mongodb://test:testtest@ds031962.mongolab.com:31962/trendis',
        collections : ['Images', 'Dev'],
      },
      /*{
        name : 'apps',
        connect : 'mongodb://test:testtest@ds039341.mongolab.com:39341/wdpwp_steamapps',
        collections : ['Apps']
      }*/
    ]
  },
  disabledCompiling: true,
}

module.exports = cfg;