var main = process.cwd();

module.exports = {
  
  'root' : main,
  'sys' : main + '/_sys/',
  'lib' : main + '/lib/',
  'apps' : main + '/_apps/',
  'dev' : main + '/_dev/',
  
  'cfg' : main + '/_sys/cfg/',
  'init' : main + '/_sys/init/',
  'public' : main + '/public/',
  'css' : main + '/public/css/',
  'js' : main + '/public/js/',
  
  
  main : {
    'styles' : main + '/_main/styles/',
    'scripts' : main + '/_main/scripts/',
    'controllers' : main + '/_main/controllers/',
    'filters' : main + '/_main/filters/',
    'views' : main + '/_main/views/'
  },
  
  "app": function(appname){
    return {
      "main"          : main+'/_apps/'+appname+'/',
      "views"         : main+'/_apps/'+appname+'/views/',
      "controllers"   : main+'/_apps/'+appname+'/controllers/',
      "templates"     : main+'/_apps/'+appname+'/templates/',
      "models"        : main+'/_apps/'+appname+'/models/',
      "parsers"       : main+'/_apps/'+appname+'/parsers/',
      "middles"       : main+'/_apps/'+appname+'/middles/',
      "public"        : main+'/public/'+appname+'/'
    }
  },
  
  "dev": function(appname){
    return {
      "main"          : main+'/_devs/'+appname+'/',
      "views"         : main+'/_devs/'+appname+'/views/',
      "controllers"   : main+'/_devs/'+appname+'/controllers/',
      "templates"     : main+'/_devs/'+appname+'/templates/',
      "models"        : main+'/_devs/'+appname+'/models/',
      "parsers"       : main+'/_devs/'+appname+'/parsers/',
      "middles"       : main+'/_devs/'+appname+'/middles/',
      "public"        : main+'/public/'+appname+'/'
    }
  }
}