var msgs = {
  ban : 'Your account is banned please try to login again with other account <a href="/">home page</a>',
  error : 'There was some sort of error on the server please try again later <a href="/">home page</a>'
  
}

module.exports = msgs;