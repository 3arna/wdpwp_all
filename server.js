var paths = require(process.cwd() + '/_sys/cfg/paths'),
express = require('express'),
bootable = require('bootable'),
sys = bootable(express());

sys.settings.env = process.env.NODE_ENV || 'development';
var PORT = process.env.PORT || 3000;

sys.phase(bootable.initializers(paths.init));

sys.boot(function(err) {
  if (err) { throw err; return process.exit(-1); }
  sys.listen(PORT, process.env.IP, function(error){
      console.log ( error ? error : 'Express server listening '+ PORT);
  });
});
