module.exports = function(app){
  
  var controllers = app.controllers;
  
  this.use('/api*', controllers.mid.head);
  
  // json stuff
  this.get('/api/json/:appId', controllers.api.jsonGet)
  
  // user stuff
  this.put('/api/user', controllers.api.userGet);
  this.post('/api/user', controllers.api.userPost);
  //this.delete('/api/user', controllers.api.userDelete);
  
  this.get('/api/app', controllers.mid.auth, controllers.api.appGet);
  this.post('/api/app', controllers.mid.auth, controllers.api.appPost);
  this.delete('/api/app/:appId', controllers.mid.auth, controllers.api.appDelete);
  
  // content stuff
  this.use('/api/content*', controllers.mid.auth, controllers.mid.access);
  

  this.get('/api/content', controllers.api.allGet);
  //this.options('/api/content', controllers.api.allGet);
  
  this.get('/api/content/section/:sectionId?', controllers.api.sectionGet);
  this.post('/api/content/section', controllers.api.sectionPost);
  this.delete('/api/content/section/:sectionId', controllers.api.sectionDelete);
  
  this.get('/api/content/entry/:sectionId?', controllers.api.entryGet);
  this.post('/api/content/entry', controllers.api.entryPost);
  this.delete('/api/content/entry/:entryId', controllers.api.entryDelete);
  
  this.get('/api/content/entity/:fieldId?', controllers.api.entityGet);
  this.post('/api/content/entity', controllers.api.entityPost);
  this.delete('/api/content/entity/:entityId', controllers.api.entityDelete);
  
  this.get('/api/content/entity/:entryId/entry', controllers.api.entityByEntryGet);
  
  //this.get('/content', controllers.content.main);
  //this.get('/content/json', controllers.content.json);
  //this.get('/api/content/tree', controllers.api.treeGet);
  
  return this;
}