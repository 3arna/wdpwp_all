var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  //var Entrie = app.sys.db.content.model('Entries');
  //var Field = app.sys.db.content.model('Fields');
  var Section = app.sys.db.content.model('Sections');
  
  
  Model.returnDataByType = function(type, find, cb){
    
    var appId = help.lodash.isString(this) && this;
    
    type = type && !help.lodash.isArray(type) && [type] || type;
    
    if(!!find && appId){
      find.app = appId;
    } 
    
    find = !!find 
      ? find
      : !!type ? {type: { $in: type }, app: appId} : {};
      
    
    Section.find(find).select({'__v': 0}).sort({'type': 1, '_id': -1}).lean().exec(
      function(err, data){
        if(err) return cb(err);
        if(data && data.length){
          data.forEach(function(entry){
            entry.created = Date.parse(entry._id.getTimestamp());
          });
        }
        cb && cb(null, data);
      }  
    );
  }
  
  // app
  
  Model.appAdd = function(app, cb){
    
    new Section({
      type: 'app',
      value: app.value,
    }).save(function(err, returnedApp){
      if(err) return cb(err);
      
      returnedApp.app = returnedApp.id;
      
      returnedApp.save(function(err){
        
        Model.entryAdd.bind(returnedApp.id)({parent: returnedApp.id}, function(err, entry){
          if(err) return cb(err);
          return cb(null, returnedApp);
        });
      });
    });
      
  }
  
  Model.appDelete = function(appId, cb){
    Section.remove({app: appId}, cb);
  }
  
  // section
  
  Model.sectionAdd = function(section, cb){
    
    var appId = this;
    
    if(!section.value){
      return cb(new Error('section name is not specified'));
    }
    
    Section.findOne({value: section.value, parent: section.parent, app: appId}).exec(function(err, returnedSection){
      
      if(err) return cb(err);
      if(returnedSection) return cb(new Error('section with name "' + section.value + '" already exists in this parent'));
      
      new Section({
        app: appId,
        type: section.type,
        parent: section.parent,
        value: section.value,
      }).save(cb);
      
    });
  }
  
  Model.sectionEdit = function(section, cb){
    
    var appId = this;
    
    if(!section.value || !section._id){
      return cb(new Error('section name or section _id is not specified'));
    }
    
    Section.findOne({value: section.value, parent: section.parent, app: appId })
      .exec(function(err, dublicatedSection){
        
        if(err) return cb(err);
        if(dublicatedSection) return cb(new Error('section with name "' + section.value + '" already exists in this parent'));
        
        Section.findOne({_id: section._id, app: appId}).exec(function(err, returnedSection){
          if(err || !returnedSection) return cb(err || new Error('no section found with id ' + section._id));
          returnedSection = help.lodash.merge(returnedSection, section);
          returnedSection.save(cb);
        });
        
    });
  }
  
  Model.sectionRemove = function(sectionId, cb){
    
    var appId = this;
    
    Section.findOne({_id: sectionId, app: appId}).exec(function(err, section){
      if(err) return cb(err);
      if(!section) return cb(new Error('can not delete section. Invalid section id ' + sectionId));
      if(section.type === 'app') return cb(new Error('can not delete app ' + sectionId));
      
      Section.count({parent: sectionId, app: appId}).exec(function(err, childrenLength){
        if(err) return cb(err);
        if(!!childrenLength) return cb(new Error('section still have ' + childrenLength + ' children inside'));
        
        Section.findOneAndRemove({_id: sectionId, app: appId}).exec(cb);
      });
    });
  }
  
  // entries
  
  Model.entryAdd = function(entry, cb){
    var appId = this;
    
    console.log(entry, appId);
    
    Section.count({_id: entry.parent, type: { $in: ['section', 'app'] }, app: appId}).exec(function(err, parentSectionLength){
      if(err) return cb(err);
      if(!parentSectionLength) return cb(new Error('can not add entry to non existing section ' + entry.parent));
      
      new Section({
        app: appId,
        type: 'entry', 
        parent: entry.parent,
        value: entry.value,
        entry: entry.entry,
      }).save(cb);
    
    });
  }
  
  Model.entryEdit = function(entry, cb){
    
    var appId = this;
    
    if(!entry._id){
      return cb(new Error('can not edit entry _id is not specified'));
    }
    
    Section.findOne({id: entry._id, app: appId}).exec(function(err, returnedEntry){
      if(err) return cb(err);
      //returnedEntry.parent = entry.parent || returnedEntry.parent;
      returnedEntry.value = entry.value || returnedEntry.value;
      returnedEntry.entry = entry.entry || returnedEntry.entry;
      returnedEntry.save(cb);
    });
  }
  
  //entities
  
  Model.entityAdd = function(entity, cb){
    
    var appId = this;
    
    Section.count({_id: entity.entry, type: 'entry', app: appId}).exec(function(err, parentEntryLength){
      if(err) return cb(err);
      if(!parentEntryLength) return cb(new Error('can not add entity to non existing entry ' + entity.entry));
    
      new Section({
        app: appId,
        type: 'entity',
        parent: entity.parent, 
        value: entity.value,
        entry: entity.entry,
      }).save(cb);
    });
  }
  
  Model.entityEdit = function(entity, cb){
    
    var appId = this;
    
    if(!entity._id){
      return cb(new Error('can not edit entity _id is not specified'));
    }
    
    Section.findOne({_id: entity._id, app: appId}).exec(function(err, returnedEntity){
      if(err) return cb(err);
      if(!returnedEntity) return cb(new Error('entity does not exist'));
      //returnedEntity.parent = entity.parent || returnedEntity.parent;
      returnedEntity.value = entity.value || returnedEntity.value;
      returnedEntity.entry = entity.entry || returnedEntity.entry;
      returnedEntity.save(cb);
    });
  }
  
  return Model;
}