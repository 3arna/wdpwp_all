var paths     = require(process.cwd() + '/_sys/cfg/paths'),
    help      = require(paths.lib + 'help'),
    M         = require(paths.lib + '/fbf/model'),
    async     = require('async');
    

module.exports = function(app){
  
  var Model = new M(app);
  var User = app.sys.db.content.model('Users');
  
  // User get and add
  
  Model.userGet = function(user, cb){
    User.findOne({username: user.username}).exec(function(err, returnedUser){
      
      if(err) return cb(err);
      if(!returnedUser) return cb(new Error('User does not exist'));
      if(returnedUser.password !== user.password) return cb(new Error('User password is incorrect'));
      
      cb(null, {token: returnedUser.id});
    });
  }
  
  Model.userAdd = function(user, cb){
    User.findOne({username: user.username}).exec(function(err, returnedUser){
      if(err) return cb(err);
      if(!!returnedUser) return cb(new Error('User with this username already exists'));
      
      new User(user).save(function(err, newUser){
        if(err) return cb(err);
        if(!returnedUser) return cb(new Error('User can not be created'));
        cb(null, {token: newUser.id});
        
      });
      
    });
  }
  
  return Model;
}