var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  //app.templateName = 'angularWay';
  
  controller.main = function(req,res,next) {
    res.end(app.render({}, 'modify/main'));
    /*models.entry.return(function(err, entries){
      if(err) return next(err);
    });*/
  };
  
  controller.json = function(req,res,next){
    async.parallel({
        entries: function(cb){models.app.entriesReturn(cb)},
        sections: function(cb){models.app.sectionReturn(cb)},
      }, function(err, data){
        var tree = [];
        help.lodash.remove(data.sections, function(section){
          return section.type === 'app' ? tree.push(section) : false
        });
        
        console.log(tree);
        
        /*data.sections.forEach(function(section){
          help.lodash.remove(data.entries, function(entry){
            return section.type === 'field' && section.id == entry.field
              ? section.entries.push(entry)
              : false;
          });
        });
        
        var source = help.lodash.groupBy(data.sections, 'parent');
        
        collectChildren(source, tree);*/
        
        res.json(tree);
      }
    )
  };
  

  return controller;
}




