var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  
  controller.head = function(req, res, next){
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', false);
    res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization, From");
    res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
    
    if (req.method === 'OPTIONS') {
      return res.status(200).end();
    }
    
    /*res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Max-Age', '86400');*/
    
    /*if(['GET', 'POST', 'DELETE'].indexOf(req.method) !== -1){
      return req.headers.authorisation !== 'wdpwp_content_dev'
        ? res.json({ok: false, msg: 'incorrect credentials'})
        : next();
    }*/
    return next();
  }
  
  controller.auth = function(req, res, next){
    
    var token = !!req.headers.authorization && req.headers.authorization.length === 24 && req.headers.authorization.toString();
    if(!token) return Response(new Error('token incorrect or not provided'), res);
    
    models.user.getTokenUser(token, function(err, user){
      if(err) return Response(err, res);
      if(!user) return Response(new Error('incorrect token'), res);
      
      req.user = user;
      
      return next();
    });
    
  }
  
  controller.access = function(req, res, next){
    
    var appId = !!req.headers.from && req.headers.authorization.length === 24 && req.headers.from.toString();
    
    if(!appId) return Response(new Error('app incorrect or not provided'), res);
    
    var isOwner = req.user.owner.indexOf(appId) !== -1;
    var isEditor = req.user.editor.indexOf(appId) !== -1;
    
    if(!isOwner && !isEditor){
      return Response(new Error('access denied'), res);
    }
    
    req.appId = appId;
    
    return next();
  }
  
  function Response(err, res){
    err && console.log('API', err);
    return err 
      ? res.status(404).json({msg: err.error && err.error.message || err.message || err.toString(), ok: false})
      : res.json({data: {},ok: true});
  }

  return controller;
}




