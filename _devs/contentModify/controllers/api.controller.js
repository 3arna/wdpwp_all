var paths       = require(process.cwd() + '/_sys/cfg/paths'),
    help        = require(paths.lib + 'help'),
    async       = require('async');
      
module.exports = function(app){
  var controller = {};
  var models = app.models;
  
  
  var msg = {
    access: {
      error: 'Right permissions required',
    },
    entryPost: {
      error: 'parent is not specified',
      success: 'entry has been added or eddited'
    },
    sectionPost: {
      error: 'specify section min 3 letters',
      success: 'section has been added or eddited'
    },
    entityPost: {
      error: 'value, parent or entry is not specified',
      success: 'entity has been added or edited'
    },
    userPost: {
      error: 'user can not be created. username and password has to be 3 - 50 letters long',
    },
    userGet: {
      error: 'can not get user. username and password has to be min 3 - 50 letters long',
    },
    appPost:{
      error: 'app can not be created. name has to be 3 - 50 letters long',
    },
    appDelete:{
      error: 'app can not be deleted. please specify the correct app id',
    }
  }
  
  // User stuff
  
  controller.userPost = function(req,res){
    
    var username = req.body.username && req.body.username.toString() || '';
    var password = req.body.password && req.body.password.toString() || '';
    
    if(username.length < 3 || username.length > 50 || password.length < 3 || password.length > 50){
      return Response.bind(res)(msg.userPost.error);
    }
      
    models.user.userAdd({username: username, password: password}, Response.bind(res));
  }
  
  controller.userGet = function(req,res){
    var username = req.body.username && req.body.username.toString() || '';
    var password = req.body.password && req.body.password.toString() || '';
    
    if(username.length < 3 || username.length > 50 || password.length < 3 || password.length > 50){
      return Response.bind(res)(msg.userGet.error);
    }
    
    models.user.userGet({username: username, password: password}, Response.bind(res));
  }
  
  // APP stuff
  
  controller.appGet = function(req, res){
    var find = {type: 'app', app: { $in: req.user.owner }};
    models.section.returnDataByType(null, find, Response.bind(res));
  }
  
  controller.appPost = function(req, res){
    var value = req.body.value && req.body.value.toString() || '';
    
    if(value.length < 3 || value.length > 50){
      return Response.bind(res)(msg.appPost.error);
    }
    
    models.section.appAdd({value: value}, function(err, app){
      if(err) return Response.bind(res)(err);
      
      req.user.owner = help.lodash.union(req.user.owner, [app.id]);
      req.user.save(function(err){
        Response.bind(res)(err, app);
      });
    });
  }
  
  controller.appDelete = function(req, res){
    var appId = req.params.appId && req.params.appId.toString();
    
    if(!appId || !appId.length){
      return Response.bind(res)(msg.appDelete.error);
    }
    
    if(req.user.owner.indexOf(appId) < 0){
      return Response.bind(res)(msg.access.error);
    }
    
    models.section.appDelete(appId, function(err){
      if(err) return Response.bind(res)(err);
      
      req.user.owner = help.lodash.remove(req.user.owner, function(app){
        return app.toString() !== appId;
      });
        
      req.user.save(function(err){
        return Response.bind(res)(err);
      });
      
    });
  }
  
  // Section stuff
  
  controller.allGet = function(req,res){
    models.section.returnDataByType.bind(req.appId)(['section', 'app', 'field', 'entry', 'entity'], null, Response.bind(res));
  }
  
  controller.sectionGet = function(req,res,next){
    models.section.returnDataByType.bind(req.appId)(['section', 'app', 'field'], null, Response.bind(res));
  }
  
  controller.sectionPost = function(req,res,next){
    var value = req.body.value;
    
    value = value && value.replace(/\s+/g, '');
    
    if(!value || value.length < 3){
      return Response.bind(res)(msg.sectionPost.error);
    }
    
    req.body._id
      ? models.section.sectionEdit.bind(req.appId)(req.body, Response.bind(res))
      : models.section.sectionAdd.bind(req.appId)(req.body, Response.bind(res));
  }
  
  controller.sectionDelete = function(req,res,next){
    models.section.sectionRemove.bind(req.appId)(req.params.sectionId, Response.bind(res));
  }
  
  //entry stuff
  
  controller.entryPost = function(req,res,next){
    if(!req.body.parent) return Response.bind(res)(msg.entryPost.error);
    
    req.body._id
      ? models.section.entryEdit.bind(req.appId)(req.body, Response.bind(res))
      : models.section.entryAdd.bind(req.appId)(req.body, Response.bind(res));
  }
  
  controller.entryGet = function(req,res,next){
    var find = req.params.sectionId && {type: 'entry', parent: req.params.sectionId};
    models.section.returnDataByType.bind(req.appId)('entry', find, Response.bind(res));
  }
  
  controller.entryDelete = function(req,res,next){
    models.section.sectionRemove.bind(req.appId)(req.params.entryId, Response.bind(res));
  }
  
  //entity stuff
  
  controller.entityPost = function(req,res,next){
    var body = req.body;
    
    /*if(!body._id && (!body.value || !body.value.length || !body.parent || !body.entry)){ 
      return Response.bind(res)(msg.entityPost.error)
    }*/
    
    req.body._id
      ? models.section.entityEdit.bind(req.appId)(body, Response.bind(res))
      : models.section.entityAdd.bind(req.appId)(body, Response.bind(res));
    
    
    
  }
  
  controller.entityGet = function(req,res,next){
    var find = req.params.fieldId && {type: 'entity', parent: req.params.fieldId};
    models.section.returnDataByType.bind(req.appId)('entity', find, Response.bind(res));
  }
  
  controller.entityDelete = function(req,res,next){
    models.section.sectionRemove.bind(req.appId)(req.params.entityId, Response.bind(res));
  }
  
  controller.entityByEntryGet = function(req,res,next){
    var find = req.params.entryId && {type: 'entity', entry: req.params.entryId};
    models.section.returnDataByType.bind(req.appId)('entity', find, Response.bind(res));
  }
  
  
  
  controller.jsonGet = function(req,res,next){
    console.time('JSON');
    var appId = req.params.appId;
    if(!appId) return Response.bind(res)(new Error('can not get not existing app json'));
    console.log('WTF!!!!!!!!!!!!!!!!');
    
    models.section.returnDataByType.bind(req.params.appId)(['section', 'field', 'entry', 'entity'], null, function(err, data){
      if(err) return Response.bind(res)(err);
      if(!data.length) return Response(res)(new Error('no data json data found for this app'));
      
      var grouped = help.lodash.groupBy(data, 'parent');
      var type = help.lodash.groupBy(data, 'type');
      
      var fields = help.lodash.keyBy(type.field, '_id');
      //var sections = help.lodash.groupBy(type.section, '_id');
      var entries = help.lodash.groupBy(type.entry, 'parent');
      var entryEntries = help.lodash.groupBy(type.entry, 'entry');
      var entities = help.lodash.groupBy(type.entity, 'entry');
      
      
      
      var soloEntry = function(entryId, sectionId){
        
        var entityList = entities[entryId];
        if(!entityList) return;
          
        var entityListByFields = help.lodash.groupBy(entityList, 'parent');
        var entryData = {};
          
        help.lodash.forEach(entityListByFields, function(entities, key){
          var fieldValue = fields[key].value;
          
          entryData[fieldValue] = entities.length<2
            ? help.lodash.first(entities).value
            : entities.map(function(entity){ return entity.value});
            
        });
      
        if(entryEntries[entryId]){
          entryData = help.lodash.extend(entryData, extractSection(sectionId));
        }
        
        return entryData;
      }
      
      var extractEntries = function(entryIds, sectionId){
        return entryIds.length<2
          ? soloEntry(help.lodash.first(entryIds), sectionId)
          : entryIds.map(function(entryId){ return soloEntry(entryId, sectionId) });
      }
      
      var extractSection = function(sectionId){
        var sectionData = [];
        var entryList = help.lodash.get(entries, sectionId);
        if(!entryList) return;
        var entryIds = entryList.map(function(tempEntry){ return tempEntry._id });
        if(!entryIds) return;
        
        sectionData.push(extractEntries(entryIds, sectionId));
          
        return sectionData;
      }
      
      console.timeEnd('JSON');
      res.json(extractSection(appId));
      
    });
  }
  
  // Tree stuff
  
  controller.treeGet = function(req,res,next){
    // models.app.treeReturn(function(err, tree){});
    async.parallel({
        sections: function(cb){models.section.returnDataByType.bind(req.appId)(null, null, cb)},
      }, function(err, data){
        var tree = [];
        help.lodash.remove(data.sections, function(section){
          return section.type === 'app' ? tree.push(section) : false
        });
        
        /*data.sections.forEach(function(section){
          help.lodash.remove(data.entries, function(entry){
            return section.type === 'field' && section.id == entry.field
              ? section.entries.push(entry)
              : false;
          });
        });*/
        
        var source = help.lodash.groupBy(data.sections, 'parent');
        
        collectChildren(source, tree);
        Response.bind(res)(err, tree);
      }
    )
  }
  
  
  var Response = function(err, data){
    err && console.log(err);
    console.log('API RESPONSE Length', data && data.length);
    return err 
      ? this.status(404).json({msg: err.error && err.error.message || err.message || err.toString(), ok: false})
      : this.json({data: data, ok: true});
  }
  
  var collectChildren = function(source, collection){
    
    collection.forEach(function(entry){
      if(source[entry.id]){
        entry.children = source[entry.id];
        delete source[entry.id];
        collectChildren(source, entry.children);
      }
    });
  }
  

  return controller;
}




