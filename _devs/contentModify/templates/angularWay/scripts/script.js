var app = angular.module('App', ['ngRoute']);
  
app.config(function($routeProvider) {
    var resolveProjects = {
      projects: function (Projects) {
        return Projects.fetch();
      }
    };
    var pagesPath = 'views/contentModify/pages/';
   
    $routeProvider
      .when('/', {
        templateUrl: pagesPath + 'pageMain.html',
      })
      .when('/entry/:entryId?', {
        templateUrl: pagesPath + 'pageEntries.html',
        controller: 'ControllerEntries',
      })
      .when('/section/:sectionId?', {
        templateUrl: pagesPath + 'pageSection.html',
        controller: 'ControllerSection',
      })
      /*.otherwise({
        redirectTo:'/'
      });*/
});

app.controller('ControllerMain', function($scope, $rootScope, $http, $location, $timeout){
    
  $scope.errorMsg = false;
  $scope.successMsg = false;
  $scope.showAddMenu = false;
  $scope.sections = [];
  $scope.fields = [];
  $scope.loading = true;
  
  $scope.clearMsg = function(){ $scope.successMsg = false; $scope.errorMsg = false; };
  
  $scope.msgShow = function(res){
    console.log(res);
    $scope.clearMsg();
    res.ok 
      ? $scope.successMsg = 'process successfull' 
      : $scope.errorMsg = res.msg || res;
    
    $timeout($scope.clearMsg, 1500);
  };
  
  $scope.loadingDone = function(){
    $scope.loading = false;
  }
  
  $scope.getSectionById = function(sectionId){
    return _.findWhere($scope.sections, {_id: sectionId});
  };
  
  $scope.getSectionIndexById = function(sectionId){
    return _.findIndex($scope.sections, function(section) {
      return section._id == sectionId;
    });
  }
  
  $http.get("/api/content/tree").success(function(res){
    $scope.fields = res.data.fields;
    $scope.sections = res.data.sections;
    $timeout($scope.loadingDone, 250);
  }).error($scope.msgShow);
  
});

  