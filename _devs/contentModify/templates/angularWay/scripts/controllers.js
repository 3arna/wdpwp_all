app.controller('ControllerEntries', function($scope, $rootScope, $http, $location) {
    
  $scope.loading = true;
  $scope.dateShow = true;
  $scope.sortField = 'name';
  $scope.reverse = true;
  
  $http.get("/api/content/entry")
  .success(function(res) {
    $scope.entries = res;
    $scope.loading = false;
  }).error(function(err){
    $scope.loading = false;
  });
  
  
  $scope.postEntry = function(){
    var data = { fieldName: $scope.fieldName, fieldEntry: $scope.fieldEntry };
    $http.post('/api/content/entry', data).success(function(apiRes){
        $scope.fieldName = '';
        $scope.fieldEntry = '';
        $scope.entries.push(apiRes.data);
        $scope.msgShow(apiRes);
    }).error($scope.msgShow);
  };
});
  
app.controller('ControllerSection', function($scope, $routeParams, $http, $location){
  
  $scope.sectionSelected = $routeParams.sectionId && $scope.getSectionById($routeParams.sectionId);
  $scope.sectionName = $scope.sectionSelected && $scope.sectionSelected.name || '';
  $scope.sectionDescription = $scope.sectionSelected && $scope.sectionSelected.description || '';
  
  $scope.title = $scope.sectionSelected ? 'Edit section' : 'Create new section';
  
  $scope.sectionPost = function(){
    var data = { 
      sectionName: $scope.sectionName,
      sectionDescription: $scope.sectionDescription,
      sectionId: $scope.sectionSelected && $scope.sectionSelected._id,
    };
    $http.post('/api/content/section', data).success(function(apiRes){
      
      if(!$scope.sectionSelected){
        $scope.sectionName = '';
        $scope.sectionDescription = '';
        $scope.sections.push(apiRes.data);
        $location.path('/section/'+apiRes.data._id);
      } else {
        var sectionIndex = $scope.getSectionIndexById($routeParams.sectionId);
        $scope.sections[sectionIndex] = apiRes.data;
      }
      
      $scope.msgShow(apiRes);
    }).error($scope.msgShow);
  };
  
  $scope.sectionRemove = function(){
    if(!confirm('Are you sure you want to delete this?')) return false;
    $http.delete('/api/content/section/' + $routeParams.sectionId ).success(function(apiRes){
      var sectionIndex = $scope.getSectionIndexById($routeParams.sectionId);
      _.remove($scope.sections, function(section, index) {
        return index == sectionIndex;
      });
      
      $location.path('/');
      
      $scope.msgShow(apiRes);
    }).error($scope.msgShow);
  };
});