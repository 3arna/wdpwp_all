module.exports = function(){
    var obj =  {
      main : {
        title : 'WDPWP betting statistics',
        description : 'our small wdpwp betting community',
        copyrights : '© 2015 '+
          '<a target="_blank" class="txt-red" href="http://wdpwp.com">wdpwp</a>'+
          '. All rights reserved.'
      },
      routes : {
        main : '/',
        slot : '/slot',
        ladder : '/ladder',
        user : '/u/',
        lounge : 'http://csgolounge.com/match?m=',
        match : '/m/',
        matches : '/matches',
        team : '/t/',
        login : '/login',
        logout : '/logout',
        player : 'http://www.hltv.org/?pageid=173&playerid=',
        settings : '/settings'
      },
      menu : [
        {
          code : '<i class="fa fa-home"></i>',
          name : 'home',
          link : '/nostream',
          noblank : true
        },
        {
          code : '<i class="fa fa-flag-checkered"></i>',
          name : 'finished',
          link : '/matches',
          noblank : true
        }
      ],
      links : [
        /*{
          name : 'chrome extension',
          link : 'https://chrome.google.com/webstore/detail/wdpwp-bettrack/naimocokmoolndojocncllmpbgoipeem'
        },*/
        /*{
          name : 'login via steam',
          link : '/auth/steam'
        },*/
        
        {
          name : 'csgl',
          link : 'http://csgolounge.com/'
        },
        {
          name : 'reddit',
          link : 'http://www.reddit.com/r/csgobetting/'
        },
        {
          name : 'hltv',
          link : 'http://hltv.org/'
        }
      ],
    }
    return obj;
}