const actionsPageSection = {};

actionsPageSection.inputOnChange = (component, e, data) => {
    var section = component.state.section;
    section[e.target.name] = e.target.value;
    component.setState({section});
  };

actionsPageSection.save = (component, process, e) => {
  
  const data = {
    sectionName: e.target.name.value,
    sectionDescription: e.target.description.value,
    sectionId: e.target.sectionId.value,
  }
  
  component.props.methodsSection.set(data, process);
  
  
  /*$.post('/api/content/section', data)
    .always((apiRes) => {
      console.log('API =>', apiRes);
      
      state.process = {
        inProgress: false,
        error: !apiRes.done && apiRes.message,
        success: apiRes.done,
      };
      
      state.section = apiRes.ok ? apiRes.data : component.props.methods.sectionGet(data.sectionId);
      component.setState(state);
      apiRes.ok && 
    });*/
  
  e.preventDefault();
}

actionsPageSection.remove = (component, e, data) => {
  
  data = {
    sectionId: component.state.section._id,
  }
  
  const state = component.state;
  state.process.inProgress = true;
  component.setState({state});
  
  if(confirm('realy want to delete this section?')){
    superagent
      .del('/api/content/section/'+ data.sectionId)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes.body);
        
        state.process = {
          inProgress: false,
          error: err || !apiRes.ok && apiRes.message,
          success: apiRes.ok,
        };
        
        component.setState(state);
        apiRes.ok && component.props.methods.sectionDelete(component.state.section._id);
      });
    
  }
}