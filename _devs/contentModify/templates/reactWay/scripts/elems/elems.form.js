class ElemFormTxtArea extends React.Component {
  render(){
    return <textarea 
      name={this.props.name}
      onChange={this.props.onChange}
      className="pdg-20px outline-none dsp-block w-100 brd-grey fix-bb fnt-sm" 
      value={this.props.value}
      placeholder={this.props.placeholder}/>
  }
}

class ElemFormInput extends React.Component {
  /*constructor(){
    super();
    this.state = {
      value: ''
    }
    this.onChangeCustom = this.onChangeCustom.bind(this);
  }
  
  componentWillMount(){
    this.setState({value: this.props.value});
  }
  
  componentWillReceiveProps(newProps){
    this.setState({
      value: newProps.value
    });
  }
  
  onChangeCustom(e){
    this.setState({ value: e.target.value })
  }*/
  render(){
    return <input 
      type="text" 
      onChange={this.props.onChange}
      value={this.props.value}
      name={this.props.name} 
      className="fix-bb pdg-tb-15px pdg-rl-20px w-100 mrg-0 outline-none fnt-lg" 
      placeholder={this.props.placeholder}/>
  }
}