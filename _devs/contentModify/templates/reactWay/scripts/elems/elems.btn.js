const Link = (props) =>
  <a className={props.className} href={`#${props.to}`}>{props.children}</a>

const ElemBtnSubmit = (props) => {
  const classNameButton = classNames({
    'clr-white outline-none pdg-tb-5px pdg-rl-20px mrg-t-5px brd-0 fnt-md': true,
    'cursor-pointer hover-bg-yellowl1 bg-yellow': !props.isDisabled,
    'op-05 bg-grey clr-white': props.isDisabled,
  });
  
  const classNameIcon = classNames({
    'fa fa-circle-o-notch fa-spin fa-fw': props.inProgress,
  });
  
  return (<button type="submit" disabled={props.isDisabled} className={classNameButton}>
    <span className={classNameIcon}>{ !props.inProgress && (props.text || 'save')}</span>
  </button>)
}
  
const ElemBtnSection = (props) => {
  const className = classNames({
    'cursor-pointer txt-deco-none clr-white mrg-b-2px dsp-block clr-inherit trans': true,
    'bg-greyl2': !props.isActive,
    'bg-blackl1': props.isActive,
  });

  return (
    <Link to={`/section/${props.item._id}`} className={className}>
      <span className="brd-blackl1 dsp-inline pdg-1px">
        <span className="bg-blackl1 hover-bg-white hover-clr-black trans pdg-rl-15px pdg-tb-5px dsp-inline">
          {props.item && props.item.name}
        </span>
      </span>
    </Link>
  );
}

const ElemBtnSectionNew = (props) =>
  <div>
    <Link to={`/section`}>
      <span className="pdg-tb-5px pdg-rl-10px dsp-inline bg-black hover-bg-blackd1 clr-green mrg-t-2px cursor-pointer">
        create new section
      </span>
    </Link>
  </div>

const ElemBtnApp = (props) => 
  <div>
    <i className="fa fa-caret-down clr-blackl3 pdg-rl-10px"/>
    <h2 className="txt-up dsp-inline pdg-rl-30px pdg-tb-2px mrg-0 bg-green clr-white hover-bg-greenl1 cursor-pointer">
      {props.text}
    </h2>
  </div>

const ElemBtnField = (props) =>
  <div className="clr-black">
    <Link to={`/field/${props.item._id}`} className="clr-inherit">
      <span className="brd-grey pdg-tb-3px pdg-rl-10px dsp-inline hover-bg-greyl2 mrg-t-2px cursor-pointer">
        {props.item && props.item.name}
      </span>
    </Link>
  </div>