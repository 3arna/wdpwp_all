class ElemDropMenu extends React.Component {
  constructor(){
    super();
    this.state = {
      links: [
        {name: 'add new section', type: 'section'},
        {name: 'add new field', type: 'field'},
      ]
    }
  }
  render(){
    console.log(this.props);
    return <ul className="pos-absolute bg-black pdg-5px clr-white pdg-0 mrg-0 list-none" 
      style={{top: this.props.pos.y + 'px', left: this.props.pos.x + 'px'}}>
        <li className="fnt-sm brd-b-blackd1 txt-center pdg-tb-5px clr-blackl3">menu</li>
        {this.state.links.map((item) => 
          <li onClick={this.props.onClick.bind(this, item.type)} className="hover-bg-green pdg-5px cursor-pointer" key={ item.name }>{item.name}</li>
        )}
        <li onClick={ this.props.onSectionRemove.bind(this, false) } className="brd-t-blackd1 txt-center pdg-tb-5px hover-bg-redl3 pdg-5px cursor-pointer">remove</li>
    </ul>
  }
}