const Text = (props) =>
  <p className="bg-black clr-white">{props.text}</p>;
  
const ElemTxtTitle = (props) =>
  <h1 className="txt-center">{props.children}</h1>