const ElemFogInProgress = (props) => 
  <div className="tb z-1 fadeIn h-100 w-100 top-0 left-0 pos-absolute bg-op-08 txt-center">
    <div className="td">
      <span className="pdg-20px bg-white">
        <i className="fa fa-circle-o-notch fa-spin fa-fw"/><small className="mrg-l-5px">Loading...</small>
      </span>
    </div>
  </div>;