class App extends React.Component {
  
  constructor(){
    super();
    this.state = {};
    this.state.process = stateProcess(this);
    this.state.tree    = stateTree(this);
    this.state.section = stateSection(this);
  }
  
  componentWillMount(){
    this.state.tree.load();
  }
  
  render(){
    
    const process = this.state.process.get('app');
    
    return (
      <div className="">
        {process.inProgress && <ElemFogInProgress/>}
        {!process.inProgress && 
          <div>
            <CompTree {...this.state} {...this.props} className="col-4 flt-left h-100vh pos-fixed fix-bb txt-left boxshadow-r-5 pdg-10px"/>
            <UtilPageSelector {...this.state} {...this.props} className="col-8 tb flt-right pdg-30px fix-bb"/>
          </div>
        }
      </div>
    );
  }
  
};

const renderRoute = () => {
  const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
  ReactDOM.render(
    <App 
      location={ location } 
      routePath={_.get(location, 0)} 
      routeParams={_.get(location, 1)} url="/api/content/tree"/>, document.getElementById('app'));
}

window.addEventListener('hashchange', renderRoute, false);

renderRoute();

  