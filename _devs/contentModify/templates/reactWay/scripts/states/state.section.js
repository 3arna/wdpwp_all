const stateSection = (component) => {
  
  const state = component.state;
  
  state.get    = (sectionId) => _.findWhere(state.sections, {_id: sectionId});
  state.remove = (sectionId, proc) => {
    proc && state.process.start(proc, true);
    superagent
      .del('/api/content/section/'+ sectionId)
      .end((err, apiRes) => {
        proc && state.process.delete(proc);
        state.tree.load();
        console.log('API =>', apiRes.body);
        component.forceUpdate();
      });
  };
  state.set = (section, proc) => {
    proc && state.process.start(proc, true);
    superagent
      .post('/api/content/section')
      .send(section)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        
        //var index = _.findIndex(state.sections, {_id: apiRes.data._id});
        //state.sections[index] = apiRes.data;
        proc && state.process.delete(proc);
        state.tree.load();
        //component.forceUpdate();
      });
  };
  
  state.setAll = (sections) => {
    state.sections = sections;
    component.forceUpdate();
  }
  
  return state;
};