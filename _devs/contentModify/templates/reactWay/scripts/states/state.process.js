const stateProcess = (component) => {
  
  const process = {
    data: {
      app: {
        name: 'app',
        inProgress: true,
        error: false,
        success: false,
      }
    }
  }
  
  process.get = (procName) => {
    if(process.data[procName]){
      return process.data[procName];
    }
    
    const proc = { name: procName };
    process.data[procName] = proc;
    return proc;
  }
  
  process.start = (proc, update) => {
    proc.inProgress = true;
    process.data[proc.name] = proc;
    update && component.forceUpdate();
  }
  
  process.error = (proc, update) => {
    proc.inProgress = false;
    process.data[proc.name] = proc;
    update && component.forceUpdate();
  }
  
  process.delete = (proc, update) => {
    delete process.data[proc.name || proc];
    update && component.forceUpdate();
  }
  
  return process;
};