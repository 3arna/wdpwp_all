const stateTree = (component) => {
  
  const tree = {
    data: {}
  }
  
  tree.load = () => {
    superagent
      .get('api/content/tree')
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes.data);
        tree.data = apiRes.data;
        component.state.process.delete('app');
        component.forceUpdate();
      });
  };
  tree.get = () => tree.data;
  
  return tree;
};