class UtilPageSelector extends React.Component {
  
  constructor(){
    super();
    this.state = {
      pageData: false,
      data: false,
    }
  }
  
  componentWillMount(){
    const pageData = _.findWhere(this.props[this.props.routePath+'s'], {_id: this.props.routeParams});
    this.setState({ pageData, data: this.props.data });
  }
  componentWillReceiveProps(nextProps){
    const pageData = _.findWhere(this.props[this.props.routePath+'s'], {_id: nextProps.routeParams});
    this.setState({ pageData, data: nextProps.data });
  }
  
  render(){
    let page = false; 
    switch(this.props.routePath){
      case 'section':
        page = <PageSection 
          process={ this.props.process } 
          methodsSection={ this.props.methodsSection } 
          pageData={ this.state.pageData } 
          data={ this.state.data }/>;
      break;
      case 'field':
        page = <PageField methods={ this.props.methods } pageData={ this.state.pageData }/>;
      break;
      default:
        page = <PageHome/>;
      break;
    }
    return <div className={this.props.className}>{page}</div>;
  }
}


  