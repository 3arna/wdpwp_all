class PageSection extends React.Component{
  constructor(){
    super();
    this.state = {
      section: {
        name: '',
        description: '',
        _id: '',
      }
    };
    
    //setting actions
    this.inputOnChange  = (e) => actionsPageSection.inputOnChange(this, e);
    this.save           = (process, e) => actionsPageSection.save(this, process, e);
    this.remove         = (e) => actionsPageSection.remove(this, e);
    //this.test = this.test.bind(this);
  }
  
  componentWillMount(){
    
    this.props.pageData && this.setState({section: this.props.pageData});
  }
  
  componentWillReceiveProps(nextProps){
    nextProps.pageData && this.setState({section: nextProps.pageData});
  }
  
  test(){
    console.log(this.props.methods.sectionGet(this.state.section._id));
  }
  
  render(){
    const process = this.props.process.get(this.state.section._id);
    
    return(
      <form /*onClick={this.test}*/ className="txt-center" onSubmit={this.save.bind(this, process)}>
        <i className="fa fa-lg fa-times-circle hover-clr-red cursor-pointer clr-redl2" onClick={this.remove}/>
        <ElemTxtTitle>{this.props.pageData ? 'Edit existing section' : 'Create new section'}</ElemTxtTitle>
        
        {this.props.pageData && 
          <div>
            <ElemFormInput name="name" onChange={this.inputOnChange} value={this.state.section.name}/>
            <ElemFormTxtArea name="description" onChange={this.inputOnChange} value={this.state.section.description}/>
            <input type="hidden" name="sectionId" value={this.state.section._id}/>
            <ElemBtnSubmit inProgress={process.inProgress}/>
            
            <div className="mrg-t-30px txt-center clr-white txt-up">
              <div className="pdg-10px bg-black col-4 flt-left fix-bb brd-blackl3">add new field</div>
              <div className="pdg-10px bg-black col-4 flt-left fix-bb brd-blackl3">add new section</div>
              <div className="pdg-10px bg-black col-4 flt-left fix-bb brd-blackl3">add new collection</div>
            </div>
            
          </div>
        }
      </form>
    )
  }
}


  