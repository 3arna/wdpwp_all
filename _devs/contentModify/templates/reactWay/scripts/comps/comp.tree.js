class CompTree extends React.Component {
  constructor(){
    super();
    
    this.state = {
      dropMenu: {
        pos: {x: 0, y: 0},
        visible: false,
      },
      newSection: {
        parent: false,
        type: false,
      }
    }
    this.dropMenuShow   = this.dropMenuShow.bind(this);
    this.dropMenuHide   = this.dropMenuHide.bind(this);
    this.addNewShow     = this.addNewShow.bind(this);
    this.addNewHide     = this.addNewHide.bind(this);
    this.onSectionSave  = this.onSectionSave.bind(this);
    this.onSectionRemove= this.onSectionRemove.bind(this);
  }
  
  dropMenuShow(section, e){
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      dropMenu: {pos: {x: e.pageX, y: e.pageY}, visible: true},
      newSection: {parent: section._id, type: false},
    });
  }
  dropMenuHide(e){
    this.state.dropMenu.visible && this.setState({dropMenu: {visible:false}});
  }
  addNewShow(type, e){
    const newSection = this.state.newSection;
    newSection.type = type;
    this.setState({newSection});
  }
  addNewHide(e){
    this.setState({newSection: {parent: false, type: false}});
  }
  onSectionSave(section, proc){
    this.props.section.set(section, proc);
  }
  onSectionRemove(section, proc){
    const sectionId = section._id || this.state.newSection.parent;
    if(confirm('realy want to delete this section?')){
      this.props.section.remove(sectionId, proc);
    }
  }
  
  render(){
    
    return(
      <div className={this.props.className} onClick={this.dropMenuHide}>
      
        {this.props.tree.get().map((item) =>
          <CompTreeSection 
            {...this.props}
            newSection= { this.state.newSection.type && this.state.newSection }
            addNewHide= { this.addNewHide }
            onContextMenu={ this.dropMenuShow }
            onSectionSave={ this.onSectionSave }
            onSectionRemove={ this.onSectionRemove }
            isActive={ item._id === this.props.routeParams } 
            key={ item._id } 
            item={ item }/>
        )}
        
        {this.state.dropMenu.visible && <ElemDropMenu 
          pos={ this.state.dropMenu.pos } 
          onClick={ this.addNewShow }
          onSectionRemove={ this.onSectionRemove }/>}
          
      </div>
    )
  }
}

  