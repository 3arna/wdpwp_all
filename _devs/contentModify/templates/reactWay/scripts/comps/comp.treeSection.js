class CompTreeSection extends React.Component {
  
  constructor(){
    super();
    this.state = {
      nameEdit: false,
      newAdd: false,
      name: '',
      item: {},
      width: 0,
      expended: true,
    }
    this.sectionNameEdit = this.sectionNameEdit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.controlSection = this.controlSection.bind(this);
  }
  
  componentWillMount(){
    this.setState({item: this.props.item});
  }
  componentWillReceiveProps(nextProps){
    this.setState({item: nextProps.item, name: nextProps.item.name});
  }
  sectionNameEdit(e){
    this.setState({nameEdit: true, width: e.target.offsetWidth, name: this.props.item.name});
  }
  onChange(e){
    this.setState({name: e.target.value});
    //this.props.onChange({name: e.target.value, _id: this.state.item._id});
  }
  onKeyDown(proc, e){
    switch(e.keyCode){
      case 27:
        return this.setState({nameEdit: false});
      break;
      case 13:
        if(e.target.value !== this.state.item.name && e.target.value.length >= 3){
          const item = this.state.item;
          item.name = e.target.value;
          //console.log(this.props);
          this.props.onSectionSave(item, proc);
        }
        return this.setState({nameEdit: false});
      break;
      case 8:
        return this.setState({width: this.state.width-7});
      break;
      default:
        this.setState({width: this.state.width+8});
      break;
      case 37:
      case 38:
      case 39:
      case 40:
      case 16:
        return false;
      break;
    }
  }
  onFocus(e){
    e.target.select();
  }
  onBlur(proc, e){
    this.setState({nameEdit: false});
    if(e.target.value !== this.state.item.name && e.target.value.length >= 3){
      const item = this.state.item;
      item.name = e.target.value;
      this.props.onSectionSave(item, proc);
    }
  }
  controlSection(proc, e){
    if(this.props.item.children.length){
      return this.setState({expended: !this.state.expended});
    }
    this.props.onSectionRemove(this.props.item, proc);
  }
  
  
  render(){
    const process = this.props.process.get(this.props.item._id);
    
    const containerClassName = classNames({
      'cursor-pointer txt-deco-none clr-white pdg-tb-1px dsp-block clr-inherit trans': true,
      '': !this.props.isActive,
      'bg-greyl1': this.props.isActive,
    });
    
    const sectionClassName = classNames({
      'trans pdg-tb-1px dsp-inline pdg-rl-15px': true,
      'bg-blackl3 hover-bg-black': this.props.item.type === 'section',
      'bg-greenl1 hover-bg-green': this.props.item.type === 'app',
    });
    
    const inputClassName = classNames({
      'w-100 pdg-rl-15px brd-0 mrg-0 pdg-0 pdg-tb-1px outline-none': true,
    });
    
    const arrowClassName = classNames({
      'fa fa-fw': true,
      'fa-caret-right': !this.state.expended && !!this.props.item.children.length,
      'fa-caret-down': this.state.expended && this.props.item.children.length,
      'clr-grey fa-times-circle-o hover-clr-redl3': !this.props.item.children.length,
      'clr-blackl3 hover-clr-blackd3 trans': this.props.item.children.length,
      
    });
    
    //console.log(this.props.addNewTo, this.props.item._id, this.props.addNewTo === this.props.item._id)
  
    return (
      <div onContextMenu={this.props.onContextMenu.bind(this, this.props.item)}>
        <Link to={`/section/${this.props.item._id}`} className={containerClassName}>
          <div className="">
            <i onClick={this.controlSection.bind(this, process)} className={arrowClassName}/>
            <span className="brd-blackl1 dsp-inline pdg-1px">
              {!this.state.nameEdit && 
                <span onClick={this.props.isActive && this.sectionNameEdit} className={sectionClassName}>
                  {this.state.item.name}
                  {process.inProgress && <i className="mrg-l-5px fa fa-circle-o-notch fa-spin fnt-sm clr-white"/>}
                </span>
              }
              {this.state.nameEdit &&
                <input
                  onChange={this.onChange}
                  onKeyDown={this.onKeyDown.bind(this, process)}
                  style={{width: this.state.width}} 
                  ref="name"
                  onBlur={this.onBlur.bind(this, process)}
                  className={inputClassName}
                  onFocus={this.onFocus}
                  autoFocus={true} 
                  value={this.state.name}/>
              }
            </span>
          </div>
        </Link>
        <div className="pdg-l-20px fix-bb w-100">
          {this.props.newSection && this.props.newSection.parent === this.props.item._id && 
            <CompTreeSectionNew {...this.props}/>}
          {this.state.expended && this.props.item.children.map((item) => 
            <CompTreeSection 
              {...this.props}
              isActive={ item._id === this.props.routeParams } 
              item={ item }
              key={item._id}/>
          )}
        </div>
      </div>
    );
  }
}