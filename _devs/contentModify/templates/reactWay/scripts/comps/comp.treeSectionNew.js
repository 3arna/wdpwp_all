class CompTreeSectionNew extends React.Component {
  
  constructor(){
    super();
    this.state = {}
    
    this.onKeyDown = this.onKeyDown.bind(this);
  }
  
  onKeyDown(proc, e){
    switch(e.keyCode){
      case 27:
        return this.props.addNewHide(e);
      break;
      case 13:
        if(e.target.value.length >= 3){
          const item = this.props.newSection;
          item.name = e.target.value;
          this.props.onSectionSave(item, proc);
          this.props.addNewHide(e);
        }
      break;
      case 37:
      case 38:
      case 39:
      case 40:
      case 16:
        return false;
      break;
      case 8:
        return this.setState({width: this.state.width-7});
      break;
      default:
        this.setState({width: this.state.width+8});
      break;
    }
  }
  
  
  render(){
    
    const process = this.props.process.get(this.props.newSection.parent + '_new');
    
    const inputNewClassName = classNames({
      'pdg-0 mrg-0 pdg-rl-15px w-max-80px fix-bb pdg-tb-1px outline-none': true,
    });
    const iconClassName = classNames({
      'fa fa-fw clr-grey': true,
      'fa-plus-circle': !process.inProgress,
      'fa-circle-o-notch fa-spin': process.inProgress,
    });
  
    return (
      <div className="pdg-tb-2px fix-bb w-100">
        <i className={iconClassName}/>
        <input
          onKeyDown={this.onKeyDown.bind(this, process)}
          className={inputNewClassName}
          autoFocus={true}
          onBlur={this.props.addNewHide}
          placeholder="name"/>
      </div>
    );
  }
}