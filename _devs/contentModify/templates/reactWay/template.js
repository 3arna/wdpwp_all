module.exports = function(){
    
    var templateData = {};
    
    templateData.main = {
      title : 'WDPWP betting statistics',
      description : 'our small wdpwp betting community',
      copyrights : '© 2015 '+
        '<a target="_blank" class="txt-red" href="http://wdpwp.com">wdpwp</a>'+
        '. All rights reserved.'
    };
    templateData.routes =  {
      main : '/',
      slot : '/slot',
      ladder : '/ladder',
      user : '/u/',
      lounge : 'http://csgolounge.com/match?m=',
      match : '/m/',
      matches : '/matches',
      team : '/t/',
      login : '/login',
      logout : '/logout',
      player : 'http://www.hltv.org/?pageid=173&playerid=',
      settings : '/settings'
    };
    
    return templateData;
}