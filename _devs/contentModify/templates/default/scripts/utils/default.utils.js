var utils = {};

utils.findSectionById = function(tree, id){
  if(!id || !tree.length){
    return false;
  }
  
  var selectedSection = false;
  
  var check = (tree, id) => _.each(tree, (section) => {
    if(section._id === id){
      selectedSection = section;
      return false;
    }
    if(!!selectedSection){
      return false;
    }
    !!section.children.length && !selectedSection && check(section.children, id);
  });
  
  check(tree, id);
  return selectedSection;
};