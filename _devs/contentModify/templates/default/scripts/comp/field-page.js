<field-page>
  <div class="w-max-600px mrg-auto">
  
    <div class="fnt-lg pdg-tb-10px pdg-rl-20px brd-b-grey mrg-b-20px">
      <span onclick={this.onRoute} class="clr-greenl3 hover-clr-green dsp-inline cursor-pointer">
        <i class="clr-inherit fa fa-chevron-circle-left"/>
        <span class="mrg-l-5px clr-greyd3">back to parent</span>
      </span>
    </div>
    
    <div class="">
      <div class={
        'trans pdg-tb-10px bg-grey brd-b-yellowd1 pdg-l-20px w-200px txt-left pdg-tb-10px dsp-inline clr-blackl3': true,
        'txt-center fnt-lg fnt-bold': true
      }>
        {opts.data.value}
      </div>
      
      <entry-container each={item in opts.data.entries} data={item}/>
        
      <entry-new if={addNewEntryModeOn} data={opts.data} />
        
      <div onclick={this.toggleNewEntry} class="bg-blackl2 dsp-inline flt-right txt-center hover-bg-black hover-clr-white brd-t-black clr-greyd2 pdg-tb-5px pdg-rl-20px cursor-pointer trans">
        <i class={
          'fa-fw fa-lg fa': true,
          'clr-yellowd1 fa-plus-square': !addNewEntryModeOn,
          'clr-redl1 fa-minus-square': addNewEntryModeOn
        }/> {!addNewEntryModeOn ? 'add new entry' : 'remove new entry' }
      </div>
      
    </div>
    
    <div if={false} class="pdg-l-20px">
      <sectionFull each={item in opts.data.children} data={item}/>
    </div>
  
  </div>
  
  <script type="es6">
    
    this.addNewEntryModeOn = !opts.data.entries.length ? true : false;
    
    this.onRoute = (e) => {
      window.location.hash =  'section/' + this.opts.data.parent;
      this.preventUpdate = true;
    };
    
    this.toggleNewEntry = (e) => {
      this.addNewEntryModeOn = !this.addNewEntryModeOn;
      this.update();
    };
    
    this.removeEntry = (entryId) => {
      tree.trigger('entryDelete', entryId);
      _.remove(opts.data.entries, (entry) => {
        return entry._id == entryId;
      });
    }
  </script>
  
</field-page>