<tree>
  
  <section-dragable each={item in opts.tree} data={item} />
  <drop-menu/>
  
  <div class="brd-t-grey mrg-t-50px txt-center">
    <a onclick={this.click} class="dsp-block clr-green txt-up" target="_blank" href="/content/json">preview Json</a>
  </div>
    
  <script type="es6">
  
    tree.on('refresh', () => this.update());
    
    this.click = (e) => {
      window.open(e.target.href, '_blank');
    }
    
  </script>
</tree>
