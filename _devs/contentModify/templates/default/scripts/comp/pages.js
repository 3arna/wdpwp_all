<section-page>
  <div class="w-max-600px mrg-auto">
    <div if={false}>
      <span class={
        'trans pdg-tb-5px dsp-inline pdg-rl-50px clr-white': true,
        'bg-blackl3 hover-bg-black brd-blackl1': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'bg-grey brd-greyd1 hover-bg-greyd2 clr-black': opts.data.type === 'field'}>
        {opts.data.value}
      </span>
      <span class="pdg-5px bg-grey brd-greyd1 clr-white">{opts.data.children.length}</span>
      <span class="brd-grey pdg-tb-5px pdg-rl-10px clr-greyd1">{opts.data.description || 'no description'}</span>
    </div>
    
    
    <div if={opts.data.type === 'section'} class="fnt-lg pdg-tb-10px pdg-rl-20px brd-b-grey mrg-b-20px">
      <span onclick={this.onRoute} class="clr-greenl3 hover-clr-green dsp-inline cursor-pointer">
        <i class="clr-inherit fa fa-chevron-circle-left"/>
        <span class="mrg-l-5px clr-greyd3">back to parent</span>
      </span>
    </div>
  
    <div class={
      'trans pdg-tb-10px pdg-rl-15px clr-white txt-center fnt-lg fnt-bold brd-b-yellowd1 pdg-l-20px w-200px txt-left dsp-inline': true,
      'bg-blackl3 fix-bb mrg-tb-1px': opts.data.type === 'section',
      'bg-greenl1 brd-green': opts.data.type === 'app',
      'clr-black txt-center fnt-lg fnt-bold': opts.data.type === 'field'}>
      {opts.data.value}
    </div>
    
    <div class="pdg-l-20px">
      <sectionFull each={item in opts.data.children} data={item}/>
    </div>
  
  </div>
  
  <script type="es6">
    this.onRoute = (e) => {
      window.location.hash =  'section/' + this.opts.data.parent;
      this.preventUpdate = true;
    }
    
  </script>
  
</section-page>

<sectionFull>
  <div if={!opts.nofields || opts.data.type !== 'field'} class="tb w-100 brd-l-grey">
    <div class="td w-min-150px">
      <span onclick={this.onRoute} class={
        'trans pdg-tb-10px pdg-rl-15px w-150px clr-white cursor-pointer fix-bb': true,
        'bg-blackl3 hover-bg-black mrg-b-1px dsp-inline': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'hover-bg-greyd2 bg-grey clr-black dsp-inline brd-r-yellowd1': opts.data.type === 'field'}
      >{opts.data.value}</span>
    </div>
    
    <div if={opts.data.type === 'field'} class="td w-100 pdg-tb-5px">
      <entry-container each={item in opts.data.entries} data={item}/>
      <entry-new if={!opts.data.entries.length} data={opts.data} eventmodeoff={true} />
    </div>
  </div>
  <div class="pdg-l-20px">
    <sectionFull each={item in opts.data.children} data={item} noFields={true}/>
  </div>
  
  <script type="es6">
    this.onRoute = (e) => {
      window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
      this.preventUpdate = true;
    }
  </script>
  
</sectionFull>

<home-page>
  <div class="fadeIn txt-center brd-grey">
    <div class="brd-greyl2 pdg-50px clr-greyd1">
      <h1>Good to see u again my friend!!!</h1>
      <p>our small app getting the shape. Hehehe.</p>
    </div>
  </div>
</home-page>