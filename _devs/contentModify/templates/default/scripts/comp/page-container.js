<page-container>
  <div id="pageContainer" class="pdg-30px fix-bb"/>
  
  <script type="es6">
  
    const routes = {};
  	
  	routes.home = (id, action) => riot.mount('#pageContainer', 'home-page', {data: 'some data'});
  	
  	routes.section = (id, action, data) => riot.mount('#pageContainer', 'section-page', {data: store.selectedSection});
  	
  	routes.field = (id, action, data) => riot.mount('#pageContainer', 'field-page', {data: store.selectedSection});
  
    this.on('mount', () => {
      riot.route((pageName, id, action) => {
        store.activeRoute = {pageName, id, action};
        tree.trigger('select', utils.findSectionById(store.tree, id));
        
        pageName = _.contains(['section', 'app'], pageName) && 'section' || pageName;
        
    	  routes[pageName || 'home'](id, action);
    	  
    	});
      
      riot.route.start(true);
    });
    
  </script>
</page-container>