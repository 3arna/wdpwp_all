function Tree() {
  
  var self = this;
  var apiUrl = '/' || 'https://csgobets.herokuapp.com/';
  riot.observable(this);
  
  this.on('load', function(){
    superagent
      .get(apiUrl + 'api/content/tree')
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log(apiRes);
        console.log('API =>', apiRes.data);
        store.tree = apiRes.data;
        self.trigger('success', apiRes.data);
      });
  });
  
  this.on('loadEntries', function(){
    superagent
      .get(apiUrl + 'api/content/entry')
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes.data);
        store.entries = apiRes.data;
        //self.trigger('success', apiRes.data);
      });
  });
  
  this.on('update', function(section){
    superagent
      .post(apiUrl + 'api/content/section')
      .send(section)
      .end((err, apiRes) => {
        console.log(apiUrl + 'api/content/section');
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        !section._id && self.trigger('load');
      });
  });
  
  this.on('delete', function(section){
    if(section.children.length){
      return console.log('Error, still have children');
    }
    if(confirm('realy want to delete this section?')){
      superagent
        .del(apiUrl + 'api/content/section/' + (section._id || section))
        .send(section)
        .end((err, apiRes) => {
          apiRes = apiRes.body;
          console.log('API =>', apiRes);
          self.trigger('load');
        });
    }
  });
  
  this.on('move', function(moveTo){
    store.draggedSection.parent = moveTo._id;
    superagent
      .post(apiUrl + 'api/content/section/')
      .send(store.draggedSection)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        store.draggedSection = false;
        self.trigger('load');
      });
  });
  
  this.on('entry', function(entry){
    superagent
      .post(apiUrl + 'api/content/entry/')
      .send(entry)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        self.trigger('load');
      });
  });
  
  this.on('entryDelete', function(entry){
    if(confirm('realy want to delete this entry?')){
      superagent
        .del(apiUrl + 'api/content/entry/' + (entry._id || entry))
        .end((err, apiRes) => {
          apiRes = apiRes.body;
          console.log('API =>', apiRes);
        });
    }
  });
  
  this.on('drag', function(section){
    if(section._id !== store.draggedSection._id){
      store.draggedSection = section;
      delete store.draggedSection.children;
    }
  });
  
  this.on('select', (section) => {
    store.selectedSection = section;
    self.trigger('refresh');
  });

};

const tree = new Tree();
