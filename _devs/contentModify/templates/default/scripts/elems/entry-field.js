<entry-field>
  <textarea 
    value={opts.data.value} 
    keyup="this.typing" 
    name="entry" 
    placeholder="short text" 
    class="brd-dash-2-grey mrg-b-5px w-100 dsp-block pdg-5px bg-greyl1 fix-bb outline-none fnt-md"/>
  
  <div class="txt-right">
    <span onclick={this.save} class="cursor-pointer hover-bg-yellow bg-yellowd1 trans pdg-rl-15px dsp-inline pdg-tb-1px fnt-bold clr-white">save</span>
  </div>
  
  <script type="es6">
    
    this.typing = (e) => {
      e.target.size = e.target.value.length || 1;
      
      switch(e.keyCode){
        case 27:
          /*e.target.value = this.opts.data.value;
          this.parent.cancel();*/
        break;
        case 13:
          /*if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
            this.parent.updateSection(e.target.value);
          }
          this.parent.cancel();
          this.unmount();*/
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
    this.save = (e) => {
      if(!this.entry.value.length){
        return false;
      }
      const entry = {
        value: this.entry.value,
        type: 'short',
        /*parent: opts.data.parent,*/
        field: opts.data.parent,
      };
      tree.trigger('entry', entry);
    }
  </script>
  
</entry-field>