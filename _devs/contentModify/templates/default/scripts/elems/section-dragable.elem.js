<section-dragable>
  <div
    class={'bg-greyl1': over, 'trans': true}
    draggable={opts.data.type !== 'app'} 
    ondrag={this.onDrag} 
    ondrop={opts.data.type !== 'field' && this.onDrop} 
    ondragover={this.onDragOver}
    ondragleave={this.onDragLeave}>
      
      <section-container data={this.opts.data} />
  
  </div>
  
  <script type="es6">
  
    this.over = false;
    
    this.onDrag = (e) => {
      e.target.style.display = 'none';
      tree.trigger('drag', e.item.item);
      e.stopPropagation();
      e.preventUpdate = true;
    };
    
    this.onDrop = (e) => {
      tree.trigger('move', e.item.item);
      e.stopPropagation();
      e.preventUpdate = true;
    };
    
    this.onDragOver = (e) => {
      this.over = true;
      this.onPrevent(e);
    };
    
    this.onDragLeave = (e) => {
      this.over = false;
      this.onPrevent(e);
    };
    
    this.onPrevent = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    
  </script>
</section-dragable>