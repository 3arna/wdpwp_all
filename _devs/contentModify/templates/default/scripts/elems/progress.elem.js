<elemInProgress>
  <div show={opts.inprogress} class="fadeOut pos-absolute tb w-100 h-100 bg-black z-1">
    <div class="td txt-center clr-greyl1 fnt-lg">
      <span class="bg-blackl1 pdg-rl-20px pdg-tb-10px">
        <i class="fa fa-circle-o-notch fa-fw fa-spin clr-green"/> Loading App...
      </span>
    </div>
  </div>
</elemInProgress>