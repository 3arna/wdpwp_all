<entry-edit>
  <textarea 
    class="brd-dash-2-grey dsp-block fix-bb outline-none fnt-md w-100 pdg-10px" 
    onblur={this.cancel}
    name="fieldName"
    placeholder="entry short text"
    onkeyup={this.typing}
    value={opts.data.value}/>
  
  <script type="es6">
  
    this.on('mount updated', (opts) => {
      this.fieldName.value === this.opts.data.value 
        ? this.fieldName.select()
        : this.fieldName.focus();
    });
    
    this.cancel = (e) => {
      if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
        this.parent.updateEntry(e.target.value);
      }
      this.parent.cancel();
    }
  
    this.typing = (e) => {
      
      switch(e.keyCode){
        case 27:
          e.target.value = this.opts.data.value;
          this.parent.cancel();
        break;
        case 13:
          
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
  </script>
</entry-edit>