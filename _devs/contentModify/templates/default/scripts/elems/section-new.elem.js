<section-new>
  <span>
    <i onclick={this.toggleExpend} class="fa fa-fw mrg-0 dsp-inline fa-genderless clr-grey"
    /><input 
      class="mrg-0 pdg-tb-1px mrg-t-1px pdg-l-15px pdg-r-10px fix-bb outline-none brd-black" 
      onblur={this.cancel} 
      name="newFieldName"
      onkeyup={this.typing}
      placeholder="{opts.newsection.type} name"/>
  </span>
  
  <script type="es6">
    
    this.minSize = 3;
    this.maxSize = 20;
    
    this.on('mount', () => {
      this.newFieldName.focus();
      this.newFieldName.size = this.newFieldName.value.length > this.minSize 
        ? this.newFieldName.value.length 
        : this.minSize;
    });
    
    this.typing = (e) => {
      e.target.size = e.target.value.length > this.minSize 
        ? e.target.value.length 
        : this.minSize;
      
      switch(e.keyCode){
        case 27:
          this.parent.clearNewSection();
          e.target.value = '';
        break;
        case 13:
          if(e.target.value.length >= this.minSize){
            this.parent.addNewSection(e.target.value.replace(/ /g,''));
            e.target.value = '';
          }
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
  </script>
</section-new>