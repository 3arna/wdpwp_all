<section-container>

  <div 
    onclick={this.onRoute}
    oncontextmenu={this.showDropMenu}
    class={
      'hover-bg-greyl1 clr-greyl1 hover-clr-white mrg-t-1px fix-bb cursor-pointer trans': true,
      'bg-greyl1 clr-white': opts.data._id === store.activeRoute.id,
    }>
    
      <i onclick={this.toggleExpend} class={
        'fa fix-bb w-20px dsp-inline txt-center': true,
        'clr-blackl2 hover-clr-green trans cursor-pointer': !!opts.data.children.length,
        'clr-inherit fa-genderless': !opts.data.children.length,
        'fa-angle-right': !expended && !!opts.data.children.length,
        'fa-angle-down': expended && !!opts.data.children.length,
      }
      /><span if={!editMode} ondblclick={this.edit} class={
        'trans pdg-tb-1px dsp-inline pdg-rl-15px': true,
        'bg-blackl3 hover-bg-black brd-blackl1': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'bg-grey brd-greyd1 hover-bg-greyd2 clr-black': opts.data.type === 'field'}
        >
        {opts.data.value || 'noname'}
      </span>
      <span if={!!opts.data.entries.length && opts.data.type==='field' } class="clr-greyd1 fnt-sm">
        {opts.data.entries.length} entries
      </span>
      <section-edit if={editMode && !newSection} data={opts.data} />
      
  </div>
  
  <div show={expended} class="pdg-l-20px">
    <section-new if={!!newSection} newsection={newSection} />
    <section-dragable each={item in opts.data.children} data={item} />
  </div>
  
  <script type="es6">
  
    //console.log(opts.data._id === store.activeRoute.id, store.activeRoute.id);
  
    this.editMode = false;
    this.expended = true;
    this.newSection = false;
    
    dropMenu.on('addNew', (data) => {
      if(this.opts.data._id === data.parent){
        console.log(data);
        this.newSection = data;
        this.expended = true;
        this.update();
      }
    });
    
    this.showDropMenu = (e) => dropMenu.trigger('show', e);
    
    this.clearNewSection = (e) => {
      this.newSection = false;
    }
    
    this.addNewSection = (value) => {
      this.newSection.value = value;
      tree.trigger('update', this.newSection);
      this.item.children.unshift(this.newSection);
      this.newSection = false;
    }
    
    this.updateSection = (value) => {
      this.item.value = value;
      tree.trigger('update', this.item);
    }
    
    this.edit = (e) => {
      this.editMode=true;
    };
    
    this.cancel = (e) => {
      this.editMode = false;
    }
    
    this.toggleExpend = (e) => {
      this.expended = !this.expended;
      e.stopPropagation();
    }
    
    this.onRoute = (e) => {
      window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
      /*if(store.activeRoute.id !== this.opts.data._id){
        window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
        tree.trigger('select', this.opts.data);
      }*/
      this.preventUpdate = true;
    }
    
  </script>
  
</section-container>