<entry-new>
  <div class="brd-b-grey mrg-b-1px fix-bb fnt-md bg-white pos-relative pdg-10px">
    <textarea 
      class="brd-dash-2-grey dsp-block fix-bb outline-none fnt-md w-100 pdg-10px" 
      onblur={!opts.eventmodeoff && this.cancel}
      name="entry"
      placeholder="enter short text"
      onkeyup={this.typing}/>
  </div>
  
  <script type="es6">
    
    this.minSize = 3;
    this.maxSize = 20;
    
    this.on('mount updated', () => {
      this.entry.focus();
    });
    
    this.cancel = (e) => {
      if(e.target.value.length){
        tree.trigger('entry', {
          value: e.target.value,
          field: opts.data._id,
          type: 'short'
        });
      }
      this.parent.toggleNewEntry();
    }
    
    this.typing = (e) => {
      
      switch(e.keyCode){
        case 27:
          !this.opts.eventmodeoff && this.parent.toggleNewEntry();
        break;
        case 13:
          /*if(e.target.value.length >= this.minSize){
            this.parent.addNewSection(e.target.value.replace(/ /g,''));
            e.target.value = '';
          }*/
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
  </script>
</entry-new>