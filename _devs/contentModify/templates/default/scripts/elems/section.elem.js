<sectionEdit>
  <span>
    <input 
      class="pdg-tb-1px pdg-l-15px pdg-r-10px fix-bb outline-none mrg-0 brd-black" 
      onblur={this.parent.cancel} 
      name="fieldName"
      placeholder="new section name"
      onkeyup={this.typing}
      value={opts.data.value}/>
  </span>
  
  <script type="es6">
  
    this.on('mount updated', (opts) => {
      this.fieldName.size = this.fieldName.value.length || 1;
      this.fieldName.value === this.opts.data.value 
        ? this.fieldName.select()
        : this.fieldName.focus();
    });
  
    this.typing = (e) => {
      e.target.size = e.target.value.length || 1;
      
      switch(e.keyCode){
        case 27:
          e.target.value = this.opts.data.value;
          this.parent.cancel();
        break;
        case 13:
          if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
            this.parent.updateSection(e.target.value);
          }
          this.parent.cancel();
          //this.unmount();
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
  </script>
</sectionEdit>