function DropMenu(){ riot.observable(this); }
const dropMenu = new DropMenu();

<drop-menu>
  <ul if={dropMenuOn} class="pos-absolute bg-black pdg-5px clr-white pdg-0 mrg-0 list-none" style="left: {pos.x}px;top: {pos.y}px;">
    <li class="fnt-sm brd-b-blackd1 txt-center pdg-tb-5px clr-blackl3">menu</li>
    <li if={this.section.type !== 'field'} onclick={this.newSection.bind(this, item.type)} each={item in items} class="hover-bg-green pdg-5px cursor-pointer">{item.name}</li>
    <li if={!this.section.children.length} onclick={this.removeSection} class="brd-t-blackd1 txt-center pdg-tb-5px hover-bg-redl3 pdg-5px cursor-pointer">remove</li>
  </ul>
  
  <script type="es6">
    
    this.dropMenuOn = false;
    this.pos = {x: 20, y: 20};
    this.section = false;
    
    dropMenu.on('show', (e) => {
      this.dropMenuOn = true;
      this.pos = {x: e.pageX, y: e.pageY};
      this.section = e.item.item;
      this.update();
    });
    
    dropMenu.on('hide', () => {
      if(this.dropMenuOn){
        this.section = false;
        this.dropMenuOn = false;
        this.update();
      }
    });
    
    this.newSection = (type, e) => dropMenu.trigger('addNew', {type, parent: this.section._id});
    this.removeSection = () => {
      tree.trigger('delete', this.section);
    }
    
    this.items = [
      {name: 'add new section', type: 'section'},
      {name: 'add new field', type: 'field'}
    ];
  </script>
</drop-menu>