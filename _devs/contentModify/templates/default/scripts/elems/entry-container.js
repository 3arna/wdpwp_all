<entry-container>
  <div onmouseenter={this.toggleControls} onmouseleave={this.toggleControls} class="brd-b-grey mrg-b-1px fix-bb fnt-md bg-white pos-relative pdg-10px">
    <p if={!editModeOn} onclick={!editModeOn && this.edit} class="mrg-0 pdg-10px">
      {opts.data.value}
    </p>
    
    <entry-edit if={!!editModeOn} data={opts.data} />
  
    <span show={!editModeOn && hoverModeOn} onclick={this.remove} class="pos-absolute top-0 right-0 pdg-5px">
      <i class="fa fa-lg fa-times clr-greyd1 cursor-pointer trans hover-clr-redl2"/>
    </span>
  </div>
  
  <script type="es6">
    
    this.hoverModeOn = false;
    this.editModeOn = this.opts.editmodeon || false;
    
    this.toggleControls = (e) => {
      this.hoverModeOn = !this.hoverModeOn;
    }
    
    this.edit = (e) => {
      this.editModeOn = true;
    }
    
    this.cancel = (e) => {
      this.editModeOn = false;
    }
    
    this.updateEntry = (entryValue) => {
      this.opts.data.value = entryValue;
      tree.trigger('entry', this.opts.data);
    }
    
    this.remove = (e) => {
      this.parent.removeEntry(opts.data._id);
    }
  </script>
  
</entry-container>