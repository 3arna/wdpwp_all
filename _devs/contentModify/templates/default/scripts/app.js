var store = {
  draggedSection: false,
  selectedSection: false,
  tree: [],
  entries: [],
  activeRoute: {},
  dropMenuVisible: false,
};

<app>
  
  <elemInProgress inprogress={loading} />
  
  <div 
    if={!loading}
    onclick={this.hideDropMenu} 
    class="fadeIn tb h-100vh bg-grey pdg-10px fix-bb w-100" 
    onmousemove={resizing && this.onResize} 
    onmouseup={resizing && this.onResizeEnd}
  >
    <div class="td bg-greyl2 boxshadow fix-bb txt-left valign-top pdg-10px brd-white" style="width:{treeWidth}px;">
      <tree tree={tree}/>
    </div>
    <div 
      class="td w-10px trans txt-center clr-greyl1 hover-clr-white txt-shadow" 
      style="cursor:col-resize;"
      onmousedown={this.onResizeStart} 
    >
      <i class="fa fa-ellipsis-v"/>
    </div>
    <div class="td boxshadow bg-greyl1 brd-greyl2 fix-bb valign-top">
       <page-container data={tree}/>
    </div>
  </div>
  
  <script type="es6">
    
    this.treeBaseWidth = 300;
    this.treeWidth = this.treeBaseWidth;
    this.resizing = false;
    this.startPoint = 0;
    
    this.loading = true;
    this.tree = [];
    
    this.on('mount', () => {
      tree.trigger('load');
    });
    
    this.onResizeStart = (e) => {
      this.resizing = true;
      this.startPoint = e.pageX;
    }
    
    this.onResize = (e) => {
      this.resizing
        ? this.treeWidth = this.treeBaseWidth +  (e.pageX - this.startPoint)
        : e.preventUpdate = true;
    }
    
    this.onResizeEnd = (e) => {
      this.treeBaseWidth = this.treeWidth;
      this.resizing = false;
    }
    
    this.on('updated', () => {
      console.log('app updated');
    });
    
    tree.on('success', (tree) => {
      this.tree = tree;
      this.loading = false;
      this.update();
    });
    
    this.hideDropMenu = (e) => {
      dropMenu.trigger('hide');
      e.preventUpdate = true;
    }
    
  </script>
</app>

<route>
  <a onclick={this.redirect} href={opts.href}><yield/></a>
  
  <script type="es6">
    this.redirect = (e) => {
      window.location.hash = e.target.hash;
    }
  </script>
</route>