module.exports = {
  return : function(type, date){
    date = date ? new Date(date) : new Date();
    type = type || 'standart';
    
    if(type == 'ms')
      date = date.getTime();
    
    if(type == 'standart' || type == 'full'){
      var dd = date.getDate();
      var mm = date.getMonth()+1;
      var yyyy = date.getFullYear();
      var h = date.getHours();
      var m = date.getMinutes();
      
      if(h<10){h='0'+h} if(m<10){m='0'+m} var time = h+':'+m;
      if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} date = yyyy+'-'+mm+'-'+dd;
      
      if(type == 'full')
         date = date +' '+ time;
    }
    
    return date;
  },
  
  fromNow : function(str, type){
    type = type || 'ms';
    var left = str;
    var num = parseInt(left);
    var msLeft = 0;
    var txt = 'none';
    if(left.indexOf('minute')!=-1){
      txt = 'minute';
      msLeft = num*60000;
    } else if(left.indexOf('hour')!=-1){
      txt = 'hour';
      msLeft = num*3600000;
    } else if(left.indexOf('day')!=-1){
      txt = 'day';
      msLeft = num*86400000
    }
    return this.parent.return(type, this.parent.return('ms') + msLeft);
  },
  
  strToMs : function(str){
    var left = str;
    var num = parseInt(left);
    var msLeft = 0;
    var txt = 'none';
    if(left.indexOf('second')!=-1){
      txt = 'second';
      msLeft = num*1000;
    } else if(left.indexOf('minute')!=-1){
      txt = 'minute';
      msLeft = num*60000;
    } else if(left.indexOf('hour')!=-1){
      txt = 'hour';
      msLeft = num*3600000;
    } else if(left.indexOf('day')!=-1){
      txt = 'day';
      msLeft = num*86400000
    }
    return msLeft;
  },
  
  ago : function(date){
    var currentTime = this.parent.return('ms');
    var timeAgo = currentTime - dateAdded;
  }
}