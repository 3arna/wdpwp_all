var str = {
  replaceSpecials : function(str){
    return str.replace(/\&lt;/g, '<').replace(/\&gt;/g, '>');
  },
  onlyLettersAndNumbers : function(str){
    return str.replace(/[^a-z0-9]/gi,'');
  },
  isIn : function(str, arr){
    var contains = false;
    if(typeof arr == 'string'){
      if(str.indexOf(arr) != -1){
        contains = true;
        return contains;
      }
    } else {
      for(var i in arr){
        if(str.indexOf(arr[i]) != -1){
          contains = true;
          return contains;
        }
      }
    }
    return contains;
  }
}

module.exports = str;