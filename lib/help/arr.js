var dir = process.cwd() + '/lib/help/';
var str = require(dir + 'str');
var lodash = require('lodash');

var arr = {
  removeEmpty : function(arr){
    var tmp = [];
    for(var i in arr){
      if(arr[i].length){
        arr[i] = str.onlyLettersAndNumbers(arr[i]);
        tmp.push(arr[i])
      }
    }
    return tmp;
  },
  toObj : function(arr, varKey, type){
    type = type || 'arr';
    var tmp = {};
    for(var i in arr){
      if(type=='arr'){tmp[arr[i][varKey]] ? tmp[arr[i][varKey]].push(arr[i]) : tmp[arr[i][varKey]] = [arr[i]]}
      else{tmp[arr[i][varKey]] = arr[i]}
    }
    return tmp;
  },
  mergeByVal : function(arr1, arr2, val){
    var obj = arr.toObj(arr2, val, 'obj');
    for(var i in arr1){
      if(obj[arr1[i][val]]){
        arr1[i] = lodash.merge(obj[arr1[i][val]], arr1[i]);
      }
    }
    return arr1;
  }
}


module.exports = arr;