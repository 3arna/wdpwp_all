var request = require('request');

var mid = {
    
  getIp : function(req){
    var ip = req.headers["x-forwarded-for"];
    ip = ip.split(",");
    return ip ? ip[ip.length-1] : req.connection.remoteAddress;
  },
  
  setSession : function(req, user){
    req.session.user = user;
  }
  
}

module.exports = mid;