var request = require('request');
var fs = require('fs');

var imgur = {
  
  key : '54478825b5a9575',
  url : 'https://api.imgur.com/3/image',
  
  upload : function(path, callback){
    var opts = {
        uri: imgur.url,
        headers: {
        'Authorization': 'Client-ID '+imgur.key
        },
        qs: {}
    }
    
    fs.readFile(path, function(e, data) {
        if(e) callback(e);
        else {
          opts.qs.type = 'base64';
          opts.body = data.toString('base64');
          
          request.post(opts, function(e, r, body) {
            if(e) callback(e);
            else if (r.statusCode !== 200 || body.error) callback(body.error || r.statusCode);
            else callback(false, JSON.parse(body).data);
          });
        }
    });
  }
  
}

module.exports = imgur;