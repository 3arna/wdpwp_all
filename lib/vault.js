
function Vault(app){
  
  this.enable = true;
  this.log = false;
  if(!app.cache) app.cache = {};
  this.store = app.cache;
  
}

module.exports = Vault;

Vault.prototype.add = function(data){
  if(!this.enable) return false;
  
  var keys = Object.keys(data);
  if(this.log)  console.log('add.keys', keys);
  var temp, t, o, l;
  for(var k in keys){
    t = keys[k].split('.');
    l = t.length;
    o = this.store;
    temp = data[keys[k]];
    
    for(var i=0; i < l-1; i++) {
        var n = t[i];
        if (n in o) {
            o = o[n];
        } else {
            o[n] = {};
            o = o[n];
        }
    }
    o[t[l - 1]] = temp;
  }
  return o;
  o=null;
}

Vault.prototype.get = function(keys){
  if(!this.enable)
    return false;
    
  if(this.log) console.log('get.keys', keys);
  
  
    
    
  var o = this.store;
  
  if(keys){
    var temp, t, l;
    t = keys.split('.');
    l = t.length;
    for(var i=0; i < l; i++) {
      var n = t[i];
      if (n in o) {
          o = o[n];
      } else {
        return false;
      }
    }
  }
  return o;
  o=null;
}

Vault.prototype.remove = function(keys){
  if(!this.enable)
    return false;
    
  if(this.log) console.log('remove.keys', keys);
  
  var temp, t, o, l;
  t = keys.split('.');
  l = t.length;
  o = this.store;
  for(var i=0; i < l; i++) {
      var n = t[i];
      if (n in o) {
        if(i==l-1){
         delete o[n]; 
        } else {
          o = o[n];
        }
      } else {
        return false;
      }
  }
  o=null;
}