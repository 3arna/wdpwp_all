var paths   = require(process.cwd() + '/_sys/cfg/paths'),
    doT     = require('dot'),
    fs      = require('fs');
    
var globals = {
  loadfile:function(path){
    return fs.existsSync(paths.views + path) ? require('fs').readFileSync(paths.views + path, 'utf8') : false;
  }
}

module.exports = function(req,res){
  
  var page = req.tmpl.page,
      name = req.tmpl.name,
      data = req.tmpl.data,
      app = req.tmpl.app;
  
  var appPaths = paths.app(app);
  var path = appPaths.templates + name;
  
  data = {
    data : data,
    templateData : fs.existsSync(path + '/index.js') ? require(path + '/index.js')() : false
  }
  
  data.body = render(path + '/views/'+page+'.dot', data);
  
  res.send(
    render(path + '/views/layout.dot', data)
  );
  
}

function render(path, data){
  var f = doT.template(fs.readFileSync(path), false, globals);
  return f(data);
}