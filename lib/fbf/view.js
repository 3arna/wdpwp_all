var fs      = require('fs'),
    paths   = require(process.cwd() + '/_sys/cfg/paths'),
    help    = require(paths.lib + 'help'),
    doT     = require('dot'),
    sass    = require('node-sass'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys');


var ext = '.html';

var globals = {
  loadMain:function(path){
    return fs.existsSync(paths.main.views + path + ext) 
      ? require('fs').readFileSync(paths.main.views + path + ext, 'utf8') 
      : paths.views + path + ext+' does not exist';
  },
  
  loadAppView:function(app, path, tmpl){
    
    tmpl = tmpl || 'default';
    var dir = paths.app(app).templates + tmpl+ '/views/'
    return fs.existsSync(dir + path + ext) 
      ? require('fs').readFileSync(dir + path + ext, 'utf8') 
      : dir + path + ext + ' does not exist';
  }
}

var templateFileName    = 'template.js';

var styleFileName       = 'template.scss';
var styleFolderName     = 'styles';

var scriptFolderName    = 'scripts';
var scriptFileName      = 'app.js';

var view = {

  template : function(tmpl){
    var appPaths = tmpl.opts.dev ? paths.dev(tmpl.appName) : paths.app(tmpl.appName);
    /**
     * tmpl.viewName
     * tmpl.appName
     * tmpl.templateName
     * tmpl.viewData
     * tmpl.opts
     */
     
    var path = appPaths.templates + tmpl.templateName+ '/';
    
    var data = fs.existsSync(path + templateFileName)
      ? require(path + templateFileName)()
      : {}
    
    if(tmpl.viewData && Object.keys(tmpl.viewData).length>0){
      for(var i in tmpl.viewData){
        tmpl.viewData[i]
          ? data[i] = tmpl.viewData[i] 
          : false;
      }
    }
    
    data.app = {
      view  : tmpl.viewName,
      name  : tmpl.appName,
      tmpl  : tmpl.templateName,
      env   : tmpl.env,
      ms    : help.date.return('ms')
    }
    
    data.body = view.render(path + 'views/' + tmpl.viewName + ext, data);
    
    return view.render(path + 'views/layout' + ext, data);
  },

  render : function(path, data){
    var file = fs.existsSync(path) 
      ? require('fs').readFileSync(path, 'utf8') 
      : paths +' does not exist';
    var fn = doT.template(file, false, globals);
    return fn(data);
  },
  
  json : function(viewData){
    return JSON.stringify(viewData);
  },
  
  sass : function(appName, templateName, opts){
    if(!appName || !templateName){
      return false;
    }
    
    var templatesAppPath = opts.dev ? paths.dev(appName).templates : paths.app(appName).templates;
    var path = templatesAppPath + templateName + '/' + styleFolderName + '/';
    
    
    var file = fs.existsSync(path + styleFileName) 
      ? fs.readFileSync(path + styleFileName, 'utf8')
      : false;
      
    if(!file){
      return false;
    }
    
    
    var sassOpts = {
      data: file.toString(),
      includePaths: [path, paths.main.styles],
      outputStyle: /*'compressed',*/ 'compact'
    }
    
    sass.render(sassOpts, function(err, result){
      if(err){
        return console.log(err);
      }
      
      fs.writeFile(paths.css + appName + '.css', result.css, function(e) {
        if(e) return console.error(e);
        console.log('[ '+ appName +' ] => '+ templateName +' - template CSS file Compiled ');
      });
      
    });
  },
  
  script : function(appName, templateName, opts){
    if(cfg.disabledCompiling || !appName || !templateName){
      return false;
    }
    
    var templatesAppPath = opts.dev ? paths.dev(appName).templates : paths.app(appName).templates;
    var path = templatesAppPath + templateName + '/' + scriptFolderName + '/';
    
    if(!fs.existsSync(path)){
      return false;
    }
    
    var filesAndFolders = extractFilesAndDirs(path);
    
    var fileList = filesAndFolders.fileList;
    var dirList = filesAndFolders.dirList;
    
    var scriptString = '';
    
    //load default file last
    if(fs.existsSync(fileList[scriptFileName.replace('.js', '')])){
      scriptString = fs.readFileSync(path + scriptFileName, 'utf8').toString();
      delete fileList[scriptFileName.replace('.js', '')];
    }
    
    for(var i in dirList){
      help.lodash.extend(fileList, extractFilesAndDirs(dirList[i], i).fileList);
    }
    
    for(var i in fileList){
      var file = fs.existsSync(fileList[i])
        ? fs.readFileSync(fileList[i], 'utf8')
        : false;
      file ? scriptString = file.toString() + '\n' + scriptString  : false
    }
      
      
    if(scriptString.length){
      // producation compiling
      //scriptString = require('babel-core').transform(js, {presets: ['es2015']}).code;
      var babelTransform =  function (js, param){
        return js;
      }
      //scriptString = require('riot').compile(scriptString, { parser: babelTransform, expr: true });
      fs.writeFile(paths.js + appName + '.js', scriptString, function(e) {
        if(e) return console.error(e);
        console.log('[ '+ appName +' ] => '+ templateName +' - template JS file Compiled ');
      });
      
    }
  }
}

function extractFilesAndDirs(path, filePrefix, ext){
  ext = ext || '.js';
  filePrefix = filePrefix || '';
  var dirList = {};
  var fileList = {};
  fs.readdirSync(path).forEach(function(fileName){
    fileName.indexOf(ext) === -1
      ? dirList[fileName] = path + fileName + '/'
      : fileList[filePrefix + fileName.replace(ext, '')] = path + fileName;
  });
  return {dirList: dirList, fileList: fileList}
}
module.exports = view;