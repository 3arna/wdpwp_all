var fs      = require('fs'),
    paths   = require(process.cwd() + '/_sys/cfg/paths'),
    view    = require(paths.lib + '/fbf/view'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys');


var Model = function(app){
  this.app = app;
  this.off = true;
  this.cols = {};
}

Model.prototype.col = function(data){
  data.db = data.db || 'def';
  if(this.app.db[data.db]){
    this.off = false;
    return this.app.db[data.db].models[data.col]
  } else {
    return false;
  }
}

module.exports = Model;