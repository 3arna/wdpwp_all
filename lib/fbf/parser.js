var fs      = require('fs'),
    paths   = require(process.cwd() + '/_sys/cfg/paths'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys'),
    request   = require('request'),
    xml2json  = require('xml2json'),
    cheerio   = require('cheerio');


var Parser = function(app){
  this.maxTimeOuts = 10;
  this.timeOut = this.maxTimeOuts;
  this.app = app;
  this.off = true;
}

Parser.prototype.toDom = function(str){
  return cheerio.load(str);
}

Parser.prototype.get = function(url, cb){
  request.get(url,function(err,body){
    !err && body && body.statusCode == 200 ? cb(err, body) : cb(err);
  })
}

Parser.prototype.post = function(url, cb){
  request.post(url,function(err,body){
    !err && body && body.statusCode == 200 ? cb(err, body) : cb(err);
  })
}

Parser.prototype.ask = function(url, type, cb, method, opts){
  var self = this;
  type = type || 'html';
  var data = false;
  var wait = false;
  
  opts = opts || {};
  opts.url = url;
  opts.method = method || 'get';
  
  //console.log(opts);
  
  request(opts,function(err,body){
    if(err || !body || !body.body || body.statusCode != 200){
      return cb(err || new Error('Parsing error'));
    }
    
    switch(type){
      case 'html':
        data = body.body;
      break;
      case 'dom':
        data = self.toDom(body.body);
      break;
      case 'json':
        try {data = JSON.parse(body.body);}
        catch(err) { return cb(err);}
      break;
      case 'xmlToObj':
        var opts = {
          object: true,
          reversible: false,
          coerce: true,
          sanitize: false,
          trim: true,
          arrayNotation: false
        }
        if(body.headers['content-type'].indexOf('text/xml')!=-1){
          data = xml2json.toJson(body.body, opts)
        } else {
          //err = 'xml problem';
        }
      break;
    }
    return cb(err, data);
  })
}

module.exports = Parser;