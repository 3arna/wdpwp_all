var fs      = require('fs'),
    paths   = require(process.cwd() + '/_sys/cfg/paths'),
    view    = require(paths.lib + '/fbf/view'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys');

var express = require('express');
var router  = express.Router();


var App = function(sys, appName, opts){
  
  this.opts   = opts || {};
  this.name   = appName;
  this.paths  = this.opts.dev ? paths.dev(appName) : paths.app(appName);
  this.exists = false;
  this.db     = sys.db;
  this.sys    = sys;
  this.env    = sys.settings.env;
  
  this.patterns = {
    controllers : '.controller.js',
    models      : '.model.js',
    parsers     : '.parser.js',
    template    : 'template.js',
    middles     : 'middle.js'
  }
  
  this.cache        = {};
  this.controllers  = {};
  this.models       = {};
  this.parsers      = {};
  this.templateName = 'default';
  
  
  this.check();
  this.loadApp();
}

App.prototype.check = function(){
  this.exists = fs.existsSync(this.paths.main) && fs.existsSync(this.paths.controllers) ? true : false;
}

App.prototype.loadApp = function(){
  if(this.exists){
    
    this.parsers = this.collectFileList('parsers');
    this.models = this.collectFileList('models');
    this.middles = this.collectFileList('middles');
    this.controllers = this.collectFileList('controllers');
    
    if(this.sys.settings.env === 'development' && !cfg.disabledCompiling){
      view.sass(this.name, this.templateName, this.opts);
      view.script(this.name, this.templateName, this.opts);
    }
    //routes
    var routes = this.loadFile(this.paths.main + '/routes.js');
    routes ? this.sys.use(routes.call(router, this)) : false;
    
    var log = this.opts.dev 
      ? '[ dev ][ ' + this.name + ' ] => APP loaded '
      : '[ ' + this.name + ' ] => APP loaded ';
    
    this.sys.settings.env == 'development' && console.log(log);
  }
}


App.prototype.loadFile = function(path){
  if(fs.existsSync(path)){return require(path);}
}

App.prototype.collectFileList = function(type){
  
  var filesList = {}, file, patterns = this.patterns;
  if(fs.existsSync(this.paths[type])){
    var files = fs.readdirSync(this.paths[type]);
  
    for(var i in files){
      if(files[i].indexOf(patterns[type]) != -1){
        file = files[i].replace(patterns[type], "").replace('.', '');
        filesList[file] = require(this.paths[type] + files[i])(this);
      }
    }
  }
  
  return filesList;
}

App.prototype.render = function(viewData, viewName, method){
  var data = {};
  method = method || 'template';
  data.viewData         = viewData || {};
  data.viewName         = viewName || false;
  data.appName          = this.name;
  data.templateName     = this.templateName;
  data.env              = this.env;
  data.opts             = this.opts;
  
  var html = 'main is working but no view or view data was loaded ' + this.name;
  
  if(viewName){
    switch(method){
      case 'template':
        html = view.template(data);
        break;
      
      case 'view':
        html = view.render(paths.views + viewName, viewData);
        break;
    }
  } else {
    viewData ? html = view.json(viewData) : false;
  }
  
  return html;
}

module.exports = App;