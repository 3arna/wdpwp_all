var fs      = require('fs'),
    paths   = require(process.cwd() + '/_sys/cfg/paths'),
    view    = require(paths.lib + '/fbf/view'),
    cfg     = require(process.cwd() + '/_sys/cfg/sys');


var Main = function(sys){
  
  this.paths  = paths.main;
  this.exists = false;
  this.db     = sys.db;
  this.sys    = sys;
  
  this.patterns = {
    controllers : '.controller.js',
    models      : '.model.js',
    scripts     : '.script.js'
  }
  
  this.files = {
    script : 'main.js'
  }
  
  this.controllers  = {};
  this.models       = {};
  
  this.loadSys();
}

Main.prototype.loadSys = function(){
    
  if(!cfg.disabledCompiling && this.sys.settings.env == 'development'){
    this.renderInOneFile('scripts');
  }
}


Main.prototype.loadFile = function(path){
  if(fs.existsSync(path)){return require(path);}
}

Main.prototype.renderInOneFile = function(type){
  var scripts = this.collectFileList(type);
  if(scripts){
    
    var dest = paths.js + this.files.script;
    var string = '';
    for(var i in scripts){
      var file = fs.existsSync(scripts[i]) 
      ? fs.readFileSync(scripts[i], 'utf8')
      : false;
      string = string + file.toString();
    }
    
    if(file){
      fs.writeFile(dest, string, function(e) {
        if(e) return console.error(e);
        console.log('[ Main ] => JS file Compiled');
      });
    }
  }
}

Main.prototype.collectFileList = function(type){
  
  var filesList = {}, file, patterns = this.patterns;
  var files = fs.readdirSync(this.paths[type]);
  
  for(var i in files){
    if(files[i].indexOf(patterns[type]) != -1){
      file = files[i].replace(patterns[type], "");
      filesList[file] = this.paths[type] + files[i];
    }
  }
  
  return filesList;
}

module.exports = Main;