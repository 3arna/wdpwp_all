var help = {
  lodash : require('lodash'),
  moment : require('moment'),
  getIp : function(req){
    var ip = req.headers["x-forwarded-for"];
    if(ip){
        var list = ip.split(",");
        ip = list[list.length-1];
    } else {
        ip = req.connection.remoteAddress;
    }
    return ip;
  },
  
  repeat : function(interval, fn){
    setTimeout(function(){fn();help.repeat(interval,fn)}, interval);
  },
  
  delay : function(fn, ms){
    setTimeout(fn, ms);
  },
  
  date : {
    
    return : function(type, date){
      date = date ? new Date(date) : new Date();
      type = type || 'standart';
      
      if(type == 'ms')
        date = date.getTime();
      
      if(type == 'standart' || type == 'full'){
        var dd = date.getDate();
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        var h = date.getHours();
        var m = date.getMinutes();
        
        if(h<10){h='0'+h} if(m<10){m='0'+m} var time = h+':'+m;
        if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} date = yyyy+'-'+mm+'-'+dd;
        
        if(type == 'full')
           date = date +' '+ time;
      }
      
      return date;
    },
    
    fromNow : function(str, type){
      type = type || 'ms';
      var left = str;
      var num = parseInt(left);
      var msLeft = 0;
      var txt = 'none';
      if(left.indexOf('minute')!=-1){
        txt = 'minute';
        msLeft = num*60000;
      } else if(left.indexOf('hour')!=-1){
        txt = 'hour';
        msLeft = num*3600000;
      } else if(left.indexOf('day')!=-1){
        txt = 'day';
        msLeft = num*86400000
      }
      return help.date.return(type, help.date.return('ms') + msLeft);
    },
    
    strToMs : function(str){
      var left = str;
      var num = parseInt(left);
      var msLeft = 0;
      var txt = 'none';
      if(left.indexOf('second')!=-1){
        txt = 'second';
        msLeft = num*1000;
      } else if(left.indexOf('minute')!=-1){
        txt = 'minute';
        msLeft = num*60000;
      } else if(left.indexOf('hour')!=-1){
        txt = 'hour';
        msLeft = num*3600000;
      } else if(left.indexOf('day')!=-1){
        txt = 'day';
        msLeft = num*86400000
      }
      return msLeft;
    },
    
    ago : function(date){
      var currentTime = help.date.return('ms');
      var timeAgo = currentTime - dateAdded;
    },
    
  },
  
  arrObjs : {
    collectVars : function(arr, varKey){
      var tmp = [];
      for(var i in arr){
        arr[i][varKey] ? tmp.push(arr[i][varKey]) : false;
      }
      return tmp;
    },
    
    addArrToObj : function(arr, arrForAdd, varKey, addName){
      var obj = help.arr.toObj(arrForAdd, varKey);
      
      for(var i in arr){
        obj[arr[i][varKey]] ? arr[i][addName] = obj[arr[i][varKey]] : false; 
      }
      return arr;
    },
    
    sum : function(arr, varKey){
      var sum = 0, num;
      for(var i in arr){
        num = Number(arr[i][varKey]);
        if(num){
          sum = sum + num;
        }
      }
      return help.num.toFixed(sum);
    },
    
    collectKeys : function(arr){
      var tmp = {};
      for(var i in arr){
        for(var y in arr[i]){
          tmp[y]=true; 
        }
      }
      return Object.keys(tmp);
    }
  },
  
  arr : {
    removeEmpty : function(arr){
      var tmp = [];
      for(var i in arr){
        if(arr[i].length){
          arr[i] = help.str.onlyLettersAndNumbers(arr[i]);
          tmp.push(arr[i])
        }
      }
      return tmp;
    },
    toObj : function(arr, varKey, type){
      type = type || 'arr';
      var tmp = {};
      for(var i in arr){
        if(type=='arr'){tmp[arr[i][varKey]] ? tmp[arr[i][varKey]].push(arr[i]) : tmp[arr[i][varKey]] = [arr[i]]}
        else{tmp[arr[i][varKey]] = arr[i]}
      }
      return tmp;
    },
    mergeByVal : function(arr1, arr2, val){
      var obj = arr.toObj(arr2, val, 'obj');
      for(var i in arr1){
        if(obj[arr1[i][val]]){
          arr1[i] = help.lodash.merge(obj[arr1[i][val]], arr1[i]);
        }
      }
      return arr1;
    }
  },
  
  obj : {
    merge : function(obj1, obj2){
      for (var i in obj2) { obj1[i] = obj2[i]; }
      return obj1;
    },
    
    clone : function(obj){
      /*var temp = {};
      for(var i in obj)
      {
         temp[i] = obj[i];
      }*/
      return JSON.parse(JSON.stringify(obj));
    },
    
    diff : function(obj1, obj2, action){
      action = action || false;
      var temp = help.lodash.clone(obj2);
      for(var i in obj1){
        if(typeof temp[i] != "undefined"){
          if(temp[i] == obj1[i]){
            delete temp[i];
          } else if(action=="rewrite") {
            temp[i] = obj1[i];
          }
        } else {
          temp[i] = obj1[i];
        }
      }
      return temp;
    },
    
    add : function(data, store){
      var temp, t, o, l;
      o = store;
      var keys = Object.keys(data);
      for(var k in keys){
        t = keys[k].split('.');
        l = t.length;
        temp = data[keys[k]];
        
        for(var i=0; i < l-1; i++) {
            var n = t[i];
            if (n in o) {
                o = o[n];
            } else {
                o[n] = {};
                o = o[n];
            }
        }
        o[t[l - 1]] = temp;
      }
      //return o;
      o=null;
    }
  },
  
  num : {
    toFixed : function(num, amount){
      amount = amount || 2;
      return Number(Number(num).toFixed(amount));
    }
  },
  
  str :{
    replaceSpecials : function(str){
      return str
      .replace(/\&lt;/g, '<')
      .replace(/\&gt;/g, '>')
      .replace(/\&#39;/g, "'");
    },
    onlyLettersAndNumbers : function(str){
      return help.str.replaceSpecials(str).replace(/[^a-z0-9']/gi,'');
    },
    onlyLetters : function(str){
      return help.str.replaceSpecials(str).replace(/[^a-z]/gi,'');
    },
    onlyNumbers : function(str){
      return help.str.replaceSpecials(str).replace(/[^0-9]/gi,'');
    },
    clear : function(str, arr){
      for(var i in arr){
        str = str.replace(arr[i], '');
      }
      return str;
    },
    isIn : function(str, arr, letterCollection){
      letterCollection = letterCollection || false;
      if(str && arr){
        if(typeof arr == 'string'){
          if(!letterCollection){
            if(str.indexOf(arr) != -1){return true;}
          } else {
            return help.str.containsLetters(str, arr);
          }
        } else {
          for(var i in arr){
            if(str.indexOf(arr[i]) != -1){
              return true;
            }
          }
        }
      }
      return false;
    },
    containsLetters : function(str, letterCollection){
      letterCollection = letterCollection.toLowerCase();
      str = str.toLowerCase();
      var temp = str;
      if(letterCollection.length>str.length){
        str = letterCollection; letterCollection = temp;
      }
      
      //letterCollection.length > 3 ? letterCollection = letterCollection.slice(0, -1) : false;
      letterCollection = letterCollection.split("");
      for(var i in letterCollection){
        if(str.indexOf(letterCollection[i]) == -1){
          return false;
        }
      }
      return true;
    },
    
    random : function randomString(len, an){
      an = an&&an.toLowerCase();
      var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
      for(;i++<len;){
        var r = Math.random()*(max-min)+min <<0;
        str += String.fromCharCode(r+=r>9?r<36?55:61:48);
      }
      return str;
    }
  },
  
  file : {
    
  }
}

module.exports= help;