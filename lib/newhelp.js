var dir = process.cwd() + '/lib/help/';

var help = {
  lodash : require('lodash'),
  date : require(dir + 'date'),
  str : require(dir + 'str'),
  arr : require(dir + 'str'),
  
  getIp : function(req){
    var ip = req.headers["x-forwarded-for"];
    if(ip){
        var list = ip.split(",");
        ip = list[list.length-1];
    } else {
        ip = req.connection.remoteAddress;
    }
    return ip;
  },
  
  repeat : function(interval, fn){
    setTimeout(function(){fn();help.repeat(interval,fn)}, interval);
  },
  
  delay : function(fn, ms){
    setTimeout(fn, ms);
  },
  
  arrObjs : {
    collectVars : function(arr, varKey){
      var tmp = [];
      for(var i in arr){
        arr[i][varKey] ? tmp.push(arr[i][varKey]) : false;
      }
      return tmp;
    },
    
    addArrToObj : function(arr, arrForAdd, varKey, addName){
      console.log(this);
      var obj = this.parent.arr.toObj(arrForAdd, varKey);
      
      for(var i in arr){
        obj[arr[i][varKey]] ? arr[i][addName] = obj[arr[i][varKey]] : false; 
      }
      return arr;
    }
  },
  
  obj : {
    merge : function(obj1, obj2){
      return help.lodash.merge(obj1, obj2);
    },
    
    clone : function(obj){
      return JSON.parse(JSON.stringify(obj));
    },
    
    add : function(data, store){
      var temp, t, o, l;
      o = store;
      var keys = Object.keys(data);
      for(var k in keys){
        t = keys[k].split('.');
        l = t.length;
        temp = data[keys[k]];
        
        for(var i=0; i < l-1; i++) {
            var n = t[i];
            if (n in o) {
                o = o[n];
            } else {
                o[n] = {};
                o = o[n];
            }
        }
        o[t[l - 1]] = temp;
      }
      //return o;
      o=null;
    }
  },
  
  file : {
    
  }
}

module.exports= help;