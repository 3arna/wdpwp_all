var querystring = require('querystring'),
    request = require('request');

var Steam = function(opt){
  opt = opt || {};
  this.page   = opt.page || "steam/auth";
  this.url    = 'https://steamcommunity.com/openid/login?';
  this.query  = {
    'openid.return_to' : false,
    'openid.realm' : false,
    'openid.ns' : 'http://specs.openid.net/auth/2.0',
    'openid.mode' : 'checkid_setup',
    'openid.identity' : 'http://specs.openid.net/auth/2.0/identifier_select',
    'openid.claimed_id' : 'http://specs.openid.net/auth/2.0/identifier_select'
  }
}

Steam.prototype.generateQuery = function(req){
  this.host = req.protocol + "://" + req.get('host');
	
	this.query['openid.mode'] = 'checkid_setup';
	this.query['openid.return_to'] = this.host+'/'+this.page;
	this.query['openid.realm'] = this.host;
	return this.url+querystring.stringify(this.query);
}

Steam.prototype.redirect = function(req, res){
  res.redirect(this.generateQuery(req));
}

Steam.prototype.auth = function(req, res, callback){
  var query = req.query;
  if(!query['openid.mode']){return res.send('no data set')}
  
  query['openid.mode'] = 'check_authentication';
  var url = this.url+querystring.stringify(query);
  
  request(url, function (err, response, body) {
    if(err || response.statusCode!=200){
      return callback(err || 'some sort of problem with steam');
    } else {
      if(body.indexOf("is_valid:true") == -1){
        return callback('invalid user');
      } else {
        var id = query['openid.identity'].toString().substr(-17, 17);
        callback(err, id);
      }
    }
  });
}

module.exports = Steam;