var paths = require(process.cwd() + '/_sys/cfg/paths'),
express = require('express'),
bootable = require('bootable'),
sys = bootable(express()),
fs = require('fs'),
https = require('https');

var key  = fs.readFileSync(process.cwd() + '/_sys/keys/privatekey.pem', 'utf8');
var cert = fs.readFileSync(process.cwd() + '/_sys/keys/server.crt', 'utf8');

sys.settings.env = process.env.ENV_VARIABLE || 'development';
var PORT = process.env.PORT || 3000;

sys.phase(bootable.initializers(paths.init));

var server = https.createServer({key: key, cert: cert}, sys);

sys.boot(function(err) {
  if (err) { throw err; return process.exit(-1); }
  server.listen(PORT, process.env.IP, function(error){
      console.log ( error ? error : 'Express server listening '+ PORT);
  });
});
