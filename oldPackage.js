{
  "name": "app-name",
  "version": "0.0.1",
  "private": true,
  "engines": {
    "node": "4.2.5"
  },
  "dependencies": {
    "async": "latest",
    "body-parser": "latest",
    "bootable": "latest",
    "cheerio": "latest",
    "cookie-parser": "latest",
    "cookie-session": "latest",
    "dot": "latest",
    "errorhandler": "latest",
    "express": "latest",
    "express-dot": "latest",
    "lodash": "latest",
    "method-override": "latest",
    "mongoose": "latest",
    "request": "latest",
    "serve-favicon": "latest",
    "serve-static": "latest",
    "xml2json": "latest"
  },
  "scripts": {
    "start": "nodemon --debug --ignore 'public/**/*.js' -e js,scss server.js --env 'development'",
    "prod": "node server.js"
  },
  "devDependencies": {
    "node-sass": "^3.3.3",
    "babel-core": "latest",
    "babel-preset-es2015": "latest",
    "babel-preset-react": "latest",
    "nodemon": "latest",
    "riot": "^2.3.11"
  }
}
