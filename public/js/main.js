var aNames = {
  class : {
    click   : 'aClick',
    active  : 'active',
    hover   : 'aHover'
  },
  attr : {
    data : 'aData'
  },
  sep : {
    key : ':',
    spc : ', '
  },
  fn : {
    convertDataToObj : function(str){
      var obj = {}, temp;
      var ops = str.split(aNames.sep.spc);
      for(var i in ops){
        temp = ops[i].split(aNames.sep.key);
        obj[temp[0]] = temp[1];
      }
      return obj;
    }
  }
}
var actions = {
  show : {
    specific : function(data){
      var cont = $(data.cont);
      var elem = $(data.elem);
      window.location.href = data.elem;
      cont.find('.show').removeClass('show').addClass('hide');
      elem.removeClass('hide').addClass('show');
    },
    inside : function(elem){
      elem = $(elem);
      var show = elem.find('.show');
      var hide =  elem.find('.hide');
      show.removeClass('show').addClass('hide');
      hide.removeClass('hide').addClass('show');
    }
  },
  
  hide : {
    inside : function(elem){
      elem = $(elem);
      var show = elem.find('.show');
      var hide =  elem.find('.hide');
      show.removeClass('show').addClass('hide');
      hide.removeClass('hide').addClass('show');
    }
  },
  
  help : {
    afterHash : function(){
      return window.location.hash.substr(1);
    }
  }
}
$(".action").hover(function() {
  var action = $( this ).attr('action');
  var elem = $(this);
  switch(action){
    case 'show' :
      actions.show(elem);
    break;
    case 'transport' :
      actions.transport(elem);
    break;
  }
}, function() {
  var action = $( this ).attr('action');
  var elem = $(this);
  switch(action){
    case 'show' :
      actions.hide(elem);
    break;
    case 'transport' :
      actions.untransport(elem);
    break;
  }
});


$('.'+aNames.class.hover).hover(function() {
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  switch(data.a){
    case 'show.inside' :
      actions.show.inside(elem);
    break;
  }
  
}, function() {
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  switch(data.a){
    case 'show.inside' :
      actions.hide.inside(elem);
    break;
  }
});


$('.'+aNames.class.click).click(function(){
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  elem.parent().find('.'+aNames.class.active).removeClass(aNames.class.active);
  elem.addClass(aNames.class.active);
  
  switch(data.a){
    case 'show.specific' :
      actions.show.specific(data);
    break;
    case 'show.inside' :
      actions.show.inside(data);
    break;
  }
  /*console.log(data);*/
})
var time = {};

  time.displayer = function(t, i){
    t = t-1;
    var h = Math.floor(t / 3600);
    var m = Math.floor((t - (h * 3600)) / 60);
    var s = t - (h * 3600) - (m * 60);
    if (h   < 10) {h   = "0"+h;}
    if (m < 10) {m = "0"+m;}
    if (s < 10) {s = "0"+s;}
    document.getElementsByClassName("time-left")[i].innerHTML = h+":"+m+":"+s;
    if(t>0)
        setTimeout(function(){time.displayer(t, i)},1000);
    else{
        document.getElementsByClassName("time-left")[i].innerHTML = 'live';
    }    
  }

  time.colector = function(){
    var length = document.getElementsByClassName("time-left").length;
    var lengthAgo = document.getElementsByClassName("time-ago").length;
    
    var timeLeft=0;
    for(var i=0; i<length; i++){
      t = document.getElementsByClassName("time-left")[i].textContent/1000;
      if(t != 'delayed')
          time.displayer(parseInt(t), i);
      timeLeft = 0;
    }
    
    for(var i=0; i<lengthAgo; i++){
      var elem = document.getElementsByClassName("time-ago")[i];
      var t = elem.textContent/1000;
      elem.innerHTML = time.ago(t);
    }
  }
  
  time.ago = function(date){
    var seconds = Math.floor(((new Date().getTime()/1000) - date)),
    interval = Math.floor(seconds / 31536000);
    
    if (interval > 1) return interval + "y ago";
    
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) return interval + "m ago";
    
    interval = Math.floor(seconds / 86400);
    if (interval >= 1) return interval + "d ago";
    
    interval = Math.floor(seconds / 3600);
    if (interval >= 1) return interval + "h ago";
    
    interval = Math.floor(seconds / 60);
    if (interval > 1) return interval + "m ago";
    
    return Math.floor(seconds) + "s ago";
  }

  function showSpecific(){
      var query = '';
      var selects = $('select');
      var t1 = selects[0].options[selects[0].selectedIndex].value;
      var t2 = selects[1].options[selects[1].selectedIndex].value;
      t1 ? query += '?teams[]='+t1 : false;
      t1 && t2 ? query += '&teams[]='+t2 : t2 ? query += '?teams[]='+t2 : false;
      window.location.replace(window.location.pathname+query);
  }
