var utils = {};

utils.findSectionById = function(tree, id){
  if(!id || !tree.length){
    return false;
  }
  
  var selectedSection = false;
  
  var check = (tree, id) => _.each(tree, (section) => {
    if(section._id === id){
      selectedSection = section;
      return false;
    }
    if(!!selectedSection){
      return false;
    }
    !!section.children.length && !selectedSection && check(section.children, id);
  });
  
  check(tree, id);
  return selectedSection;
};
function Tree() {
  
  var self = this;
  var apiUrl = '/' || 'https://csgobets.herokuapp.com/';
  riot.observable(this);
  
  this.on('load', function(){
    superagent
      .get(apiUrl + 'api/content/tree')
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log(apiRes);
        console.log('API =>', apiRes.data);
        store.tree = apiRes.data;
        self.trigger('success', apiRes.data);
      });
  });
  
  this.on('loadEntries', function(){
    superagent
      .get(apiUrl + 'api/content/entry')
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes.data);
        store.entries = apiRes.data;
        //self.trigger('success', apiRes.data);
      });
  });
  
  this.on('update', function(section){
    superagent
      .post(apiUrl + 'api/content/section')
      .send(section)
      .end((err, apiRes) => {
        console.log(apiUrl + 'api/content/section');
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        !section._id && self.trigger('load');
      });
  });
  
  this.on('delete', function(section){
    if(section.children.length){
      return console.log('Error, still have children');
    }
    if(confirm('realy want to delete this section?')){
      superagent
        .del(apiUrl + 'api/content/section/' + (section._id || section))
        .send(section)
        .end((err, apiRes) => {
          apiRes = apiRes.body;
          console.log('API =>', apiRes);
          self.trigger('load');
        });
    }
  });
  
  this.on('move', function(moveTo){
    store.draggedSection.parent = moveTo._id;
    superagent
      .post(apiUrl + 'api/content/section/')
      .send(store.draggedSection)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        store.draggedSection = false;
        self.trigger('load');
      });
  });
  
  this.on('entry', function(entry){
    superagent
      .post(apiUrl + 'api/content/entry/')
      .send(entry)
      .end((err, apiRes) => {
        apiRes = apiRes.body;
        console.log('API =>', apiRes);
        self.trigger('load');
      });
  });
  
  this.on('entryDelete', function(entry){
    if(confirm('realy want to delete this entry?')){
      superagent
        .del(apiUrl + 'api/content/entry/' + (entry._id || entry))
        .end((err, apiRes) => {
          apiRes = apiRes.body;
          console.log('API =>', apiRes);
        });
    }
  });
  
  this.on('drag', function(section){
    if(section._id !== store.draggedSection._id){
      store.draggedSection = section;
      delete store.draggedSection.children;
    }
  });
  
  this.on('select', (section) => {
    store.selectedSection = section;
    self.trigger('refresh');
  });

};

const tree = new Tree();

<sectionEdit>
  <span>
    <input 
      class="pdg-tb-1px pdg-l-15px pdg-r-10px fix-bb outline-none mrg-0 brd-black" 
      onblur={this.parent.cancel} 
      name="fieldName"
      placeholder="new section name"
      onkeyup={this.typing}
      value={opts.data.value}/>
  </span>
  
  <script type="es6">
  
    this.on('mount updated', (opts) => {
      this.fieldName.size = this.fieldName.value.length || 1;
      this.fieldName.value === this.opts.data.value 
        ? this.fieldName.select()
        : this.fieldName.focus();
    });
  
    this.typing = (e) => {
      e.target.size = e.target.value.length || 1;
      
      switch(e.keyCode){
        case 27:
          e.target.value = this.opts.data.value;
          this.parent.cancel();
        break;
        case 13:
          if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
            this.parent.updateSection(e.target.value);
          }
          this.parent.cancel();
          //this.unmount();
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
  </script>
</sectionEdit>
<section-new>
  <span>
    <i onclick={this.toggleExpend} class="fa fa-fw mrg-0 dsp-inline fa-genderless clr-grey"
    /><input 
      class="mrg-0 pdg-tb-1px mrg-t-1px pdg-l-15px pdg-r-10px fix-bb outline-none brd-black" 
      onblur={this.cancel} 
      name="newFieldName"
      onkeyup={this.typing}
      placeholder="{opts.newsection.type} name"/>
  </span>
  
  <script type="es6">
    
    this.minSize = 3;
    this.maxSize = 20;
    
    this.on('mount', () => {
      this.newFieldName.focus();
      this.newFieldName.size = this.newFieldName.value.length > this.minSize 
        ? this.newFieldName.value.length 
        : this.minSize;
    });
    
    this.typing = (e) => {
      e.target.size = e.target.value.length > this.minSize 
        ? e.target.value.length 
        : this.minSize;
      
      switch(e.keyCode){
        case 27:
          this.parent.clearNewSection();
          e.target.value = '';
        break;
        case 13:
          if(e.target.value.length >= this.minSize){
            this.parent.addNewSection(e.target.value.replace(/ /g,''));
            e.target.value = '';
          }
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
  </script>
</section-new>
<section-edit>
  <span>
    <input 
      class="pdg-tb-1px pdg-l-15px pdg-r-10px fix-bb outline-none mrg-0 brd-black" 
      onblur={this.cancel}
      name="fieldName"
      placeholder="new section name"
      onkeyup={this.typing}
      value={opts.data.value}/>
  </span>
  
  <script type="es6">
  
    this.on('mount updated', (opts) => {
      this.fieldName.size = this.fieldName.value.length || 1;
      this.fieldName.value === this.opts.data.value 
        ? this.fieldName.select()
        : this.fieldName.focus();
    });
    
    this.cancel = (e) => {
      this.parent.cancel();
    }
  
    this.typing = (e) => {
      e.target.size = e.target.value.length || 1;
      
      switch(e.keyCode){
        case 27:
          e.target.value = this.opts.data.value;
          this.parent.cancel();
        break;
        case 13:
          if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
            this.parent.updateSection(e.target.value);
          }
          this.parent.cancel();
          //this.unmount();
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
  </script>
</section-edit>
<section-dragable>
  <div
    class={'bg-greyl1': over, 'trans': true}
    draggable={opts.data.type !== 'app'} 
    ondrag={this.onDrag} 
    ondrop={opts.data.type !== 'field' && this.onDrop} 
    ondragover={this.onDragOver}
    ondragleave={this.onDragLeave}>
      
      <section-container data={this.opts.data} />
  
  </div>
  
  <script type="es6">
  
    this.over = false;
    
    this.onDrag = (e) => {
      e.target.style.display = 'none';
      tree.trigger('drag', e.item.item);
      e.stopPropagation();
      e.preventUpdate = true;
    };
    
    this.onDrop = (e) => {
      tree.trigger('move', e.item.item);
      e.stopPropagation();
      e.preventUpdate = true;
    };
    
    this.onDragOver = (e) => {
      this.over = true;
      this.onPrevent(e);
    };
    
    this.onDragLeave = (e) => {
      this.over = false;
      this.onPrevent(e);
    };
    
    this.onPrevent = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    
  </script>
</section-dragable>
<section-container>

  <div 
    onclick={this.onRoute}
    oncontextmenu={this.showDropMenu}
    class={
      'hover-bg-greyl1 clr-greyl1 hover-clr-white mrg-t-1px fix-bb cursor-pointer trans': true,
      'bg-greyl1 clr-white': opts.data._id === store.activeRoute.id,
    }>
    
      <i onclick={this.toggleExpend} class={
        'fa fix-bb w-20px dsp-inline txt-center': true,
        'clr-blackl2 hover-clr-green trans cursor-pointer': !!opts.data.children.length,
        'clr-inherit fa-genderless': !opts.data.children.length,
        'fa-angle-right': !expended && !!opts.data.children.length,
        'fa-angle-down': expended && !!opts.data.children.length,
      }
      /><span if={!editMode} ondblclick={this.edit} class={
        'trans pdg-tb-1px dsp-inline pdg-rl-15px': true,
        'bg-blackl3 hover-bg-black brd-blackl1': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'bg-grey brd-greyd1 hover-bg-greyd2 clr-black': opts.data.type === 'field'}
        >
        {opts.data.value || 'noname'}
      </span>
      <span if={!!opts.data.entries.length && opts.data.type==='field' } class="clr-greyd1 fnt-sm">
        {opts.data.entries.length} entries
      </span>
      <section-edit if={editMode && !newSection} data={opts.data} />
      
  </div>
  
  <div show={expended} class="pdg-l-20px">
    <section-new if={!!newSection} newsection={newSection} />
    <section-dragable each={item in opts.data.children} data={item} />
  </div>
  
  <script type="es6">
  
    //console.log(opts.data._id === store.activeRoute.id, store.activeRoute.id);
  
    this.editMode = false;
    this.expended = true;
    this.newSection = false;
    
    dropMenu.on('addNew', (data) => {
      if(this.opts.data._id === data.parent){
        console.log(data);
        this.newSection = data;
        this.expended = true;
        this.update();
      }
    });
    
    this.showDropMenu = (e) => dropMenu.trigger('show', e);
    
    this.clearNewSection = (e) => {
      this.newSection = false;
    }
    
    this.addNewSection = (value) => {
      this.newSection.value = value;
      tree.trigger('update', this.newSection);
      this.item.children.unshift(this.newSection);
      this.newSection = false;
    }
    
    this.updateSection = (value) => {
      this.item.value = value;
      tree.trigger('update', this.item);
    }
    
    this.edit = (e) => {
      this.editMode=true;
    };
    
    this.cancel = (e) => {
      this.editMode = false;
    }
    
    this.toggleExpend = (e) => {
      this.expended = !this.expended;
      e.stopPropagation();
    }
    
    this.onRoute = (e) => {
      window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
      /*if(store.activeRoute.id !== this.opts.data._id){
        window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
        tree.trigger('select', this.opts.data);
      }*/
      this.preventUpdate = true;
    }
    
  </script>
  
</section-container>
<elemInProgress>
  <div show={opts.inprogress} class="fadeOut pos-absolute tb w-100 h-100 bg-black z-1">
    <div class="td txt-center clr-greyl1 fnt-lg">
      <span class="bg-blackl1 pdg-rl-20px pdg-tb-10px">
        <i class="fa fa-circle-o-notch fa-fw fa-spin clr-green"/> Loading App...
      </span>
    </div>
  </div>
</elemInProgress>
<entry-new>
  <div class="brd-b-grey mrg-b-1px fix-bb fnt-md bg-white pos-relative pdg-10px">
    <textarea 
      class="brd-dash-2-grey dsp-block fix-bb outline-none fnt-md w-100 pdg-10px" 
      onblur={!opts.eventmodeoff && this.cancel}
      name="entry"
      placeholder="enter short text"
      onkeyup={this.typing}/>
  </div>
  
  <script type="es6">
    
    this.minSize = 3;
    this.maxSize = 20;
    
    this.on('mount updated', () => {
      this.entry.focus();
    });
    
    this.cancel = (e) => {
      if(e.target.value.length){
        tree.trigger('entry', {
          value: e.target.value,
          field: opts.data._id,
          type: 'short'
        });
      }
      this.parent.toggleNewEntry();
    }
    
    this.typing = (e) => {
      
      switch(e.keyCode){
        case 27:
          !this.opts.eventmodeoff && this.parent.toggleNewEntry();
        break;
        case 13:
          /*if(e.target.value.length >= this.minSize){
            this.parent.addNewSection(e.target.value.replace(/ /g,''));
            e.target.value = '';
          }*/
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
  </script>
</entry-new>
<entry-field>
  <textarea 
    value={opts.data.value} 
    keyup="this.typing" 
    name="entry" 
    placeholder="short text" 
    class="brd-dash-2-grey mrg-b-5px w-100 dsp-block pdg-5px bg-greyl1 fix-bb outline-none fnt-md"/>
  
  <div class="txt-right">
    <span onclick={this.save} class="cursor-pointer hover-bg-yellow bg-yellowd1 trans pdg-rl-15px dsp-inline pdg-tb-1px fnt-bold clr-white">save</span>
  </div>
  
  <script type="es6">
    
    this.typing = (e) => {
      e.target.size = e.target.value.length || 1;
      
      switch(e.keyCode){
        case 27:
          /*e.target.value = this.opts.data.value;
          this.parent.cancel();*/
        break;
        case 13:
          /*if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
            this.parent.updateSection(e.target.value);
          }
          this.parent.cancel();
          this.unmount();*/
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
    this.save = (e) => {
      if(!this.entry.value.length){
        return false;
      }
      const entry = {
        value: this.entry.value,
        type: 'short',
        /*parent: opts.data.parent,*/
        field: opts.data.parent,
      };
      tree.trigger('entry', entry);
    }
  </script>
  
</entry-field>
<entry-edit>
  <textarea 
    class="brd-dash-2-grey dsp-block fix-bb outline-none fnt-md w-100 pdg-10px" 
    onblur={this.cancel}
    name="fieldName"
    placeholder="entry short text"
    onkeyup={this.typing}
    value={opts.data.value}/>
  
  <script type="es6">
  
    this.on('mount updated', (opts) => {
      this.fieldName.value === this.opts.data.value 
        ? this.fieldName.select()
        : this.fieldName.focus();
    });
    
    this.cancel = (e) => {
      if(e.target.value !== this.opts.data.value && e.target.value.length >= 3){
        this.parent.updateEntry(e.target.value);
      }
      this.parent.cancel();
    }
  
    this.typing = (e) => {
      
      switch(e.keyCode){
        case 27:
          e.target.value = this.opts.data.value;
          this.parent.cancel();
        break;
        case 13:
          
        break;
        default:
          e.preventUpdate = true;
        break;
      }
    }
    
  </script>
</entry-edit>
<entry-container>
  <div onmouseenter={this.toggleControls} onmouseleave={this.toggleControls} class="brd-b-grey mrg-b-1px fix-bb fnt-md bg-white pos-relative pdg-10px">
    <p if={!editModeOn} onclick={!editModeOn && this.edit} class="mrg-0 pdg-10px">
      {opts.data.value}
    </p>
    
    <entry-edit if={!!editModeOn} data={opts.data} />
  
    <span show={!editModeOn && hoverModeOn} onclick={this.remove} class="pos-absolute top-0 right-0 pdg-5px">
      <i class="fa fa-lg fa-times clr-greyd1 cursor-pointer trans hover-clr-redl2"/>
    </span>
  </div>
  
  <script type="es6">
    
    this.hoverModeOn = false;
    this.editModeOn = this.opts.editmodeon || false;
    
    this.toggleControls = (e) => {
      this.hoverModeOn = !this.hoverModeOn;
    }
    
    this.edit = (e) => {
      this.editModeOn = true;
    }
    
    this.cancel = (e) => {
      this.editModeOn = false;
    }
    
    this.updateEntry = (entryValue) => {
      this.opts.data.value = entryValue;
      tree.trigger('entry', this.opts.data);
    }
    
    this.remove = (e) => {
      this.parent.removeEntry(opts.data._id);
    }
  </script>
  
</entry-container>
function DropMenu(){ riot.observable(this); }
const dropMenu = new DropMenu();

<drop-menu>
  <ul if={dropMenuOn} class="pos-absolute bg-black pdg-5px clr-white pdg-0 mrg-0 list-none" style="left: {pos.x}px;top: {pos.y}px;">
    <li class="fnt-sm brd-b-blackd1 txt-center pdg-tb-5px clr-blackl3">menu</li>
    <li if={this.section.type !== 'field'} onclick={this.newSection.bind(this, item.type)} each={item in items} class="hover-bg-green pdg-5px cursor-pointer">{item.name}</li>
    <li if={!this.section.children.length} onclick={this.removeSection} class="brd-t-blackd1 txt-center pdg-tb-5px hover-bg-redl3 pdg-5px cursor-pointer">remove</li>
  </ul>
  
  <script type="es6">
    
    this.dropMenuOn = false;
    this.pos = {x: 20, y: 20};
    this.section = false;
    
    dropMenu.on('show', (e) => {
      this.dropMenuOn = true;
      this.pos = {x: e.pageX, y: e.pageY};
      this.section = e.item.item;
      this.update();
    });
    
    dropMenu.on('hide', () => {
      if(this.dropMenuOn){
        this.section = false;
        this.dropMenuOn = false;
        this.update();
      }
    });
    
    this.newSection = (type, e) => dropMenu.trigger('addNew', {type, parent: this.section._id});
    this.removeSection = () => {
      tree.trigger('delete', this.section);
    }
    
    this.items = [
      {name: 'add new section', type: 'section'},
      {name: 'add new field', type: 'field'}
    ];
  </script>
</drop-menu>
<tree>
  
  <section-dragable each={item in opts.tree} data={item} />
  <drop-menu/>
  
  <div class="brd-t-grey mrg-t-50px txt-center">
    <a onclick={this.click} class="dsp-block clr-green txt-up" target="_blank" href="/content/json">preview Json</a>
  </div>
    
  <script type="es6">
  
    tree.on('refresh', () => this.update());
    
    this.click = (e) => {
      window.open(e.target.href, '_blank');
    }
    
  </script>
</tree>

<section-page>
  <div class="w-max-600px mrg-auto">
    <div if={false}>
      <span class={
        'trans pdg-tb-5px dsp-inline pdg-rl-50px clr-white': true,
        'bg-blackl3 hover-bg-black brd-blackl1': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'bg-grey brd-greyd1 hover-bg-greyd2 clr-black': opts.data.type === 'field'}>
        {opts.data.value}
      </span>
      <span class="pdg-5px bg-grey brd-greyd1 clr-white">{opts.data.children.length}</span>
      <span class="brd-grey pdg-tb-5px pdg-rl-10px clr-greyd1">{opts.data.description || 'no description'}</span>
    </div>
    
    
    <div if={opts.data.type === 'section'} class="fnt-lg pdg-tb-10px pdg-rl-20px brd-b-grey mrg-b-20px">
      <span onclick={this.onRoute} class="clr-greenl3 hover-clr-green dsp-inline cursor-pointer">
        <i class="clr-inherit fa fa-chevron-circle-left"/>
        <span class="mrg-l-5px clr-greyd3">back to parent</span>
      </span>
    </div>
  
    <div class={
      'trans pdg-tb-10px pdg-rl-15px clr-white txt-center fnt-lg fnt-bold brd-b-yellowd1 pdg-l-20px w-200px txt-left dsp-inline': true,
      'bg-blackl3 fix-bb mrg-tb-1px': opts.data.type === 'section',
      'bg-greenl1 brd-green': opts.data.type === 'app',
      'clr-black txt-center fnt-lg fnt-bold': opts.data.type === 'field'}>
      {opts.data.value}
    </div>
    
    <div class="pdg-l-20px">
      <sectionFull each={item in opts.data.children} data={item}/>
    </div>
  
  </div>
  
  <script type="es6">
    this.onRoute = (e) => {
      window.location.hash =  'section/' + this.opts.data.parent;
      this.preventUpdate = true;
    }
    
  </script>
  
</section-page>

<sectionFull>
  <div if={!opts.nofields || opts.data.type !== 'field'} class="tb w-100 brd-l-grey">
    <div class="td w-min-150px">
      <span onclick={this.onRoute} class={
        'trans pdg-tb-10px pdg-rl-15px w-150px clr-white cursor-pointer fix-bb': true,
        'bg-blackl3 hover-bg-black mrg-b-1px dsp-inline': opts.data.type === 'section',
        'bg-greenl1 hover-bg-green brd-green': opts.data.type === 'app',
        'hover-bg-greyd2 bg-grey clr-black dsp-inline brd-r-yellowd1': opts.data.type === 'field'}
      >{opts.data.value}</span>
    </div>
    
    <div if={opts.data.type === 'field'} class="td w-100 pdg-tb-5px">
      <entry-container each={item in opts.data.entries} data={item}/>
      <entry-new if={!opts.data.entries.length} data={opts.data} eventmodeoff={true} />
    </div>
  </div>
  <div class="pdg-l-20px">
    <sectionFull each={item in opts.data.children} data={item} noFields={true}/>
  </div>
  
  <script type="es6">
    this.onRoute = (e) => {
      window.location.hash = this.opts.data.type + '/' + this.opts.data._id;
      this.preventUpdate = true;
    }
  </script>
  
</sectionFull>

<home-page>
  <div class="fadeIn txt-center brd-grey">
    <div class="brd-greyl2 pdg-50px clr-greyd1">
      <h1>Good to see u again my friend!!!</h1>
      <p>our small app getting the shape. Hehehe.</p>
    </div>
  </div>
</home-page>
<page-container>
  <div id="pageContainer" class="pdg-30px fix-bb"/>
  
  <script type="es6">
  
    const routes = {};
  	
  	routes.home = (id, action) => riot.mount('#pageContainer', 'home-page', {data: 'some data'});
  	
  	routes.section = (id, action, data) => riot.mount('#pageContainer', 'section-page', {data: store.selectedSection});
  	
  	routes.field = (id, action, data) => riot.mount('#pageContainer', 'field-page', {data: store.selectedSection});
  
    this.on('mount', () => {
      riot.route((pageName, id, action) => {
        store.activeRoute = {pageName, id, action};
        tree.trigger('select', utils.findSectionById(store.tree, id));
        
        pageName = _.contains(['section', 'app'], pageName) && 'section' || pageName;
        
    	  routes[pageName || 'home'](id, action);
    	  
    	});
      
      riot.route.start(true);
    });
    
  </script>
</page-container>
<field-page>
  <div class="w-max-600px mrg-auto">
  
    <div class="fnt-lg pdg-tb-10px pdg-rl-20px brd-b-grey mrg-b-20px">
      <span onclick={this.onRoute} class="clr-greenl3 hover-clr-green dsp-inline cursor-pointer">
        <i class="clr-inherit fa fa-chevron-circle-left"/>
        <span class="mrg-l-5px clr-greyd3">back to parent</span>
      </span>
    </div>
    
    <div class="">
      <div class={
        'trans pdg-tb-10px bg-grey brd-b-yellowd1 pdg-l-20px w-200px txt-left pdg-tb-10px dsp-inline clr-blackl3': true,
        'txt-center fnt-lg fnt-bold': true
      }>
        {opts.data.value}
      </div>
      
      <entry-container each={item in opts.data.entries} data={item}/>
        
      <entry-new if={addNewEntryModeOn} data={opts.data} />
        
      <div onclick={this.toggleNewEntry} class="bg-blackl2 dsp-inline flt-right txt-center hover-bg-black hover-clr-white brd-t-black clr-greyd2 pdg-tb-5px pdg-rl-20px cursor-pointer trans">
        <i class={
          'fa-fw fa-lg fa': true,
          'clr-yellowd1 fa-plus-square': !addNewEntryModeOn,
          'clr-redl1 fa-minus-square': addNewEntryModeOn
        }/> {!addNewEntryModeOn ? 'add new entry' : 'remove new entry' }
      </div>
      
    </div>
    
    <div if={false} class="pdg-l-20px">
      <sectionFull each={item in opts.data.children} data={item}/>
    </div>
  
  </div>
  
  <script type="es6">
    
    this.addNewEntryModeOn = !opts.data.entries.length ? true : false;
    
    this.onRoute = (e) => {
      window.location.hash =  'section/' + this.opts.data.parent;
      this.preventUpdate = true;
    };
    
    this.toggleNewEntry = (e) => {
      this.addNewEntryModeOn = !this.addNewEntryModeOn;
      this.update();
    };
    
    this.removeEntry = (entryId) => {
      tree.trigger('entryDelete', entryId);
      _.remove(opts.data.entries, (entry) => {
        return entry._id == entryId;
      });
    }
  </script>
  
</field-page>
<todo>

  <h3>{ opts.title }</h3>

  <ul>
    <li each={ items.filter(whatShow) }>
      <label class={ completed: done }>
        <input type="checkbox" checked={ done } onclick={ parent.toggle }> { title }
      </label>
    </li>
  </ul>

  <form onsubmit={ add }>
    <input name="input" onkeyup={ edit }>
    <button disabled={ !text }>Add #{ items.filter(whatShow).length + 1 }</button>

    <button disabled={ items.filter(onlyDone).length == 0 } onclick={ removeAllDone }>
    X{ items.filter(onlyDone).length } </button>
  </form>
  
  
  <script type="es6">
    this.items = opts.items

    edit = (e) => {
      this.text = e.target.value
    }

    this.add = (e) => {
      if (this.text) {
        this.items.push({ title: this.text })
        this.text = this.input.value = ''
      }
    }

    this.removeAllDone = (e) => {
      this.items = this.items.filter(function(item) {
        return !item.done
      })
    }
    
    this.whatShow = (item) => {
      return !item.hidden
    }

    this.onlyDone = (item) => {
     return item.done
   }

    this.toggle = (e) => {
      var item = e.item
      item.done = !item.done
      return true
    }
  </script>

</todo>
var store = {
  draggedSection: false,
  selectedSection: false,
  tree: [],
  entries: [],
  activeRoute: {},
  dropMenuVisible: false,
};

<app>
  
  <elemInProgress inprogress={loading} />
  
  <div 
    if={!loading}
    onclick={this.hideDropMenu} 
    class="fadeIn tb h-100vh bg-grey pdg-10px fix-bb w-100" 
    onmousemove={resizing && this.onResize} 
    onmouseup={resizing && this.onResizeEnd}
  >
    <div class="td bg-greyl2 boxshadow fix-bb txt-left valign-top pdg-10px brd-white" style="width:{treeWidth}px;">
      <tree tree={tree}/>
    </div>
    <div 
      class="td w-10px trans txt-center clr-greyl1 hover-clr-white txt-shadow" 
      style="cursor:col-resize;"
      onmousedown={this.onResizeStart} 
    >
      <i class="fa fa-ellipsis-v"/>
    </div>
    <div class="td boxshadow bg-greyl1 brd-greyl2 fix-bb valign-top">
       <page-container data={tree}/>
    </div>
  </div>
  
  <script type="es6">
    
    this.treeBaseWidth = 300;
    this.treeWidth = this.treeBaseWidth;
    this.resizing = false;
    this.startPoint = 0;
    
    this.loading = true;
    this.tree = [];
    
    this.on('mount', () => {
      tree.trigger('load');
    });
    
    this.onResizeStart = (e) => {
      this.resizing = true;
      this.startPoint = e.pageX;
    }
    
    this.onResize = (e) => {
      this.resizing
        ? this.treeWidth = this.treeBaseWidth +  (e.pageX - this.startPoint)
        : e.preventUpdate = true;
    }
    
    this.onResizeEnd = (e) => {
      this.treeBaseWidth = this.treeWidth;
      this.resizing = false;
    }
    
    this.on('updated', () => {
      console.log('app updated');
    });
    
    tree.on('success', (tree) => {
      this.tree = tree;
      this.loading = false;
      this.update();
    });
    
    this.hideDropMenu = (e) => {
      dropMenu.trigger('hide');
      e.preventUpdate = true;
    }
    
  </script>
</app>

<route>
  <a onclick={this.redirect} href={opts.href}><yield/></a>
  
  <script type="es6">
    this.redirect = (e) => {
      window.location.hash = e.target.hash;
    }
  </script>
</route>