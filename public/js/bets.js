time.colector();
var subpage = window.location.hash.substr(1);
subpage
  ? actions.show.specific({elem:'#'+subpage, cont:'.transport'})
  : false

var basicInterval = 5000;

var unique = function(){
  $.getJSON("/api/unique", function(data) {
    if($("#unique").text() == data.unique && $("#requests").text() == data.requests){
      basicInterval > 100000 ? false : basicInterval += 5000;
    } else {
      //console.log('new users', data);
      basicInterval = 5000;
      $("#unique").text(data.unique);
      $("#requests").text(data.requests);
    }
    setTimeout(unique, basicInterval);
  });
}

setTimeout(unique, basicInterval);

