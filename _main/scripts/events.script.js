$(".action").hover(function() {
  var action = $( this ).attr('action');
  var elem = $(this);
  switch(action){
    case 'show' :
      actions.show(elem);
    break;
    case 'transport' :
      actions.transport(elem);
    break;
  }
}, function() {
  var action = $( this ).attr('action');
  var elem = $(this);
  switch(action){
    case 'show' :
      actions.hide(elem);
    break;
    case 'transport' :
      actions.untransport(elem);
    break;
  }
});


$('.'+aNames.class.hover).hover(function() {
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  switch(data.a){
    case 'show.inside' :
      actions.show.inside(elem);
    break;
  }
  
}, function() {
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  switch(data.a){
    case 'show.inside' :
      actions.hide.inside(elem);
    break;
  }
});


$('.'+aNames.class.click).click(function(){
  var elem = $(this);
  var data = aNames.fn.convertDataToObj(elem.attr(aNames.attr.data));
  
  elem.parent().find('.'+aNames.class.active).removeClass(aNames.class.active);
  elem.addClass(aNames.class.active);
  
  switch(data.a){
    case 'show.specific' :
      actions.show.specific(data);
    break;
    case 'show.inside' :
      actions.show.inside(data);
    break;
  }
  /*console.log(data);*/
})
