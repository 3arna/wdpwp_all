var actions = {
  show : {
    specific : function(data){
      var cont = $(data.cont);
      var elem = $(data.elem);
      window.location.href = data.elem;
      cont.find('.show').removeClass('show').addClass('hide');
      elem.removeClass('hide').addClass('show');
    },
    inside : function(elem){
      elem = $(elem);
      var show = elem.find('.show');
      var hide =  elem.find('.hide');
      show.removeClass('show').addClass('hide');
      hide.removeClass('hide').addClass('show');
    }
  },
  
  hide : {
    inside : function(elem){
      elem = $(elem);
      var show = elem.find('.show');
      var hide =  elem.find('.hide');
      show.removeClass('show').addClass('hide');
      hide.removeClass('hide').addClass('show');
    }
  },
  
  help : {
    afterHash : function(){
      return window.location.hash.substr(1);
    }
  }
}
