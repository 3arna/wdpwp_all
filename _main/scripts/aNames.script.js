var aNames = {
  class : {
    click   : 'aClick',
    active  : 'active',
    hover   : 'aHover'
  },
  attr : {
    data : 'aData'
  },
  sep : {
    key : ':',
    spc : ', '
  },
  fn : {
    convertDataToObj : function(str){
      var obj = {}, temp;
      var ops = str.split(aNames.sep.spc);
      for(var i in ops){
        temp = ops[i].split(aNames.sep.key);
        obj[temp[0]] = temp[1];
      }
      return obj;
    }
  }
}
