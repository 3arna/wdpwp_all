var time = {};

  time.displayer = function(t, i){
    t = t-1;
    var h = Math.floor(t / 3600);
    var m = Math.floor((t - (h * 3600)) / 60);
    var s = t - (h * 3600) - (m * 60);
    if (h   < 10) {h   = "0"+h;}
    if (m < 10) {m = "0"+m;}
    if (s < 10) {s = "0"+s;}
    document.getElementsByClassName("time-left")[i].innerHTML = h+":"+m+":"+s;
    if(t>0)
        setTimeout(function(){time.displayer(t, i)},1000);
    else{
        document.getElementsByClassName("time-left")[i].innerHTML = 'live';
    }    
  }

  time.colector = function(){
    var length = document.getElementsByClassName("time-left").length;
    var lengthAgo = document.getElementsByClassName("time-ago").length;
    
    var timeLeft=0;
    for(var i=0; i<length; i++){
      t = document.getElementsByClassName("time-left")[i].textContent/1000;
      if(t != 'delayed')
          time.displayer(parseInt(t), i);
      timeLeft = 0;
    }
    
    for(var i=0; i<lengthAgo; i++){
      var elem = document.getElementsByClassName("time-ago")[i];
      var t = elem.textContent/1000;
      elem.innerHTML = time.ago(t);
    }
  }
  
  time.ago = function(date){
    var seconds = Math.floor(((new Date().getTime()/1000) - date)),
    interval = Math.floor(seconds / 31536000);
    
    if (interval > 1) return interval + "y ago";
    
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) return interval + "m ago";
    
    interval = Math.floor(seconds / 86400);
    if (interval >= 1) return interval + "d ago";
    
    interval = Math.floor(seconds / 3600);
    if (interval >= 1) return interval + "h ago";
    
    interval = Math.floor(seconds / 60);
    if (interval > 1) return interval + "m ago";
    
    return Math.floor(seconds) + "s ago";
  }

  function showSpecific(){
      var query = '';
      var selects = $('select');
      var t1 = selects[0].options[selects[0].selectedIndex].value;
      var t2 = selects[1].options[selects[1].selectedIndex].value;
      t1 ? query += '?teams[]='+t1 : false;
      t1 && t2 ? query += '&teams[]='+t2 : t2 ? query += '?teams[]='+t2 : false;
      window.location.replace(window.location.pathname+query);
  }
